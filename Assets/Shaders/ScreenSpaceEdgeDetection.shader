﻿Shader "Hidden/ScreenSpaceEdgeDetection"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_EdgeSize ("Edge Size", Float) = 1
		_EdgePower ("Edge Power", Float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always
		// Edge Darkening
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			struct v2f {
				float2 uv_Main : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_TexelSize;

			float _EdgeSize;
			float _EdgePower;

			float4 ColorMod(float4 c, float d) {
				return c - (c - c * c) * (d - 1);
			}

			v2f vert(appdata v) {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv_Main = v.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target {
				float2 uv_offset = _MainTex_TexelSize.xy * _EdgeSize;
				fixed4 src_l = tex2D(_MainTex, i.uv_Main + float2(-uv_offset.x, 0));
				fixed4 src_r = tex2D(_MainTex, i.uv_Main + float2(+uv_offset.x, 0));
				fixed4 src_b = tex2D(_MainTex, i.uv_Main + float2(           0, -uv_offset.y));
				fixed4 src_t = tex2D(_MainTex, i.uv_Main + float2(           0, +uv_offset.y));
				fixed4 src = tex2D(_MainTex, i.uv_Main);

				fixed4 grad = abs(src_r - src_l) + abs(src_b - src_t);
				float intens = saturate(0.333 * (grad.x + grad.y + grad.z));
				float d = _EdgePower * intens + 1;
				return ColorMod(src, d);
			}

			
			ENDCG
		}
		/*
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				col = 1 - col;
				return col;
			}
			ENDCG
		}*/
	}
}
