﻿Shader "Hidden/ScreenSpaceGreyScale"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_DisplacementTex ("Displacement Texture", 2D) = "white" {}
		_Magnitude ("Magnitude", Range(0, 1)) = 1
		_WobbleEffect ("Wobble Effect", Range(1, 1000)) = 500
		_GreyIntensity ("Grey Intensity", float) = 1
		_Color ("Color", Color) = (1,0,0,1)

		_Intensity ("Intensity", float) = 1
		_Smoothness ("Smothness", float) = 10
		_Roundness ("Roundness", float) = 1
		_Rounded ("Rounded", float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _Magnitude;
			sampler2D _DisplacementTex;
			float _WobbleEffect;
			float _GreyIntensity;
			float4 _Color;

			float _Intensity; 	// x
			float _Smoothness; 	// y
			float _Roundness; 	// z
			float _Rounded; 	// w

			// create vignette effect between two colors, first color (greyscale) is the vignette and the second color (color) is the focus point
			fixed4 vignette(v2f i, fixed4 greyscale, fixed4 color)
			{
				float2 d = abs(i.uv - float2(0.5, 0.5)) * _Intensity;// * _Vignette_Settings.x; // this is the center point where we do the effect, if we change this then we can move the effect around a bit
                d.x *= lerp(1.0, _ScreenParams.x / _ScreenParams.y, _Rounded);//_Vignette_Settings.w);
                d = pow(d, _Roundness); //_Vignette_Settings.z); // Roundness
                half vfactor = pow(saturate(1.0 - dot(d, d)), (_Magnitude * 10) + 0.5f);//_Vignette_Settings.y);
                float3 vignette = lerp(greyscale, color, vfactor);
				return fixed4(vignette.x, vignette.y, vignette.z, 1);
			}

			float2 displacement(v2f i)
			{
				float2 distuv = float2(i.uv.x + _Time.x * 2, i.uv.y + _Time.x * 2);
				float2 disp = tex2D(_DisplacementTex, distuv).xy;
				return ((disp * 2) - 1) * _Magnitude / _WobbleEffect;
			}

			fixed4 frag (v2f i) : SV_Target
			{				
				// maybe use displacement only on the greyscale image
				float2 disp = displacement(i);

				// fixed4 col = tex2D(_MainTex, i.uv /*+ disp*/);
				fixed4 col = tex2D(_MainTex, i.uv + disp);
				// fixed4 col_grey = tex2D(_MainTex, i.uv + disp);
			
				float grey = (col.r + col.g + col.b) / 3;
				grey /= _GreyIntensity;
				// float grey = (col_grey.r + col_grey.g + col_grey.b) / 3;

				fixed4 greyscale = fixed4(grey, grey, grey, col.a) * _Color;

				fixed4 grey_vig = vignette(i, greyscale, col);

				// fixed4 vig_final = vignette(i);
				// vig_final = 1 - vig_final;

				// greyscale *= vig_final;

				// have to use stencil buffer to mask between different textures?
				// need to sample and lerp between the different colors (color and greyscale) and use the vignette mask to know where to apply greyscale and where to apply color
				
				// Final pixel = alpha * ( color1 ) + (1.0-alpha) * (color 2)
				fixed4 final = _Magnitude * (grey_vig) + (1.0 - _Magnitude) * col;

				return final;
			}
			ENDCG
		}
	}
}
