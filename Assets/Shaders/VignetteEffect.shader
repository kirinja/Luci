﻿Shader "Hidden/VignetteEffect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		//half4 _Vignette_Settings; // x: intensity, y: smoothness, z: roundness, w: rounded
		_Intensity ("Intensity", float) = 1
		_Smoothness ("Smothness", float) = 10
		_Roundness ("Roundness", float) = 1
		_Rounded ("Rounded", float) = 1
		_Color ("Color", Color) =  (1, 1, 1, 1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#pragma target 3.0

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			// Procedural vignette
            // #if VIGNETTE_CLASSIC
			// {
            //     half2 d = abs(uv - _Vignette_Center) * _Vignette_Settings.x;
            //     d.x *= lerp(1.0, _ScreenParams.x / _ScreenParams.y, _Vignette_Settings.w);
            //     d = pow(d, _Vignette_Settings.z); // Roundness
            //     half vfactor = pow(saturate(1.0 - dot(d, d)), _Vignette_Settings.y);
            //     color *= lerp(_Vignette_Color, (1.0).xxx, vfactor);
            // }

			//half4 _Vignette_Settings; // x: intensity, y: smoothness, z: roundness, w: rounded

			float _Intensity; 	// x
			float _Smoothness; 	// y
			float _Roundness; 	// z
			float _Rounded; 	// w
			float3 _Color;

			fixed4 frag (v2f i) : SV_Target
			{
				float2 d = abs(i.uv - float2(0.5, 0.5)) * _Intensity;// * _Vignette_Settings.x;
                d.x *= lerp(1.0, _ScreenParams.x / _ScreenParams.y, _Rounded);//_Vignette_Settings.w);
                d = pow(d, _Roundness); //_Vignette_Settings.z); // Roundness
                half vfactor = pow(saturate(1.0 - dot(d, d)), _Smoothness);//_Vignette_Settings.y);
                float3 color = lerp(_Color, (1.0).xxx, vfactor);
				fixed4 col2 = fixed4(color.x, color.y, color.z, 1);
				

				fixed4 col = tex2D(_MainTex, i.uv);
				col *= col2;
				// just invert the colors
				//col = 1 - col;
				return col;
			}
			ENDCG
		}
	}
}
