using UnityEngine;
using System.Collections;

[System.Serializable]
public class GeoPainterPoint : object
{
    public GameObject go;

    public Vector3 pos;

    public Vector3 scale;

    public Vector3 normal;

    public bool useNormal;

}