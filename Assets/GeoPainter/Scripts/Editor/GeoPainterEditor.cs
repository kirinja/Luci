using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[System.Serializable]
[UnityEditor.CustomEditor(typeof(GeoPainter))]
public class GeoPainterEditor : Editor
{
    // if stuff doesnt work properly then we might need to use GameObject[] instead of List. We might also need to use object[]/List and convert maually to gameobject
    public static string appTitle;

    private bool isPainting;
    [SerializeField]
    private List<GameObject> myGroups;
    [SerializeField]
    private List<GameObject> myLibrary;
    [SerializeField]
    private List<GameObject> myObjToInstArray;

    private bool showGroups;

    private bool showPaint;

    private bool showBiblio;

    private bool showRandom;

    private GameObject currentGroup;

    private GeoPainterGroup currentGroupScript;

    private int copyFromIndex;
    [SerializeField]
    private List<string> myCopyFromPop; //was object originally

    //********************************************************************************************
    //*** OnInspectorGUI
    //********************************************************************************************
    public override void OnInspectorGUI()//End Panel
    {
        var myTarget = (GeoPainter)target;

        //Check if an object has been deleted
        if (!(this.currentGroupScript == null) && !(this.currentGroupScript.myPointsList.Count == 0))
        {
            int i = ((int) this.currentGroupScript.myPointsList.Count) - 1;
            while (i >= 0)
            {
                var element = this.currentGroupScript.myPointsList[i];
                if (element.go == null)
                {
                    this.currentGroupScript.myPointsList.RemoveAt(i);
                }
                i--;
            }
        }
        //	myGroups = target.myGroups;
        //GROUPS
        //myStyleBoldText.fontStyle = FontStyle.Bold;
        if (this.myGroups == null)
        {
            this.myGroups = new List<GameObject>();
            if (myTarget.myGroupsBuiltIn == null)
                myTarget.myGroupsBuiltIn = new List<GameObject>();
            foreach (var obj in myTarget.myGroupsBuiltIn)
            {
                if (!(obj == null))
                {
                    this.myGroups.Add(obj);
                }
            }
            myTarget.groupSelIndex = 1;
        }
        this.showGroups = EditorGUILayout.Foldout(this.showGroups, "GROUPS", EditorStyles.boldLabel);
        if (this.showGroups && !this.isPainting)
        {
             //Add / Remove Groups
            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
            //EditorGUILayout.LabelField("Add","");
            if (GUILayout.Button("+", new GUILayoutOption[] { }))
            //if (GUILayout.Button("+", new GUILayoutOption[]{ GUILayout.MinWidth(100), GUILayout.ExpandWidth(true), GUILayout.MaxWidth(450)}))
            {
                this.addGroup();
                myTarget.myGroupsBuiltIn = myGroups;//.ToBuiltin(typeof(GameObject));
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            if (this.myGroups.Count > 0)
            {
                 //Groups Display
                int nbrTemp = 1;
                foreach (var obj in this.myGroups)
                {
                    string myLabel = ("" + nbrTemp) + ": ";
                    EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {GUILayout.ExpandWidth(true), GUILayout.MinWidth(100)});
                    if (nbrTemp == myTarget.groupSelIndex)
                    {
                        obj.name = EditorGUILayout.TextField(myLabel + " (EDIT)", obj.name);
                    }
                    else
                    {
                        obj.name = EditorGUILayout.TextField(myLabel + "  ", obj.name);
                    }
                    if (GUILayout.Button("EDIT", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.MinWidth(100) }))
                    {
                        myTarget.groupSelIndex = nbrTemp;
                        this.currentGroup = this.myGroups[nbrTemp - 1];
                        //this.currentGroupScript = this.currentGroup.GetComponent("GeoPainterGroup");
                        this.currentGroupScript = this.currentGroup.GetComponent<GeoPainterGroup>();
                        this.myLibrary = new List<GameObject>(this.currentGroupScript.myLibraryBuiltIn);// {this.currentGroupScript.myLibraryBuiltIn};
                        var position = this.currentGroup.transform.position;
                        SceneView.lastActiveSceneView.pivot = position;
                        SceneView.lastActiveSceneView.Repaint();
                    }
                    if (GUILayout.Button("REMOVE", new GUILayoutOption[] {GUILayout.Width(100)}))
                    {
                        this.removeGroup(nbrTemp, false);
                        myTarget.myGroupsBuiltIn = this.myGroups;//.ToBuiltin(typeof(GameObject));
                    }
                    if (GUILayout.Button("RELEASE", new GUILayoutOption[] {GUILayout.Width(100)}))
                    {
                        myTarget.groupSelIndex = nbrTemp;
                        this.currentGroup = this.myGroups[nbrTemp - 1];
                        //this.currentGroupScript = this.currentGroup.GetComponent("GeoPainterGroup");
                        this.currentGroupScript = this.currentGroup.GetComponent<GeoPainterGroup>();
                        //this.myLibrary = new object[] {this.currentGroupScript.myLibraryBuiltIn};
                        this.myLibrary = new List<GameObject>(this.currentGroupScript.myLibraryBuiltIn);//{ this.currentGroupScript.myLibraryBuiltIn };

                        var position = this.currentGroup.transform.position;
                        SceneView.lastActiveSceneView.pivot = position;
                        SceneView.lastActiveSceneView.Repaint();
                        this.updatePrefab(true);
                        this.removeGroup(nbrTemp, true);
                        myTarget.myGroupsBuiltIn = this.myGroups;//.ToBuiltin(typeof(GameObject));
                    }
                    EditorGUILayout.EndHorizontal();
                    nbrTemp++;
                }
            }
        }
        //PANELS
        if (this.myGroups.Count > 0)
        {
            this.currentGroup = this.myGroups[((int) (((int) myTarget.groupSelIndex) - 1))];
            //this.currentGroupScript = this.currentGroup.GetComponent("GeoPainterGroup");
            this.currentGroupScript = this.currentGroup.GetComponent<GeoPainterGroup>();
            //BIBLIO
            if (this.myLibrary == null)
            {
                //this.myLibrary = new object[] {this.currentGroupScript.myLibraryBuiltIn};
                this.myLibrary = new List<GameObject>(this.currentGroupScript.myLibraryBuiltIn);
            }
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            this.showBiblio = EditorGUILayout.Foldout(this.showBiblio, "LIBRARY", EditorStyles.boldLabel);
            if (this.showBiblio && !this.isPainting)
            {
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                /*
				//Method
				target.bibSortIndex = EditorGUILayout.Popup("Method: ", target.bibSortIndex, target.bibSortMethod);
				if(target.bibSortIndex == 1) {
					target.bibSoloSelect = EditorGUILayout.IntField("Selected Element:", target.bibSoloSelect);
				}
				
				EditorGUILayout.Space();
				EditorGUILayout.Space();
				*/
                //Elements
                int x = 0;
                if (GUILayout.Button("ADD OBJECT", new GUILayoutOption[] {}))
                {
                    this.myLibrary.Add(null);
                    this.currentGroupScript.myLibraryBuiltIn = this.myLibrary;//.ToBuiltin(typeof(GameObject));
                }
                x = 0;
                while (x < this.myLibrary.Count)
                {
                    EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
                    EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                    EditorGUILayout.LabelField(("Object #" + x) + " :", new GUILayoutOption[] {});
                    this.myLibrary[x] = (GameObject)EditorGUILayout.ObjectField((UnityEngine.Object) this.myLibrary[x], typeof(GameObject), false, new GUILayoutOption[] {});
                    if (GUILayout.Button("REMOVE", new GUILayoutOption[] {}))
                    {
                        this.myLibrary.RemoveAt(x);
                        this.currentGroupScript.myLibraryBuiltIn = this.myLibrary;//.ToBuiltin(typeof(GameObject));
                        break;
                    }
                    EditorGUILayout.EndVertical();
                    if (!(this.myLibrary[x] == null))
                    {
                        Texture2D previewTex = null;
                        previewTex = AssetPreview.GetAssetPreview(this.myLibrary[x]);
                        GUILayout.Button(previewTex, GUILayout.Width(100), GUILayout.Height(100));
                    }
                    EditorGUILayout.EndHorizontal();
                    GUILayout.Space(20);
                    x++;
                }
                if (GUI.changed)
                {
                    this.currentGroupScript.myLibraryBuiltIn = this.myLibrary;//.ToBuiltin(typeof(GameObject));
                }
                GUI.changed = false;
                if (this.myLibrary.Count != 0)
                {
                    if (myTarget.bibSoloSelect > (this.myLibrary.Count - 1))
                    {
                        myTarget.bibSoloSelect = this.myLibrary.Count - 1;
                    }
                }
                if (myTarget.bibSoloSelect < 0)
                {
                    myTarget.bibSoloSelect = 0;
                }
                EditorGUILayout.Space();
                if (GUILayout.Button("Update Prefabs", new GUILayoutOption[] {GUILayout.Height(30)}))
                {
                    this.updatePrefab(false);
                    this.randomize();
                }
                if (GUILayout.Button("Replace Prefab", new GUILayoutOption[] {GUILayout.Height(30)}))
                {
                    this.replacePrefab();
                }
            }
            //End Biblio		
            //PAINT
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            this.showPaint = EditorGUILayout.Foldout(this.showPaint, "PAINT", EditorStyles.boldLabel);
            if (this.showPaint)
            {
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                myTarget.paintLayer = EditorGUILayout.LayerField("Paint Layer:", (int) myTarget.paintLayer, new GUILayoutOption[] {});
                EditorGUILayout.Space();
                if (!this.isPainting)
                {
                    if (GUILayout.Button("Start Painting", new GUILayoutOption[] {GUILayout.Height(40)}))
                    {
                        myTarget.rndAuto = false;
                        this.myObjToInstArray = new List<GameObject>(); //new object[0];
                        this.isPainting = true;
                    }
                }
                else
                {
                    if (GUILayout.Button("Stop Painting  (CTRL+Click) = Paint (SHIFT+Click) = Erase", new GUILayoutOption[] {GUILayout.Height(40)}))
                    {
                        myTarget.myGroupsBuiltIn = this.myGroups;//.ToBuiltin(typeof(GameObject));
                        this.isPainting = false;
                    }
                }
                if (GUILayout.Button("Clean Painting", new GUILayoutOption[] {GUILayout.Height(30)}))
                {
                    var i = ((int) this.currentGroupScript.myPointsList.Count) - 1;
                    while (i >= 0)
                    {
                        var element = this.currentGroupScript.myPointsList[i];
                        UnityEngine.Object.DestroyImmediate(element.go);
                        this.currentGroupScript.myPointsList.RemoveAt(i);
                        i--;
                    }
                }
                //Distance Radius
                EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
                EditorGUILayout.PrefixLabel("Distance Radius ");
                EditorGUILayout.PrefixLabel("(D, SHIFT+D): ");
                myTarget.myDistance = EditorGUILayout.FloatField((float)myTarget.myDistance, new GUILayoutOption[] {});
                if (myTarget.myDistance <= 0)
                {
                    myTarget.myDistance = 0;
                }
                EditorGUILayout.EndHorizontal();
                //Spray Radius
                EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
                EditorGUILayout.PrefixLabel("Spray Radius ");
                EditorGUILayout.PrefixLabel("(S, SHIFT+S): ");
                myTarget.mySpray = EditorGUILayout.FloatField((float)myTarget.mySpray, new GUILayoutOption[] {});
                if (myTarget.mySpray <= 0)
                {
                    myTarget.mySpray = 0;
                }
                EditorGUILayout.EndHorizontal();
                //Delete Radius
                EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
                EditorGUILayout.PrefixLabel("Delete Radius");
                EditorGUILayout.PrefixLabel(" (C, SHIFT+C): ");
                myTarget.myDelete = EditorGUILayout.FloatField((float)myTarget.myDelete, new GUILayoutOption[] {});
                if (myTarget.myDelete <= 0)
                {
                    myTarget.myDelete = 0;
                }
                EditorGUILayout.EndHorizontal();
                myTarget.useNormal = EditorGUILayout.Toggle("Use Normal ?", myTarget.useNormal, new GUILayoutOption[] {});
            }
            //RandomSection
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            this.showRandom = EditorGUILayout.Foldout(this.showRandom, "RANDOMIZE", EditorStyles.boldLabel);
            if (this.showRandom && !this.isPainting)
            {
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                this.currentGroupScript.rndSeed = EditorGUILayout.IntSlider("Seed: ", (int) this.currentGroupScript.rndSeed, 1, 12600, new GUILayoutOption[] {});
                //POSITION
                EditorGUILayout.Space();
                GUILayout.Label("POSITION", EditorStyles.boldLabel, new GUILayoutOption[] {});
                EditorGUILayout.Space();
                EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
                EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                EditorGUILayout.LabelField("", "", new GUILayoutOption[] {GUILayout.Width(3)});
                EditorGUILayout.LabelField("x:", "", new GUILayoutOption[] {GUILayout.Width(3)});
                EditorGUILayout.LabelField("y:", "", new GUILayoutOption[] {GUILayout.Width(3)});
                EditorGUILayout.LabelField("z:", "", new GUILayoutOption[] {GUILayout.Width(3)});
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                EditorGUILayout.LabelField("", "Offset", new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.offPosX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.offPosX, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.offPosY = EditorGUILayout.FloatField("", (float) this.currentGroupScript.offPosY, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.offPosZ = EditorGUILayout.FloatField("", (float) this.currentGroupScript.offPosZ, new GUILayoutOption[] {GUILayout.Width(200)});
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                EditorGUILayout.LabelField("", "Random Min", new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndPosMinX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndPosMinX, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndPosMinY = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndPosMinY, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndPosMinZ = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndPosMinZ, new GUILayoutOption[] {GUILayout.Width(200)});
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                EditorGUILayout.LabelField("", "Random Max", new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndPosMaxX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndPosMaxX, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndPosMaxY = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndPosMaxY, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndPosMaxZ = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndPosMaxZ, new GUILayoutOption[] {GUILayout.Width(200)});
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
                //ROTATION
                EditorGUILayout.Space();
                GUILayout.Label("ROTATION", EditorStyles.boldLabel, new GUILayoutOption[] {});
                EditorGUILayout.Space();
                EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
                EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                EditorGUILayout.LabelField("", "", new GUILayoutOption[] {GUILayout.Width(3)});
                EditorGUILayout.LabelField("x:", "", new GUILayoutOption[] {GUILayout.Width(3)});
                EditorGUILayout.LabelField("y:", "", new GUILayoutOption[] {GUILayout.Width(3)});
                EditorGUILayout.LabelField("z:", "", new GUILayoutOption[] {GUILayout.Width(3)});
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                EditorGUILayout.LabelField("", "Offset", new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.offRotX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.offRotX, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.offRotY = EditorGUILayout.FloatField("", (float) this.currentGroupScript.offRotY, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.offRotZ = EditorGUILayout.FloatField("", (float) this.currentGroupScript.offRotZ, new GUILayoutOption[] {GUILayout.Width(200)});
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                EditorGUILayout.LabelField("", "Random Min", new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndRotMinX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndRotMinX, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndRotMinY = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndRotMinY, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndRotMinZ = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndRotMinZ, new GUILayoutOption[] {GUILayout.Width(200)});
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                EditorGUILayout.LabelField("", "Random Max", new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndRotMaxX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndRotMaxX, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndRotMaxY = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndRotMaxY, new GUILayoutOption[] {GUILayout.Width(200)});
                this.currentGroupScript.rndRotMaxZ = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndRotMaxZ, new GUILayoutOption[] {GUILayout.Width(200)});
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
                //Scale
                EditorGUILayout.Space();
                GUILayout.Label("SCALE", EditorStyles.boldLabel, new GUILayoutOption[] {});
                EditorGUILayout.Space();
                this.currentGroupScript.scaleUniform = EditorGUILayout.Toggle("Uniform:", this.currentGroupScript.scaleUniform, new GUILayoutOption[] {});
                if (this.currentGroupScript.scaleUniform == false)
                {
                    EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
                    EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                    EditorGUILayout.LabelField("", "", new GUILayoutOption[] {GUILayout.Width(3)});
                    EditorGUILayout.LabelField("x:", "", new GUILayoutOption[] {GUILayout.Width(3)});
                    EditorGUILayout.LabelField("y:", "", new GUILayoutOption[] {GUILayout.Width(3)});
                    EditorGUILayout.LabelField("z:", "", new GUILayoutOption[] {GUILayout.Width(3)});
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                    EditorGUILayout.LabelField("", "Offset", new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.offSclX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.offSclX, new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.offSclY = EditorGUILayout.FloatField("", (float) this.currentGroupScript.offSclY, new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.offSclZ = EditorGUILayout.FloatField("", (float) this.currentGroupScript.offSclZ, new GUILayoutOption[] {GUILayout.Width(200)});
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                    EditorGUILayout.LabelField("", "Random Min", new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.rndSclMinX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndSclMinX, new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.rndSclMinY = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndSclMinY, new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.rndSclMinZ = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndSclMinZ, new GUILayoutOption[] {GUILayout.Width(200)});
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                    EditorGUILayout.LabelField("", "Random Max", new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.rndSclMaxX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndSclMaxX, new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.rndSclMaxY = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndSclMaxY, new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.rndSclMaxZ = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndSclMaxZ, new GUILayoutOption[] {GUILayout.Width(200)});
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndHorizontal();
                }
                else
                {
                    EditorGUILayout.BeginVertical(new GUILayoutOption[] {});
                    EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
                    EditorGUILayout.LabelField("", "", new GUILayoutOption[] {GUILayout.Width(30)});
                    EditorGUILayout.LabelField("", "Offset", new GUILayoutOption[] {GUILayout.Width(200)});
                    EditorGUILayout.LabelField("", "Random Min", new GUILayoutOption[] {GUILayout.Width(200)});
                    EditorGUILayout.LabelField("", "Random Max", new GUILayoutOption[] {GUILayout.Width(200)});
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
                    EditorGUILayout.LabelField("All:", "", new GUILayoutOption[] {GUILayout.Width(30)});
                    this.currentGroupScript.offSclX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.offSclX, new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.rndSclMinX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndSclMinX, new GUILayoutOption[] {GUILayout.Width(200)});
                    this.currentGroupScript.rndSclMaxX = EditorGUILayout.FloatField("", (float) this.currentGroupScript.rndSclMaxX, new GUILayoutOption[] {GUILayout.Width(200)});
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndHorizontal();
                }
                //AUTO
                EditorGUILayout.Space();
                GUILayout.Label("UPDATE", EditorStyles.boldLabel, new GUILayoutOption[] {});
                EditorGUILayout.Space();
                myTarget.rndAuto = GUILayout.Toggle(myTarget.rndAuto, "Auto Update", "button", new GUILayoutOption[] {});
                if (GUILayout.Button("Randomize", new GUILayoutOption[] {GUILayout.Height(30)}))
                {
                    this.randomize();
                }
                if (GUI.changed && (myTarget.rndAuto != false)) // HACK
                {
                    this.randomize();
                    GUI.changed = false;
                }
                this.myCopyFromPop = new List<string>();
                var i = 0;
                while (i < this.myGroups.Count)
                {
                    this.myCopyFromPop.Add(this.myGroups[((int) i)].name);
                    i++;
                }
                EditorGUILayout.Space();
                GUILayout.Label("COPY & PASTE FROM GROUP", EditorStyles.boldLabel, new GUILayoutOption[] {});
                EditorGUILayout.Space();
                EditorGUILayout.BeginHorizontal(new GUILayoutOption[] {});
                //this.copyFromIndex = (int) EditorGUILayout.Popup(this.copyFromIndex, Array.ConvertAll<object, string>(myCopyFromPop, ConvertObjectToString));
                this.copyFromIndex = (int) EditorGUILayout.Popup(this.copyFromIndex, myCopyFromPop.ToArray());
                if (GUILayout.Button("COPY", new GUILayoutOption[] {GUILayout.Width(100)}))
                {
                    this.copySettings();
                }
                EditorGUILayout.EndHorizontal();
            }
        }
    }
    
    //********************************************************************************************
    //*** Update Prefab
    //********************************************************************************************
    public virtual void copySettings()
    {
        //var myTempScript = this.myGroups[this.copyFromIndex].GetComponent("GeoPainterGroup");
        var myTempScript = this.myGroups[this.copyFromIndex].GetComponent<GeoPainterGroup>();
        this.currentGroupScript.rndSeed = myTempScript.rndSeed;
        this.currentGroupScript.offPosX = myTempScript.offPosX;
        this.currentGroupScript.offPosY = myTempScript.offPosY;
        this.currentGroupScript.offPosZ = myTempScript.offPosZ;
        this.currentGroupScript.rndPosMinX = myTempScript.rndPosMinX;
        this.currentGroupScript.rndPosMinY = myTempScript.rndPosMinY;
        this.currentGroupScript.rndPosMinZ = myTempScript.rndPosMinZ;
        this.currentGroupScript.rndPosMaxX = myTempScript.rndPosMaxX;
        this.currentGroupScript.rndPosMaxY = myTempScript.rndPosMaxY;
        this.currentGroupScript.rndPosMaxZ = myTempScript.rndPosMaxZ;
        this.currentGroupScript.offRotX = myTempScript.offRotX;
        this.currentGroupScript.offRotY = myTempScript.offRotY;
        this.currentGroupScript.offRotZ = myTempScript.offRotZ;
        this.currentGroupScript.rndRotMinX = myTempScript.rndRotMinX;
        this.currentGroupScript.rndRotMinY = myTempScript.rndRotMinY;
        this.currentGroupScript.rndRotMinZ = myTempScript.rndRotMinZ;
        this.currentGroupScript.rndRotMaxX = myTempScript.rndRotMaxX;
        this.currentGroupScript.rndRotMaxY = myTempScript.rndRotMaxY;
        this.currentGroupScript.rndRotMaxZ = myTempScript.rndRotMaxZ;
        this.currentGroupScript.scaleUniform = myTempScript.scaleUniform;
        this.currentGroupScript.offSclX = myTempScript.offSclX;
        this.currentGroupScript.offSclY = myTempScript.offSclY;
        this.currentGroupScript.offSclZ = myTempScript.offSclZ;
        this.currentGroupScript.rndSclMinX = myTempScript.rndSclMinX;
        this.currentGroupScript.rndSclMinY = myTempScript.rndSclMinY;
        this.currentGroupScript.rndSclMinZ = myTempScript.rndSclMinZ;
        this.currentGroupScript.rndSclMaxX = myTempScript.rndSclMaxX;
        this.currentGroupScript.rndSclMaxY = myTempScript.rndSclMaxY;
        this.currentGroupScript.rndSclMaxZ = myTempScript.rndSclMaxZ;
    }

    //********************************************************************************************
    //*** Update Prefab
    //********************************************************************************************
    public virtual void updatePrefab(object _release)
    {
        Undo.RegisterSceneUndo("GeoPainter Update Prefab");
        foreach (var element in this.currentGroupScript.myPointsList)
        {
             //EditorUtility.ReconnectToLastPrefab(element.go);
            PrefabUtility.ReconnectToLastPrefab(element.go);
            //EditorUtility.ResetGameObjectToPrefabState(element.go);
            PrefabUtility.ResetToPrefabState(element.go);
        }
    }

    //********************************************************************************************
    //*** Replace Prefab
    //********************************************************************************************	
    public virtual void replacePrefab()
    {
        Undo.RegisterSceneUndo("Replace Prefab");
        foreach (var element in this.currentGroupScript.myPointsList)
        {
            UnityEngine.Object.DestroyImmediate(element.go);
            int myRandom = Random.Range(0, (int) this.currentGroupScript.myLibraryBuiltIn.Count);
            var objToInst = this.currentGroupScript.myLibraryBuiltIn[UnityScript.Lang.UnityBuiltins.parseInt(myRandom)];
            //var myNewObject = EditorUtility.InstantiatePrefab(objToInst);
            UnityEngine.GameObject myNewObject = (GameObject)PrefabUtility.InstantiatePrefab(objToInst);

            myNewObject.transform.position = element.pos;
            myNewObject.transform.rotation = Quaternion.identity;
            
            //{
            //    var _8 = element.pos;
            //    var _9 = myNewObject.transform;
            //    _9.position = _8;
            //}

            //{
            //    Quaternion _10 = Quaternion.identity;
            //    var _11 = myNewObject.transform;
            //    _11.rotation = _10;
            //}
            element.scale = myNewObject.transform.localScale;
            element.go = myNewObject;
            this.randomizeSolo(element);
            if (currentGroup.transform.childCount == 0)
            {
                currentGroup.transform.position = myNewObject.transform.position;
                myNewObject.transform.parent = currentGroup.transform;
            }
            else
            {
                myNewObject.transform.parent = currentGroup.transform;
            }
            //if (this.currentGroup.transform.childCount == 0)
            //{

            //    {
            //        var _12 = myNewObject.transform.position;
            //        var _13 = this.currentGroup.transform;
            //        _13.position = _12;
            //    }

            //    {
            //        var _14 = this.currentGroup.transform;
            //        var _15 = myNewObject.transform;
            //        _15.parent = _14;
            //    }
            //}
            //else
            //{

            //    {
            //        var _16 = this.currentGroup.transform;
            //        var _17 = myNewObject.transform;
            //        _17.parent = _16;
            //    }
            //}
        }
    }

    //********************************************************************************************
    //*** Randomize transform
    //********************************************************************************************
    public virtual void randomize()
    {
        Undo.RegisterSceneUndo("GeoPainter Randomize");
        Random.seed = (int) this.currentGroupScript.rndSeed;
        foreach (var element in this.currentGroupScript.myPointsList)
        {
            this.randomizeSolo(element);
        }
    }

    //********************************************************************************************
    //*** Randomize transform solo
    //********************************************************************************************	
    public virtual void randomizeSolo(GeoPainterPoint element)
    {
         //Random.seed = currentGroupScript.rndSeed;
        Transform obj = element.go.transform;
        Quaternion myRot = Quaternion.identity;
        if (element.useNormal) // HACK
        {
             //myRot = Quaternion.LookRotation(hit.normal);
            myRot = Quaternion.FromToRotation(obj.up, element.normal) * obj.rotation;
        }
        obj.position = element.pos;
        obj.rotation = myRot;
        obj.localScale = element.scale;
        //Position
        var tmpPosX = this.currentGroupScript.offPosX + Random.Range(this.currentGroupScript.rndPosMinX, this.currentGroupScript.rndPosMaxX);
        var tmpPosY = this.currentGroupScript.offPosY + Random.Range(this.currentGroupScript.rndPosMinY, this.currentGroupScript.rndPosMaxY);
        var tmpPosZ = this.currentGroupScript.offPosZ + Random.Range(this.currentGroupScript.rndPosMinZ, this.currentGroupScript.rndPosMaxZ);
        obj.Translate((float) tmpPosX, (float) tmpPosY, (float) tmpPosZ);
        //Rotation
        var tmpRotX = this.currentGroupScript.offRotX + Random.Range(this.currentGroupScript.rndRotMinX, this.currentGroupScript.rndRotMaxX);
        var tmpRotY = this.currentGroupScript.offRotY + Random.Range(this.currentGroupScript.rndRotMinY, this.currentGroupScript.rndRotMaxY);
        var tmpRotZ = this.currentGroupScript.offRotZ + Random.Range(this.currentGroupScript.rndRotMinZ, this.currentGroupScript.rndRotMaxZ);
        obj.Rotate(tmpRotX, tmpRotY, tmpRotZ);
        //Scale
        var tmpSclX = this.currentGroupScript.offSclX + Random.Range(this.currentGroupScript.rndSclMinX, this.currentGroupScript.rndSclMaxX);
        var tmpSclY = this.currentGroupScript.offSclY + Random.Range(this.currentGroupScript.rndSclMinY, this.currentGroupScript.rndSclMaxY);
        var tmpSclZ = this.currentGroupScript.offSclZ + Random.Range(this.currentGroupScript.rndSclMinZ, this.currentGroupScript.rndSclMaxZ);
        if (this.currentGroupScript.scaleUniform == false) // HACK
        {
            obj.localScale = obj.localScale + new Vector3(tmpSclX, tmpSclY, tmpSclZ);
        }
        else
        {
            obj.localScale = obj.localScale + new Vector3(tmpSclX, tmpSclX, tmpSclX);
        }
    }

    //********************************************************************************************
    //*** Paint
    //********************************************************************************************
    public virtual void paint()//EditorUtility.ReconnectToLastPrefab(newObj);
    {
        var myTarget = (GeoPainter) target;
        RaycastHit hit = default(RaycastHit);
        
        if ((this.currentGroupScript.myLibraryBuiltIn.Count == 0) || (this.currentGroupScript.myLibraryBuiltIn[0] == null))
        {
            return;
        }
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        object layerMask = 1 << myTarget.paintLayer;
        if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity, (int) layerMask))
        {
            GameObject newObj = null;
            Quaternion myRot;
            float dist = Mathf.Infinity;
            GameObject objToInst = null;
            //Spray
            if (myTarget.mySpray > 0)
            {
                var randomCircle = ( Random.insideUnitCircle) * myTarget.mySpray;
                Vector3 rayDirection = (hit.point + new Vector3(randomCircle.x, 0, randomCircle.y)) - ray.origin;
                RaycastHit newHit = default(RaycastHit);
                if (Physics.Raycast(ray.origin, rayDirection, out newHit, Mathf.Infinity, (int) layerMask))
                {
                    hit = newHit;
                }
            }
            //Check Distance
            if ((this.currentGroup.transform.childCount != 0))
            {
                foreach (var obj in this.myObjToInstArray)
                {
                    float tempDist = Vector3.Distance(hit.point, obj.transform.position);
                    if (tempDist < dist)
                    {
                        dist = tempDist;
                    }
                }
            }
            if (dist >= myTarget.myDistance)
            {
                 //Biblio Method
                if (myTarget.bibSortIndex == 0)
                {
                     //Random
                    int myRandom = Random.Range(0, (int) this.currentGroupScript.myLibraryBuiltIn.Count);
                    objToInst = this.currentGroupScript.myLibraryBuiltIn[UnityScript.Lang.UnityBuiltins.parseInt(myRandom)];
                }
                if (myTarget.bibSortIndex == 1)
                {
                    objToInst = this.currentGroupScript.myLibraryBuiltIn[myTarget.bibSoloSelect];
                }
                //Check is we're using normal placement
                myRot = Quaternion.identity;
                if (myTarget.useNormal) // HACK
                {
                     //myRot = Quaternion.LookRotation(hit.normal);
                    myRot = ( Quaternion.FromToRotation(objToInst.transform.up, hit.normal)) * objToInst.transform.rotation;
                }
                Undo.RegisterSceneUndo("GeoPainter Paint Add");
                //Create the Object
                //newObj = EditorUtility.InstantiatePrefab(objToInst);
                newObj = (GameObject)PrefabUtility.InstantiatePrefab(objToInst);
                newObj.transform.position = hit.point;
                newObj.transform.rotation = myRot;
                myObjToInstArray.Add(newObj);
                //{
                //    Vector3 _18 = hit.point;
                //    var _19 = newObj.transform;
                //    _19.position = _18;
                //}

                //{
                //    var _20 = myRot;
                //    var _21 = newObj.transform;
                //    _21.rotation = (Quaternion) _20; // HACK
                //}
                //this.myObjToInstArray.Add(newObj);
                //Update Points Array
                this.currentGroupScript.addObject(newObj, hit.point, newObj.transform.localScale, hit.normal, myTarget.useNormal);
                //Update Position Pivot
                if (currentGroup.transform.childCount == 0)
                {
                    currentGroup.transform.position = newObj.transform.position;
                    newObj.transform.parent = currentGroup.transform;
                }
                else
                {
                    newObj.transform.parent = currentGroup.transform;
                }
                randomizeSolo(currentGroupScript.myPointsList[currentGroupScript.myPointsList.Count - 1]);
                //EditorUtility.ReconnectToLastPrefab(newObj);

                //if (this.currentGroup.transform.childCount == 0)
                //{

                //    {
                //        var _22 = newObj.transform.position;
                //        var _23 = this.currentGroup.transform;
                //        _23.position = _22;
                //    }

                //    {
                //        var _24 = this.currentGroup.transform;
                //        var _25 = newObj.transform;
                //        _25.parent = _24;
                //    }
                //}
                //else
                //{

                //    {
                //        var _26 = this.currentGroup.transform;
                //        var _27 = newObj.transform;
                //        _27.parent = _26;
                //    }
                //}
                //this.randomizeSolo(this.currentGroupScript.myPointsList[((int) this.currentGroupScript.myPointsList.Count) - 1]);
            }
        }
    }

    //********************************************************************************************
    //*** Paint Remove
    //********************************************************************************************
    public virtual void paintRemove()
    {
        var myTarget = (GeoPainter) target;

        RaycastHit hit = default(RaycastHit);
        Undo.RegisterSceneUndo("GeoPainter Paint Remove");
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity))
        {
            /*
           //currentGroup
           if(hit.transform.IsChildOf(currentGroup.transform))
           {
               DestroyImmediate(hit.collider.gameObject);
           }
           */
            for (int i = 0; i < currentGroupScript.myPointsList.Count; i++)
            {
                var element = currentGroupScript.myPointsList[i];
                if (Vector3.Distance(hit.point, element.go.transform.position) <= myTarget.myDelete)
                {
                    DestroyImmediate(element.go);
                    currentGroupScript.myPointsList.RemoveAt(i);
                }
            }
            //var i = 0;
            //while (i < this.currentGroupScript.myPointsList.Count)
            //{
            //    var element = this.currentGroupScript.myPointsList[i];
            //    if (Vector3.Distance(hit.point, element.go.transform.position) <= myTarget.myDelete)
            //    {
            //        UnityEngine.Object.DestroyImmediate(element.go);
            //        this.currentGroupScript.myPointsList.RemoveAt(i);
            //    }
            //    i++;
            //}
        }
    }

    //********************************************************************************************
    //*** Add a group
    //********************************************************************************************
    public virtual void addGroup()
    {
        var myTarget = (GeoPainter) target;
        Undo.RegisterSceneUndo("GeoPainter New Group");
        var go = new GameObject("GeoPainter_Group_" + myTarget.nbrGroupsCreated.ToString());
        myTarget.nbrGroupsCreated = ((int)myTarget.nbrGroupsCreated) + 1;
        go.AddComponent<GeoPainterGroup>();
        this.myGroups.Add(go);
        myTarget.groupSelIndex = this.myGroups.Count;
        this.currentGroup = this.myGroups[((int) (((int)myTarget.groupSelIndex) - 1))];
        //this.currentGroupScript = this.currentGroup.GetComponent("GeoPainterGroup");
        this.currentGroupScript = this.currentGroup.GetComponent<GeoPainterGroup>();
        //this.myLibrary = new object[] {this.currentGroupScript.myLibraryBuiltIn};
        this.myLibrary = new List<GameObject>(this.currentGroupScript.myLibraryBuiltIn);
    }

    //********************************************************************************************
    //*** Remove a group
    //********************************************************************************************
    public virtual void removeGroup(object _index, bool _release)
    {
        var myTarget = (GeoPainter) target;
        Undo.RegisterSceneUndo("Remove Group");
        object index = ((int) _index) - 1;
        if (_release == false)
        {
            object go = this.myGroups[((int) index)];
            UnityEngine.Object.DestroyImmediate((UnityEngine.Object) go);
        }
        this.myGroups.RemoveAt((int) index);
        myTarget.groupSelIndex = this.myGroups.Count;
        if (this.myGroups.Count != 0)
        {
            this.currentGroup = this.myGroups[((int) (((int)myTarget.groupSelIndex) - 1))];
            //this.currentGroupScript = this.currentGroup.GetComponent("GeoPainterGroup");
            this.currentGroupScript = this.currentGroup.GetComponent<GeoPainterGroup>();
            //this.myLibrary = new object[] {this.currentGroupScript.myLibraryBuiltIn};
            this.myLibrary = new List<GameObject>(this.currentGroupScript.myLibraryBuiltIn);
        }
        else
        {
            this.addGroup();
        }
    }

    //********************************************************************************************
    //*** HANDLES
    //********************************************************************************************
    public virtual void drawHandles()
    {
        var myTarget = (GeoPainter) target;
        RaycastHit hit = default(RaycastHit);
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        var layerMask = 1 << myTarget.paintLayer;
        if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity, (int) layerMask))
        {
            if ((myTarget.mySpray != 0))
            {
                Handles.color = Color.green;
                Handles.CircleCap(1, hit.point, Quaternion.LookRotation(hit.normal), (float)myTarget.mySpray);
                //Handles.CircleHandleCap(1, hit.point, Quaternion.LookRotation(hit.normal), (float)myTarget.mySpray, EventType.Ignore);
            }
            if ((myTarget.myDistance != 0))
            {
                Handles.color = Color.blue;
                Handles.CircleCap(0, hit.point, Quaternion.LookRotation(hit.normal), (float)myTarget.myDistance);
            }
            if ((myTarget.myDelete != 0))
            {
                Handles.color = Color.red;
                Handles.CircleCap(0, hit.point, Quaternion.LookRotation(hit.normal), (float)myTarget.myDelete);
            }
        }
    }

    //********************************************************************************************
    //*** ON SCENE GUI
    //********************************************************************************************
    public virtual void OnSceneGUI()//if(Event.current.type == EventType.MouseDown && Event.current.button == 0)
    {
        var myTarget = (GeoPainter) target;
        int ctrlID = GUIUtility.GetControlID(GeoPainterEditor.appTitle.GetHashCode(), FocusType.Passive);
        if (this.isPainting)
        {
            Event e = Event.current;
            //Hanldes
            this.drawHandles();
            if ((e.keyCode == KeyCode.D) && !e.shift)
            {
                myTarget.myDistance = ((float)myTarget.myDistance) + 0.01f;
                this.Repaint();
                HandleUtility.Repaint();
            }
            if ((e.keyCode == KeyCode.D) && e.shift)
            {
                myTarget.myDistance = ((float)myTarget.myDistance) - 0.01f;
                if (myTarget.myDistance <= 0)
                {
                    myTarget.myDistance = 0;
                }
                this.Repaint();
                HandleUtility.Repaint();
            }
            if ((e.keyCode == KeyCode.S) && !e.shift)
            {
                myTarget.mySpray = ((float)myTarget.mySpray) + 0.01f;
                this.Repaint();
                HandleUtility.Repaint();
            }
            if ((e.keyCode == KeyCode.S) && e.shift)
            {
                myTarget.mySpray = ((float)myTarget.mySpray) - 0.01f;
                if (myTarget.mySpray <= 0)
                {
                    myTarget.mySpray = 0;
                }
                this.Repaint();
                HandleUtility.Repaint();
            }
            if ((e.keyCode == KeyCode.C) && !e.shift)
            {
                myTarget.myDelete = ((float)myTarget.myDelete) + 0.01f;
                this.Repaint();
                HandleUtility.Repaint();
            }
            if ((e.keyCode == KeyCode.C) && e.shift)
            {
                myTarget.myDelete = ((float)myTarget.myDelete) - 0.01f;
                if (myTarget.myDelete <= 0)
                {
                    myTarget.myDelete = 0;
                }
                this.Repaint();
                HandleUtility.Repaint();
            }
            switch (e.type)
            {
                case EventType.mouseDrag:
                    if (e.control)
                    {
                        this.paint();
                        e.Use();
                    }
                    else
                    {
                        if (e.shift)
                        {
                            this.paintRemove();
                            e.Use();
                        }
                    }
                    break;
                case EventType.mouseUp:
                    if (e.control)
                    {
                        this.paint();
                        //	Undo.RegisterUndo(myObjToInstArray.ToBuiltin(GameObject),"New Paint Object");
                        // HACK this might be missing?
                        //this.myObjToInstArray = new object[0];
                        this.myObjToInstArray = new List<GameObject>();
                        e.Use();
                    }
                    else
                    {
                        if (e.shift)
                        {
                            this.paintRemove();
                            e.Use();
                        }
                    }
                    break;
                case EventType.layout:
                    HandleUtility.AddDefaultControl(ctrlID);
                    break;
                case EventType.MouseMove:
                    HandleUtility.Repaint();
                    break;
            }
        }
    }

    //********************************************************************************************
    //*** Menu Item
    //********************************************************************************************
    [UnityEditor.MenuItem("GameObject/Add GeoPainter")]
    public static void CreateMenu()
    {
        // Get existing open window or if none, make a new one:
        if (!GameObject.Find("GeoPainterSys"))
        {
            var go = new GameObject("GeoPainterSys");
            go.AddComponent<GeoPainter>();
            Selection.activeGameObject = go;
        }
    }

    public GeoPainterEditor()
    {
        this.showGroups = true;
        this.showPaint = true;
        this.showBiblio = true;
        this.showRandom = true;
        //this.myCopyFromPop = new object[0];
        this.myCopyFromPop = new List<string>();
    }

    static GeoPainterEditor()
    {
        GeoPainterEditor.appTitle = "Geo Painter Tools";
    }

}