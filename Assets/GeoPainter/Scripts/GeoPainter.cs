using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GeoPainter : MonoBehaviour
{
    public int paintLayer;

    public object[] myGroups;

    [SerializeField]
    public List<GameObject> myGroupsBuiltIn;

    public int nbrGroupsCreated;

    public float myDistance;

    public float mySpray;

    public float myDelete;

    public bool useNormal;

    public int groupSelIndex;

    public string[] bibSortMethod;

    public int bibSortIndex;

    public int bibSoloSelect;

    public bool rndAuto;

    public GeoPainter()
    {
        this.myGroups = new object[0];
        this.myDistance = 0.5f;
        this.mySpray = 1;
        this.myDelete = 2;
        this.useNormal = true;
        this.groupSelIndex = 1;
        this.bibSortMethod = new string[] {"Random", "Solo"};
    }

}