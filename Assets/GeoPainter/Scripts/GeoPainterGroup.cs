using System.Collections.Generic;
using UnityEngine;
using System.Collections;

[System.Serializable]
public class GeoPainterGroup : MonoBehaviour
{
    [SerializeField]
    public List<GameObject> myLibraryBuiltIn;

    public int rndSeed;

    [UnityEngine.SerializeField]
    public System.Collections.Generic.List<GeoPainterPoint> myPointsList;

    //Position
    public float offPosX;

    public float offPosY;

    public float offPosZ;

    public float rndPosMinX;

    public float rndPosMinY;

    public float rndPosMinZ;

    public float rndPosMaxX;

    public float rndPosMaxY;

    public float rndPosMaxZ;

    //Rotation
    public float offRotX;

    public float offRotY;

    public float offRotZ;

    public float rndRotMinX;

    public float rndRotMinY;

    public float rndRotMinZ;

    public float rndRotMaxX;

    public float rndRotMaxY;

    public float rndRotMaxZ;

    //Scale
    public bool scaleUniform;

    public float offSclX;

    public float offSclY;

    public float offSclZ;

    public float rndSclMinX;

    public float rndSclMinY;

    public float rndSclMinZ;

    public float rndSclMaxX;

    public float rndSclMaxY;

    public float rndSclMaxZ;

    public virtual void addObject(GameObject _go, Vector3 _pos, Vector3 _scale, Vector3 _normal, bool _useNormal)
    {
        GeoPainterPoint myNewPoint = new GeoPainterPoint();
        myNewPoint.go = _go;
        myNewPoint.pos = _pos;
        myNewPoint.scale = _scale;
        myNewPoint.normal = _normal;
        myNewPoint.useNormal = _useNormal;
        this.myPointsList.Add(myNewPoint);
    }

    public GeoPainterGroup()
    {
        this.rndSeed = 1;
        this.myPointsList = new List<GeoPainterPoint>();
        this.myLibraryBuiltIn = new List<GameObject>();
    }

}