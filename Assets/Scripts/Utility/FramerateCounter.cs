﻿using TMPro;
using UnityEngine;


[RequireComponent(typeof(TextMeshProUGUI))]
public class FramerateCounter : MonoBehaviour
{
    public string ToggleKey = "f";


    private int frames_;
    private Timer timer_;
    private TextMeshProUGUI text_;


	// Use this for initialization
    private void Start ()
    {
		timer_ = new Timer(1f);
        text_ = GetComponent<TextMeshProUGUI>();
        text_.text = "FPS: ??";
    }
	

	// Update is called once per frame
    private void Update ()
	{
	    var deltaTime = TimeManager.UnscaledDeltaTime;

	    ++frames_;

	    if (Input.GetKeyDown(ToggleKey))
	    {
	        text_.enabled = !text_.enabled;
	    }

	    if (timer_.Update(deltaTime))
	    {
	        text_.text = "FPS: " + frames_;
	        frames_ = 0;
            timer_.ResetToSurplus();
	    }
	}
}
