﻿using UnityEngine;

public class MathFunctions
{
    public static float NormalizeAngle(float angle)
    {
        var normalizedDeg = angle % 360f;

        if (normalizedDeg < 0f)
            normalizedDeg += 360f;
        else if (normalizedDeg >= 360f)
            normalizedDeg -= 360f;

        return normalizedDeg;
    }


    public static float NormalizeAngle180(float angle)
    {
        var normalizedDeg = angle % 360f;

        if (normalizedDeg < -180f)
            normalizedDeg += 360f;
        else if (normalizedDeg >= 180f)
            normalizedDeg -= 360f;

        return normalizedDeg;
    }


    public static float AngleYFromVector(Vector3 vector)
    {
        return Mathf.Rad2Deg * Mathf.Atan2(vector.x, vector.z);
    }


    public static float AngleXFromVector(Vector3 vector)
    {
        return Mathf.Rad2Deg * Mathf.Atan2(-vector.y, vector.z); // TODO: Check if y and z should switch place
    }
}