﻿using UnityEngine;

public class TimedParticleDestroyer : MonoBehaviour
{
    private void Update()
    {
        if (!GetComponent<ParticleSystem>().isEmitting)
        {
            Destroy(gameObject);
        }
    }
}