﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VignetteEffect : MonoBehaviour
{

    public Shader Shader;
    private Material material_;

    public float Intensity = 1;
    public float Smothness = 10;
    public float Roundness = 1;
    public float Rounded = 1;
    public Color Color = Color.black;

	// Use this for initialization
	void Start ()
    {
		material_ = new Material(Shader);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        material_.SetFloat("_Intensity", Intensity);
        material_.SetFloat("_Smoothness", Smothness);
        material_.SetFloat("_Roundness", Roundness);
        material_.SetFloat("_Rounded", Rounded);
        material_.SetColor("_Color", Color);
        Graphics.Blit(source, destination, material_);
    }


}
