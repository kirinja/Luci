﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ScreenSpaceEdgeDetection : MonoBehaviour 
{
	public const string PROP_EDGE_SIZE = "_EdgeSize";
	public const string PROP_EDGE_POWER = "_EdgePower";

	public Shader Shader;

	public float edgeSize = 1f;
	public float edgePower = 3f;

	private Material shaterMat_;

	void OnEnable() 
	{
		shaterMat_ = new Material(Shader);
	}

	void OnDisable() 
	{
		DestroyImmediate(shaterMat_);
	}
	
	void OnRenderImage(RenderTexture src, RenderTexture dst) 
	{
		shaterMat_.SetFloat(PROP_EDGE_SIZE, edgeSize);
		shaterMat_.SetFloat(PROP_EDGE_POWER, edgePower);
		
		// var rt0 = RenderTexture.GetTemporary(src.width, src.height, 0, RenderTextureFormat.ARGB32);
		// var rt1 = RenderTexture.GetTemporary(src.width, src.height, 0, RenderTextureFormat.ARGB32);
		
		Graphics.Blit(src, dst, shaterMat_);
		//Swap(ref rt0, ref rt1);

		/* 
		if (nPapers > 0) {
			Graphics.Blit(rt0, rt1, shaterMat_, PASS_EDGE);
			Swap(ref rt0, ref rt1);
			
			for (var i = 0; i < nPapers; i++) {
				paperDataset[i].SetProps(shaterMat_);
				Graphics.Blit(rt0, (i == (nPapers - 1) ? dst : rt1), shaterMat_, PASS_PAPER);
				Swap(ref rt0, ref rt1);
			}
		} else {
			Graphics.Blit(rt0, dst, shaterMat_, PASS_EDGE);
		}*/

		//RenderTexture.ReleaseTemporary(rt0);
		//RenderTexture.ReleaseTemporary(rt1);
	}
}
