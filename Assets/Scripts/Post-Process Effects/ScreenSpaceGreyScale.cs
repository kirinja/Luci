﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ScreenSpaceGreyScale : MonoBehaviour
{

    public Shader Shader;

    private Material material_;
    [Range(0, 1)]
    public float Magnitude;
    [Range(1, 10)]
    public float GreyIntensity = 1;
    [Range(1, 1000)]
    public float WobbleEffect;
    public Texture Texture;
    public Color Color;

    [HideInInspector]
    public float Intensity = 1;    // x
    [HideInInspector]
    public float Smoothness = 10;  // y
    [HideInInspector]
    public float Roundness = 1;    // z
    [HideInInspector]
    public float Rounded = 1;      // w

    // Use this for initialization
    void Start ()
    {
		material_ = new Material(Shader);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        // switch between color and greyscale + displacement wobble effect
        material_.SetFloat("_Magnitude", Magnitude);
        material_.SetTexture("_DisplacementTex", Texture);
        material_.SetFloat("_WobbleEffect", WobbleEffect);
        material_.SetFloat("_GreyIntensity", GreyIntensity);
        material_.SetColor("_Color", Color);

        //vignette effect setting
        material_.SetFloat("_Intensity", Intensity);
        material_.SetFloat("_Smoothness", Smoothness);
        material_.SetFloat("_Roundness", Roundness);
        material_.SetFloat("_Rounded", Rounded);


        Graphics.Blit(source, destination, material_);
    }


}
