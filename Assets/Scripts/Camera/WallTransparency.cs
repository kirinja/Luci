﻿using UnityEngine;
using System.Collections.Generic;

public class WallTransparency : MonoBehaviour
{
    public LayerMask OccluderMask;

    //This is the material with the Transparent/Diffuse With Shadow shader
    public Material HiderMaterial;

    public Vector3 TargetOffset;

    private Transform watchTarget_;
    

    private Dictionary<Renderer, Material[]> lastMaterials_;
    private Dictionary<Renderer, Color[]> lastColor_;
    private Dictionary<Renderer, Texture[]> lastTexture_;


    private void Start()
    {
        lastMaterials_ = new Dictionary<Renderer, Material[]>();
        lastColor_ = new Dictionary<Renderer, Color[]>();
        lastTexture_ = new Dictionary<Renderer, Texture[]>();

        watchTarget_ = GameObject.FindGameObjectWithTag("Player").transform;
        TargetOffset = new Vector3(0, 0.5f, 0);
    }

    private void Update()
    {
        // Restore all objects' materials and clear dictionaries
        foreach (var t in lastMaterials_.Keys)
        {
            t.GetComponent<Renderer>().materials = lastMaterials_[t];

            for (var i = 0; i < lastMaterials_[t].Length; ++i)
            {
                if (!t.GetComponent<Renderer>().materials[i].HasProperty("_Color")) continue;
                t.GetComponent<Renderer>().materials[i].color = lastColor_[t][i];
                t.GetComponent<Renderer>().materials[i].mainTexture = lastTexture_[t][i];
            }
        }
        lastMaterials_.Clear();
        lastColor_.Clear();
        lastTexture_.Clear();

        //Cast a ray from this object's transform the the watch target's transform.
        var hits = Physics.RaycastAll(
            watchTarget_.transform.position,
            (transform.position) - (watchTarget_.transform.position + TargetOffset),
            Vector3.Distance((watchTarget_.transform.position + TargetOffset), (transform.position)),
            OccluderMask
        );

        //Loop through all overlapping objects and disable their mesh renderer
        if (hits.Length <= 0) return;
        foreach (var hit in hits)
        {
            foreach (var ren in hit.collider.GetComponentsInChildren<Renderer>())
            {
                if (lastMaterials_.ContainsKey(ren)) continue;

                var materials = ren.materials;

                lastMaterials_[ren] = ren.materials;
                lastColor_.Add(ren, new Color[materials.Length]);
                lastTexture_.Add(ren, new Texture[materials.Length]);
                for (var index = 0; index < materials.Length; ++index)
                {
                    if (!materials[index].HasProperty("_Color")) continue;

                    lastColor_[ren][index] = ren.materials[index].color;
                    lastTexture_[ren][index] = ren.materials[index].mainTexture;

                    var c = lastColor_[ren][index];

                    materials[index] =
                        new Material(HiderMaterial)
                        {
                            color = new Color(c.r, c.g, c.b, HiderMaterial.color.a),
                            mainTexture = lastTexture_[ren][index]
                        };
                }
                ren.materials = materials;
            }
        }
    }
}