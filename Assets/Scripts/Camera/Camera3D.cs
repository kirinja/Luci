﻿using NaughtyAttributes;
using UnityEngine;

/// <summary>
/// This is a fairly simple camera script that makes the camera orbit around a Target
/// </summary>
public class Camera3D : MonoBehaviour
{
    [Tooltip("Camera mouse sensitivity")] public Vector2 MouseSensitivity = new Vector2(40, 40);
    [Tooltip("Camera gamepad sensitivity")] public Vector2 GamepadSensitivity = new Vector2(40, 40);

    public float OffsetY = 0.1f;
    public float VerticalStartAngle = 15f;

    [MinValue(0f)] public float AutoFollowSpeed = 1f;
    [MinValue(0f)] public float LockFollowSpeed = 400f;
    [MinValue(0f), MaxValue(180f)] public float BackwardsAngleMargin = 10f;

    [Tooltip("The minimum angle the camera can move vertically, measured in degrees")]
    public float ClampCameraMin = -15f;

    [Tooltip("The maximum angle the camera can move vertically, measured in degrees")]
    public float ClampCameraMax = 40f;

    [Tooltip("The preferred distance of the camera, relative to the Target, in Unity units")] [MinValue(0f)]
    public float PreferredDistance = 10.0f;

    [Tooltip("The speed which the Camera zooms in if an object is obstructing it")]
    public float CameraAccelerationIn = 4.0f;

    [Tooltip("The speed which the Camera zooms out if there is a clear path to do so")]
    public float CameraAccelerationOut = 2.0f;

    [Tooltip("What should the Camera collision detecting check against")] public LayerMask LayerMask;

    public bool GamepadInvertX;
    public bool GamepadInvertY;
    public bool MouseInvertX;
    public bool MouseInvertY;


    private Transform target_;
    private float horizontalRotation_;
    private float verticalRotation_;
    private float desiredHorizontalRotation_;
    private float actualDistance_;
    private bool isFollowing_;
    private LockOn lockOn_;
    private Vector3 previousTargetPosition_;


    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        target_ = GameObject.FindGameObjectWithTag("Player").transform;
        previousTargetPosition_ = target_.position;

        // Getting the angles to start from
        var angles = target_.eulerAngles;
        desiredHorizontalRotation_ = angles.y;
        horizontalRotation_ = desiredHorizontalRotation_;
        verticalRotation_ = angles.x + VerticalStartAngle;
        actualDistance_ = PreferredDistance;
        isFollowing_ = true;

        lockOn_ = FindObjectOfType<LockOn>();
        lockOn_.OnLockOn += OnLockOn;
    }


    private void LateUpdate()
    {
        if (!target_ || Pause.Instance.Paused) return;

        HandleInput();

        AutoFollow();

        horizontalRotation_ = MathFunctions.NormalizeAngle(horizontalRotation_);
        verticalRotation_ = MathFunctions.NormalizeAngle(Mathf.Clamp(MathFunctions.NormalizeAngle180(verticalRotation_),
            ClampCameraMin, ClampCameraMax));


        // I don't like calling Move twice, but if I don't do it before UpdateDistance it will use the wrong position. That would cause clipping to occur, if only for one frame.
        Move();
        UpdateDistance();
        Move();
    }


    private void Move()
    {
        var rotation = Quaternion.Euler(verticalRotation_, horizontalRotation_, 0);

        transform.rotation = rotation;
        transform.position = target_.position + Vector3.up * OffsetY + rotation * (Vector3.back * actualDistance_);
    }


    private void HandleInput()
    {
        var deltaTime = Time.unscaledDeltaTime;

        var gamepadInput =
            new Vector2(
                Input.GetAxisRaw("Right Horizontal") * GamepadSensitivity.x * deltaTime * (GamepadInvertX ? -1 : 1),
                Input.GetAxisRaw("Right Vertical") * GamepadSensitivity.y * deltaTime * (GamepadInvertY ? -1 : 1));
        var mouseInput =
            new Vector2(Input.GetAxisRaw("Mouse X") * MouseSensitivity.x * deltaTime * (MouseInvertX ? -1 : 1),
                Input.GetAxisRaw("Mouse Y") * MouseSensitivity.y * deltaTime * (MouseInvertY ? -1 : 1));

        if (gamepadInput.magnitude > 0f || mouseInput.magnitude > 0f)
        {
            isFollowing_ = false;
        }
        else
        {
            return;
        }

        var input = gamepadInput.magnitude > mouseInput.magnitude ? gamepadInput : mouseInput;

        horizontalRotation_ += input.x;
        verticalRotation_ += input.y;
    }


    private void AutoFollow()
    {
        var currentTargetHorizontal = new Vector3(target_.position.x, 0f, target_.position.z);
        var previousTargetHorizontal = new Vector3(previousTargetPosition_.x, 0f, previousTargetPosition_.z);
        var targetMovement = (currentTargetHorizontal - previousTargetHorizontal).magnitude;
        previousTargetPosition_ = target_.position;

        if (!isFollowing_) return;

        desiredHorizontalRotation_ = lockOn_.LockedOn
            ? MathFunctions.AngleYFromVector(lockOn_.LockDirection)
            : target_.eulerAngles.y;

        var rotationAngle = desiredHorizontalRotation_ - horizontalRotation_;

        rotationAngle = MathFunctions.NormalizeAngle180(rotationAngle);
        if (Mathf.Abs(rotationAngle) > 180 - BackwardsAngleMargin) return;

        var rotateAmount = (lockOn_.LockedOn ? LockFollowSpeed * Time.deltaTime : AutoFollowSpeed * targetMovement) *
                           Mathf.Sign(rotationAngle);
        if (Mathf.Abs(rotateAmount) > Mathf.Abs(rotationAngle))
        {
            rotateAmount = rotationAngle;
        }

        horizontalRotation_ += rotateAmount;
    }


    private void UpdateDistance()
    {
        var rayLength = PreferredDistance;

        RaycastHit rayHit;

        if (Physics.SphereCast(target_.position + Vector3.up * OffsetY, 0.15f,
            (transform.position - (target_.position + Vector3.up * OffsetY)).normalized, out rayHit,
            rayLength, LayerMask))
        {
            Debug.DrawLine(target_.position + new Vector3(0, OffsetY, 0), transform.position, Color.magenta);

            actualDistance_ = rayHit.distance;
        }
        else
        {
            Debug.DrawLine(target_.position + new Vector3(0f, OffsetY, 0f), transform.position, Color.green);

            actualDistance_ = PreferredDistance;
        }
    }


    private void OnLockOn(LockOn lockOn)
    {
        isFollowing_ = true;
    }
}