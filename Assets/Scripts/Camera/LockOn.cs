﻿using System.Collections.Generic;
using System.Linq;
using NaughtyAttributes;
using UnityEngine;

public class LockOn : MonoBehaviour
{
    [Required] public Transform Indicator;
    [Required] public Sprite SmallIndicator;
    [Required] public Sprite LargeIndicator;

    public delegate void LockDelegate(LockOn lockOn);

    public LockDelegate OnLockOn = delegate { };


    private List<EnemyAI> enemiesInSight_;

    private Controller3D refPlayerMovement_;

    private int targetIndex_;
    private EnemyAI currentTarget_;


    public bool LockedOn { get; private set; }


    public Vector3 LockDirection { get; private set; }


    public int TargetIndex
    {
        get { return targetIndex_; }
        set
        {
            if (value >= enemiesInSight_.Count || value < 0)
            {
                Debug.Log("value too high or too low " + value);
                targetIndex_ = 0;
            }
            else
            {
                Debug.Log("set value " + value);
                targetIndex_ = value;
            }
            currentTarget_ = enemiesInSight_[value];
        }
    }


    // Use this for initialization
    private void Start()
    {
        enemiesInSight_ = new List<EnemyAI>();
        refPlayerMovement_ = GameObject.FindGameObjectWithTag("Player").GetComponent<Controller3D>();
        targetIndex_ = 0;
    }


    private void Update()
    {
        if (Input.GetAxisRaw("Lock") > 0f)
        {
            if (!LockedOn && !currentTarget_)
            {
                LockDirection = refPlayerMovement_.transform.forward;
            }
            else if (currentTarget_)
            {
                LockDirection = (currentTarget_.transform.position - refPlayerMovement_.transform.position)
                    .normalized;
            }
            if (!LockedOn)
            {
                LockedOn = true;
                OnLockOn(this);
            }
            refPlayerMovement_.LockDirection = LockDirection;

            Indicator.GetComponent<SpriteRenderer>().sprite = LargeIndicator;
        }
        else
        {
            LockedOn = false;

            Indicator.GetComponent<SpriteRenderer>().sprite = SmallIndicator;
        }
        refPlayerMovement_.Strafing = LockedOn;
    }


    // Update is called once per frame
    private void LateUpdate()
    {
        // set the collider where the player is
        transform.position = refPlayerMovement_.transform.position;

        if (currentTarget_)
        {
            Indicator.gameObject.SetActive(true);
            Indicator.position = currentTarget_.transform.position + Vector3.up * currentTarget_.LockOnIndicatorOffset;
        }
        else
        {
            Indicator.gameObject.SetActive(false);
        }
    }


    private void OnTriggerStay(Collider other)
    {
        var enemyAi = other.transform.GetComponent<EnemyAI>();
        var health = other.transform.GetComponent<CharacterHealth>();

        if (!enemyAi) return;

        var planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        // try to get renderer from parent, otherwise get children
        var render = enemyAi.transform.GetComponent<Renderer>();
        if (!render)
            render = enemyAi.transform.GetComponentInChildren<Renderer>();

        var isVisible = GeometryUtility.TestPlanesAABB(planes, render.bounds);
        if (!isVisible)
        {
            if (enemiesInSight_.Contains(enemyAi))
            {
                health.OnDeath -= OnDeath;
                enemiesInSight_.Remove(enemyAi);
                targetIndex_ = enemiesInSight_.FindIndex(Equals);
            }
            if (enemyAi.Equals(currentTarget_))
            {
                targetIndex_ = 0;
                currentTarget_ = enemiesInSight_.Count > 0 ? enemiesInSight_[0] : null;
            }
        }
        else if (!enemiesInSight_.Contains(enemyAi) && health.IsAlive)
        {
            enemyAi.GetComponent<CharacterHealth>().OnDeath += OnDeath;
            enemiesInSight_.Add(enemyAi);
            if (!currentTarget_)
            {
                currentTarget_ = enemyAi;
                targetIndex_ = enemiesInSight_.Count - 1;
            }
        }
    }


    private void OnTriggerExit(Collider other)
    {
        var enemyAi = other.transform.GetComponent<EnemyAI>();
        if (enemyAi)
        {
            if (enemiesInSight_.Contains(enemyAi))
            {
                enemyAi.GetComponent<CharacterHealth>().OnDeath -= OnDeath;
                enemiesInSight_.Remove(enemyAi);
                targetIndex_ = enemiesInSight_.FindIndex(Equals);
            }
            if (enemyAi.Equals(currentTarget_))
            {
                targetIndex_ = 0;
                currentTarget_ = enemiesInSight_.Count > 0 ? enemiesInSight_.First() : null;
            }
        }
    }


    // on death we remove the object that died from the list
    private void OnDeath(CharacterHealth health)
    {
        var enemyAi = health.GetComponent<EnemyAI>();
        enemiesInSight_.Remove(enemyAi);
        targetIndex_ = enemiesInSight_.FindIndex(Equals);
        if (enemyAi.Equals(currentTarget_))
        {
            targetIndex_ = 0;
            currentTarget_ = enemiesInSight_.Count > 0 ? enemiesInSight_.First() : null;
        }
    }
}