﻿using UnityEngine;

public class TeleportCheat : MonoBehaviour
{
    public string TeleportKey = "g";

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(TeleportKey))
        {
            var player = GameObject.FindGameObjectWithTag("Player").transform;
            player.position = transform.position;
            player.rotation = transform.rotation;
        }
    }
}