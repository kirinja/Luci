﻿using UnityEngine;


public class ChristmasMaterialReplacer : ChristmasObject
{
    public Material[] RegularMaterials;
    public Material[] ChristmasMaterials;


    public override void UpdateChristmas(bool activated)
    {
        var ren = GetComponent<Renderer>();
        ren.materials = activated ? ChristmasMaterials : RegularMaterials;
    }
}