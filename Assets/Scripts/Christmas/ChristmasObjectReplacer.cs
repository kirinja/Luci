﻿using NaughtyAttributes;
using UnityEngine;

public class ChristmasObjectReplacer : ChristmasObject
{
    [Required] public Transform ChristmasTransform;
    [Required] public Transform RegularTransform;
    

    public override void UpdateChristmas(bool activated)
    {
        ChristmasTransform.gameObject.SetActive(activated);
        RegularTransform.gameObject.SetActive(!activated);
    }
}