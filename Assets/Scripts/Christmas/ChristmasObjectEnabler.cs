﻿using UnityEngine;

public class ChristmasObjectEnabler : ChristmasObject
{
    [Tooltip("True if object should be active when christmas mode is on. If false, the object will be active when christmas mode is off instead.")]
    public bool ActiveWhenChristmas = true;


    public override void UpdateChristmas(bool activated)
    {
        gameObject.SetActive(ActiveWhenChristmas ? ChristmasManager.Instance.Activated : !ChristmasManager.Instance.Activated);
    }
}
