﻿using UnityEngine;

public class ChristmasManager : MonoBehaviour
{
    private static ChristmasManager instance_;

    public bool Activated { get; private set; }

    public delegate void ChristmasChanged(bool activated);

    public ChristmasChanged OnChristmasChanged = delegate { };


    public static bool HasInstance
    {
        get { return instance_; }
    }


    public static ChristmasManager Instance
    {
        get
        {
            if (!instance_)
            {
                instance_ = FindObjectOfType<ChristmasManager>();
            }

            if (!instance_)
            {
                var go = new GameObject("Christmas Manager");
                instance_ = go.AddComponent<ChristmasManager>();
            }

            return instance_;
        }
    }


    private void Awake()
    {
        if (!instance_)
        {
            instance_ = this;
        }
        else if (instance_ != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(this);
    }


    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown("j"))
        {
            Activated = !Activated;
            OnChristmasChanged(Activated);
        }
    }
}