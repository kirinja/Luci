﻿using UnityEngine;


public abstract class ChristmasObject : MonoBehaviour
{
    public abstract void UpdateChristmas(bool activated);


    // Use this for initialization
    private void Start ()
    {
	    ChristmasManager.Instance.OnChristmasChanged += UpdateChristmas;
	    UpdateChristmas(ChristmasManager.Instance.Activated);
    }


    private void OnDestroy()
    {
        if (!ChristmasManager.HasInstance) return;

        ChristmasManager.Instance.OnChristmasChanged -= UpdateChristmas;
    }
}
