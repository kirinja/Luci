﻿using UnityEngine;


[RequireComponent(typeof(ParticleSystem))]
public class ChristmasParticleColorReplacer : ChristmasObject
{
    public Color RegularColor;
    public Color ChristmasColor;


	// Use this for initialization
    public override void UpdateChristmas(bool activated)
    {
        var ps = GetComponent<ParticleSystem>();
        var main = ps.main;
        main.startColor = activated ? ChristmasColor : RegularColor;
    }
}
