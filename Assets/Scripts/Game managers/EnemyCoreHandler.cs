﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyCoreHandler : MonoBehaviour
{
    // this keeps track of the cores we have defeated
    private static EnemyCoreHandler instance_;
    
    private List<string> defeatedCores_;


    public static EnemyCoreHandler Instance
    {
        get
        {
            if (instance_ == null)
                instance_= FindObjectOfType<EnemyCoreHandler>();
            if (instance_ == null)
            {
                var activeScene = SceneManager.GetActiveScene();
                var mainScene = SceneManager.GetSceneByName("Main");
                if (mainScene.IsValid())
                {
                    SceneManager.SetActiveScene(mainScene);
                }

                var go = new GameObject("Enemy Core Handler");

                SceneManager.SetActiveScene(activeScene);

                instance_ = go.AddComponent<EnemyCoreHandler>();
                //go.hideFlags = HideFlags.HideInHierarchy;
            }
            return instance_;
        }
    }

    private void Awake()
    {
        defeatedCores_ = new List<string>();
        var data = IO.Instance.LoadGame();

        if (data == null) return;
        
        if (data.DefeatedCores.Count == 0) return;
        defeatedCores_ = data.DefeatedCores;
    }

    public void KillCore(CoreAI core)
    {
        if (!defeatedCores_.Contains(core.CoreAreaName))
            defeatedCores_.Add(core.CoreAreaName);
    }

    public List<string> DefeatedCores()
    {
        return defeatedCores_;
    }

    private void Clear()
    {
        defeatedCores_.Clear();
    }

    public bool IsCoreDead(CoreAI core)
    {
        return defeatedCores_.Contains(core.CoreAreaName);
    }

    public bool IsLastCore()
    {
        return defeatedCores_.Count >= 4;
    }
}
