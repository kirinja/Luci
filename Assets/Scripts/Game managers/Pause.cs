﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    private static Pause instance_;

    private float normalTimeScale_;
    private bool paused_;


    public delegate void PauseChange(bool paused);

    public PauseChange OnPauseChanged;


    public static bool HasInstance
    {
        get { return instance_; }
    }


    public static Pause Instance
    {
        get
        {
            // find any Pause manager in the scene
            if (instance_ == null)
            {
                instance_ = FindObjectOfType<Pause>();
            }

            // if we couldnt find one then create a new object with it
            if (instance_ == null)
            {
                var activeScene = SceneManager.GetActiveScene();
                var mainScene = SceneManager.GetSceneByName("Main");
                if (mainScene.IsValid())
                {
                    SceneManager.SetActiveScene(mainScene);
                }

                var go = new GameObject("Pause Singleton");
                SceneManager.SetActiveScene(activeScene);
                go.AddComponent<Pause>();
                instance_ = go.GetComponent<Pause>();
            }

            return instance_;
        }
    }


    private void Awake()
    {
        // Singleton, so destroy this instance if another one exists
        if (instance_ == null)
            instance_ = this;
        else if (instance_ != this)
        {
            Destroy(gameObject);
            return;
        }

        if (OnPauseChanged == null)
            OnPauseChanged = delegate { };
    }


    private void Start()
    {
        paused_ = false;
    }


    private void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            Paused = !Paused;
        }
    }


    public bool Paused
    {
        get { return paused_; }
        set
        {
            if (value == paused_) return;

            paused_ = value;

            if (paused_)
            {
                normalTimeScale_ = Time.timeScale;
                TimeManager.Instance.TimeScale = 0f;
            }
            else
            {
                TimeManager.Instance.TimeScale = normalTimeScale_;
            }

            OnPauseChanged(paused_);
        }
    }

    private void OnDestroy()
    {
        if (TimeManager.HasInstance)
            Paused = false;
    }
}