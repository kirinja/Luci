﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SetActiveSceneOnAwake : MonoBehaviour
{
    private void Start()
    {
        SceneManager.SetActiveScene(gameObject.scene);
    }
}
