﻿using UnityEngine;

public class TimeManager : MonoBehaviour
{
    private static TimeManager instance_;


    public static bool HasInstance
    {
        get { return instance_; }
    }


    public static TimeManager Instance
    {
        get
        {
            // find any SceneHandler in the scene
            if (instance_ == null)
            {
                instance_ = FindObjectOfType<TimeManager>();
            }

            // if we couldnt find one then create a new object with it
            if (instance_ == null)
            {
                var go = new GameObject("TimeManager Singleton");
                go.AddComponent<TimeManager>();
                instance_ = go.GetComponent<TimeManager>();
            }

            return instance_;
        }
    }

    private float fixedDeltaTime_;
    private float timeScale_;


    private void Awake()
    {
        // Singleton, so destroy this instance if another one exists
        if (instance_ == null)
            instance_ = this;
        else if (instance_ != this)
        {
            Destroy(gameObject);
            return;
        }


        fixedDeltaTime_ = Time.fixedDeltaTime;
        timeScale_ = Time.timeScale;
    }


    private void OnDestroy()
    {
        Time.timeScale = 1f;
        Time.fixedDeltaTime = fixedDeltaTime_;
    }


    public static float DeltaTime
    {
        get { return Time.inFixedTimeStep ? Instance.fixedDeltaTime_ * Instance.timeScale_ : Time.deltaTime; }
    }


    public static float UnscaledDeltaTime
    {
        get { return Time.inFixedTimeStep ? Instance.fixedDeltaTime_ : Time.unscaledDeltaTime; }
    }


    public float TimeScale
    {
        get { return timeScale_; }
        set
        {
            timeScale_ = value;
            Time.timeScale = timeScale_;
            Time.fixedDeltaTime = fixedDeltaTime_ * timeScale_;
        }
    }
}