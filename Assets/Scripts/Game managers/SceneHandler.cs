﻿using System.Collections;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    [ReorderableList] public string[] Scenes = new string[0];
    [ReorderableList] public string[] ScenesToReload = new string[0];
    public string ActiveScene = "";
    public bool LoadOnAwake;


    private static SceneHandler instance_;

    private FadeSprite fadeSprite_;

    public static SceneHandler Instance
    {
        get
        {
            // find any SceneHandler in the scene
            if (instance_ == null)
            {
                instance_ = FindObjectOfType<SceneHandler>();
            }

            // if we couldnt find one then create a new object with it
            if (instance_ == null)
            {
                var go = new GameObject("SceneHandler Singleton");
                go.AddComponent<SceneHandler>();
                instance_ = go.GetComponent<SceneHandler>();
                // Note that for scenes to load on awake, the object has to exist already
            }

            return instance_;
        }
    }


    private void Awake()
    {
        // Singleton, so destroy this instance if another one exists
        if (instance_ == null)
            instance_ = this;
        else if (instance_ != this)
        {
            Destroy(gameObject);
            return;
        }

        fadeSprite_ = GetComponent<FadeSprite>();

        if (LoadOnAwake)
            LoadAllScenes();
    }


    private void LoadAllScenes()
    {
        StartCoroutine(LoadAllScenesWithLoadingScreen());
    }

    private IEnumerator LoadAllScenesWithLoadingScreen()
    {
        SceneManager.LoadScene("LoadingScreen", LoadSceneMode.Additive);

        foreach (var scene in Scenes)
        {
            var loadingScreen = SceneManager.GetSceneByName("LoadingScreen");
            SceneManager.SetActiveScene(loadingScreen);

            if (SceneManager.GetSceneByName(scene).isLoaded) continue;
            AsyncOperation async = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
            async.allowSceneActivation = false;
            while (!async.isDone)
            {
                // Loading completed
                if (Mathf.Abs(async.progress - 0.9f) < 0.01f)
                {
                    async.allowSceneActivation = true;

                    yield return StartCoroutine(fadeSprite_.FadeIn());
                }

                yield return null;
            }
        }
        SceneManager.UnloadSceneAsync("LoadingScreen");

        // Fade to new screen
        yield return StartCoroutine(fadeSprite_.FadeOut());
        SetSceneActive();
    }


    public void ReloadLevel()
    {
        for (var i = ScenesToReload.Length - 1; i >= 0 ; i--)
        {
            var scene = ScenesToReload[i];
            if (SceneManager.GetSceneByName(scene).isLoaded)
                SceneManager.UnloadSceneAsync(scene);
        }
        foreach (var scene in ScenesToReload)
        {
            SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
        }
    }


    private void SetSceneActive()
    {
        var active = SceneManager.GetSceneByName(ActiveScene);
        if (active.isLoaded)
        {
            SceneManager.SetActiveScene(active);
        }
    }
}