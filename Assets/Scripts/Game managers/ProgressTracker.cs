﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProgressTracker : MonoBehaviour
{
    private static ProgressTracker instance_;
    private Scene scene_;
    #region Scene Structure
    private GameObject[] rootObjects_;
    private GameObject forest_;
    private GameObject mountain_;
    private GameObject desert_;
    #endregion

    #region Difficulty Layers
    #region Forest Layers
    private GameObject forestLayer1_;
    private GameObject forestLayer2_;
    #endregion
    #region Mountain Layers
    private GameObject mountainLayer1_;
    private GameObject mountainLayer2_;
    #endregion
    #region Desert Layers
    private GameObject desertLayer1_;
    private GameObject desertLayer2_;
    #endregion
    #endregion

    #region Layer Indexs
    private int forestIndex_;
    private int mountainIndex_;
    private int desertIndex_;
#endregion

    // need to load data correctly for enemies and progress, right now we only handle player checkpoint position and visage

    public static ProgressTracker Instance
    {
        get
        {
            // find any IO in the scene
            if (instance_ == null)
            {
                instance_ = FindObjectOfType<ProgressTracker>();
            }

            // if we couldnt find one then create a new object with it
            if (instance_ == null)
            {
                GameObject go = new GameObject("Progress Singleton");
                go.AddComponent<ProgressTracker>();
                go.hideFlags = HideFlags.HideInHierarchy;
                instance_ = go.GetComponent<ProgressTracker>();
            }
            return instance_;
        }
    }

	// Use this for initialization
	void Awake ()
	{
	    scene_ = SceneManager.GetSceneByName("DifficultyProgression");

        FindHiarchy();
        FindLayers();
    }

    private void FindHiarchy()
    {
        // setup the layer objects so we can reference what to active at what point
        rootObjects_ = scene_.GetRootGameObjects();
        forest_ = Array.Find(rootObjects_, o => o.name.Equals("Forest"));
        mountain_ = Array.Find(rootObjects_, o => o.name.Equals("Mountain"));
        desert_ = Array.Find(rootObjects_, o => o.name.Equals("Desert"));
    }

    private void FindLayers()
    {
        forestLayer1_ = forest_.transform.GetChild(0).gameObject;
        forestLayer2_ = forest_.transform.GetChild(1).gameObject;

        mountainLayer1_ = mountain_.transform.GetChild(0).gameObject;
        mountainLayer2_ = mountain_.transform.GetChild(1).gameObject;

        desertLayer1_ = desert_.transform.GetChild(0).gameObject;
        desertLayer2_ = desert_.transform.GetChild(1).gameObject;
    }

    public void UpdateProgress(CoreAI coreAi)
    {
        // determine where to increase the difficulty in the world
        // depends on which core was defeated, meaning that each core have to correspond to harder layers?

        // this needs to know the different "layers" and which one to activate depending on which core just died
        // we need to know which core ai that is linked to which area

        // this is gonna get the scene that has the "difficulty layers" 
        // and use the CoreAI name combined with EnemyCoreController to know which ones to activate
        var area = coreAi.CoreAreaName;
        switch (area)
        {
            case "Desert":
                DefeatDesertArea();
                break;

            case "Forest":
                DefeatForestArea();
                break;

            case "Mountain":
                DefeatMountainArea();
                break;
        }
    }

    public void DefeatDesertArea()
    {
        // here we have to check which previous areas have been defeated so we know which layers to activate in each area
        var defeatedCores = EnemyCoreHandler.Instance.DefeatedCores();

        bool forest = false;
        bool mountain = false;

        // need to check which cores have been defeated and only activate the layers for undefeated cores
        foreach (var c in defeatedCores)
        {
            if (c.Equals("Mountain"))
                mountain = true;
            if (c.Equals("Forest"))
                forest = true;
        }

        // if we have defeated mountain then we do not want to activate that layer
        if (!mountain)
        {
            switch (mountainIndex_)
            {
                case 0:
                    mountainLayer1_.SetActive(true);
                    mountainIndex_++;
                    break;
                case 1:
                    mountainLayer2_.SetActive(true);
                    break;
            }
        }

        if (!forest)
        {
            switch (forestIndex_)
            {
                case 0:
                    forestLayer1_.SetActive(true);
                    forestIndex_++;
                    break;
                case 1:
                    forestLayer2_.SetActive(true);
                    break;
            }
        }
    }

    public void DefeatMountainArea()
    {
        // here we have to check which previous areas have been defeated so we know which layers to activate in each area
        var defeatedCores = EnemyCoreHandler.Instance.DefeatedCores();

        bool forest = false;
        bool desert = false;

        // need to check which cores have been defeated and only activate the layers for undefeated cores
        foreach (var c in defeatedCores)
        {
            if (c.Equals("Desert"))
                desert = true;
            if (c.Equals("Forest"))
                forest = true;
        }

        // if we have defeated mountain then we do not want to activate that layer
        if (!desert)
        {
            switch (desertIndex_)
            {
                case 0:
                    desertLayer1_.SetActive(true);
                    desertIndex_++;
                    break;
                case 1:
                    desertLayer2_.SetActive(true);
                    break;
            }
        }

        if (!forest)
        {
            switch (forestIndex_)
            {
                case 0:
                    forestLayer1_.SetActive(true);
                    forestIndex_++;
                    break;
                case 1:
                    forestLayer2_.SetActive(true);
                    break;
            }
        }
    }

    public void DefeatForestArea()
    {
        // here we have to check which previous areas have been defeated so we know which layers to activate in each area
        var defeatedCores = EnemyCoreHandler.Instance.DefeatedCores();

        bool mountain = false;
        bool desert = false;

        // need to check which cores have been defeated and only activate the layers for undefeated cores
        foreach (var c in defeatedCores)
        {
            if (c.Equals("Desert"))
                desert = true;
            if (c.Equals("Mountain"))
                mountain = true;
        }

        // if we have defeated mountain then we do not want to activate that layer
        if (!desert)
        {
            switch (desertIndex_)
            {
                case 0:
                    desertLayer1_.SetActive(true);
                    desertIndex_++;
                    break;
                case 1:
                    desertLayer2_.SetActive(true);
                    break;
            }
        }

        if (!mountain)
        {
            switch (mountainIndex_)
            {
                case 0:
                    mountainLayer1_.SetActive(true);
                    mountainIndex_++;
                    break;
                case 1:
                    mountainLayer2_.SetActive(true);
                    break;
            }
        }
    }
}
