﻿using FMODUnity;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class MusicTrigger : MonoBehaviour
{
    public enum PlayTrigger
    {
        Start,
        OnTriggerEnter
    }


    public PlayTrigger TriggerEvent;

    [EventRef]
    public string MusicEvent;


    private void Awake()
    {
        GetComponent<Collider>().isTrigger = true;
    }


    private void Start()
    {
        HandleGameEvent(PlayTrigger.Start);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        HandleGameEvent(PlayTrigger.OnTriggerEnter);
    }


    private void HandleGameEvent(PlayTrigger gameEvent)
    {
        if (TriggerEvent == gameEvent)
        {
            PlayMusic();
        }
    }


    private void PlayMusic()
    {
        MusicManager.Instance.PlayMusic(MusicEvent);
    }
}