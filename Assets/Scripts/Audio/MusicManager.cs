﻿using FMOD.Studio;
using FMODUnity;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private EventInstance currentMusicInstance_;
    private EventInstance oldInstance_;
    private string currentMusicEvent_;
    private string eventPlaying_;
    private bool hasOld_;


    private static MusicManager instance_;


    public static bool HasInstance
    {
        get { return instance_; }
    }


    public static MusicManager Instance
    {
        get
        {
            if (!instance_)
            {
                instance_ = FindObjectOfType<MusicManager>();
            }

            if (!instance_)
            {
                var go = new GameObject("Music Manager");
                instance_ = go.AddComponent<MusicManager>();
                go.hideFlags = HideFlags.HideInHierarchy;
            }

            return instance_;
        }
    }


    private void Awake()
    {
        // Singleton, so destroy this instance if another one exists
        if (!instance_)
        {
            instance_ = this;
        }
        else if (instance_ != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this);
    }


    private void Start()
    {
        ChristmasManager.Instance.OnChristmasChanged += OnChristmasChanged;
    }


    private void OnChristmasChanged(bool christmas)
    {
        PlaySong(eventPlaying_ + (christmas ? "_xmas" : ""));
    }


    public void PlayMusic(string musicEvent)
    {
        eventPlaying_ = musicEvent;
        if (ChristmasManager.Instance.Activated)
        {
            musicEvent = musicEvent + "_xmas";
        }
        
        PlaySong(musicEvent);
    }


    private void PlaySong(string musicEvent)
    {
        if (musicEvent.Equals(currentMusicEvent_)) return;

        if (currentMusicEvent_ != null)
        {
            if (hasOld_)
            {
                oldInstance_.stop(STOP_MODE.ALLOWFADEOUT);
                oldInstance_.release();
            }
            currentMusicInstance_.setParameterValue("Volume", 0f);
            oldInstance_ = currentMusicInstance_;
            hasOld_ = true;
        }

        currentMusicInstance_ = RuntimeManager.CreateInstance(musicEvent);
        currentMusicEvent_ = musicEvent;
        currentMusicInstance_.start();
        currentMusicInstance_.setParameterValue("Volume", 1f);
    }
}
