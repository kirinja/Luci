﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPositioner : MonoBehaviour
{
    private Transform cameraTransform_;

	// Use this for initialization
	void Start ()
	{
	    cameraTransform_ = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void LateUpdate()
    {
        transform.rotation = cameraTransform_.rotation;
    }
}
