﻿using FMODUnity;
using NaughtyAttributes;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
[RequireComponent(typeof(Collider))]
public class CollectiblePickup : MonoBehaviour
{
    [Required] public Collectible Card;

    [BoxGroup("Sounds")] [EventRef] public string PickupSound;

    public float RotationSpeed = 180f;


    private void Start()
    {
        GetComponent<Renderer>().material = Card.Material;

        var holder = FindObjectOfType<CollectibleHolder>();
        holder.RegisterCollectible(Card);
        if (holder.HasFound(Card.Name)) Destroy(gameObject);
    }


    private void Update()
    {
        var deltaTime = TimeManager.DeltaTime;

        transform.Rotate(Vector3.up, RotationSpeed * deltaTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        var holder = other.GetComponent<CollectibleHolder>();
        if (holder)
        {
            holder.PickupCollectible(Card.Name);
            RuntimeManager.PlayOneShot(PickupSound, transform.position);
            Destroy(gameObject);
        }
    }
}