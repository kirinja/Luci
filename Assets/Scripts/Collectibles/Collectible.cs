﻿using NaughtyAttributes;
using UnityEngine;


[CreateAssetMenu(menuName = "Collectibles/Card")]
public class Collectible : ScriptableObject
{
    public string Name;
    [Required] public Sprite Image;
    [Required] public Material Material;
}