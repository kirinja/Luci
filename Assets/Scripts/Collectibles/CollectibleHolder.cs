﻿using System.Collections.Generic;
using UnityEngine;

public class CollectibleHolder : MonoBehaviour
{
    public float TextPopupTime = 3f;

    
    private List<Collectible> allCollectibles_;
    private List<string> foundCollectibles_;


    private void Start()
    {
        allCollectibles_ = new List<Collectible>();
        LoadSaveData();
    }


    public void PickupCollectible(string cardName)
    {
        foundCollectibles_.Add(cardName);
        DisplayProgress();
        foreach (var collectible in allCollectibles_)
        {
            if (collectible.Name == cardName)
            {
                ShowCard(collectible);
                break;
            }
        }
    }


    public void RegisterCollectible(Collectible collectible)
    {
        allCollectibles_.Add(collectible);
    }


    public bool HasFound(string cardName)
    {
        return foundCollectibles_.Contains(cardName);
    }


    private void LoadSaveData()
    {
        var saveData = IO.Instance.LoadGame();
        foundCollectibles_ = saveData == null ? new List<string>() : new List<string>(saveData.Collectibles);
    }


    private void ShowCard(Collectible card)
    {
        var imgPrompt = FindObjectOfType<ImagePrompt>();
        if (imgPrompt)
        {
            imgPrompt.DisplayImage(card.Image, TextPopupTime);
        }
    }


    private void DisplayProgress()
    {
        var prompt = FindObjectOfType<UIPrompt>();
        if (prompt)
        {
            prompt.DisplayText(foundCollectibles_.Count + "/" + allCollectibles_.Count + " bonus cards found", TextPopupTime);
        }

        
    }


    public string[] FoundCollectibles
    {
        get { return foundCollectibles_.ToArray(); }
    }


    public int NumberOfCollectibles
    {
        get { return allCollectibles_.Count; }
    }
}