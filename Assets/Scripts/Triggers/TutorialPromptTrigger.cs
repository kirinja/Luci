﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class TutorialPromptTrigger : MonoBehaviour
{
    public Controller3D.PlayerActions Action;

    public string Text;

    public float DelayTime = 5f;
    public float TimeShown = 1f;


    private bool shown_;
    private Controller3D controller3D_;
    private Timer delayTimer_;


    private void Start()
    {
        controller3D_ = FindObjectOfType<Controller3D>();
        if (controller3D_)
        {
            controller3D_.OnPlayerAction += OnPlayerAction;
        }
        shown_ = false;
        delayTimer_ = new Timer(DelayTime, true);
    }


    private void Update()
    {
        var deltaTime = TimeManager.UnscaledDeltaTime;

        if (!controller3D_)
        {
            controller3D_ = FindObjectOfType<Controller3D>();
            if (controller3D_)
            {
                controller3D_.OnPlayerAction += OnPlayerAction;
            }
        }

        if (delayTimer_.Update(deltaTime))
        {
            ShowPrompt();
        }
    }


    private void OnPlayerAction(Controller3D controller3D, Controller3D.PlayerActions action)
    {
        if (action == Action)
        {
            shown_ = true;
            delayTimer_.Finish();
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(shown_ || !other.CompareTag("Player")) return;

        shown_ = true;
        delayTimer_ = new Timer(DelayTime);
        
    }


    private void ShowPrompt()
    {
        var prompt = FindObjectOfType<UIPrompt>();
        if (prompt)
        {
            prompt.DisplayText(Text, TimeShown);
        }
    }
}
