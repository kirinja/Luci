﻿using System.Collections.Generic;
using FMODUnity;
using NaughtyAttributes;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    [BoxGroup("Sounds")] [EventRef] public string TakeDamageSound;
    [BoxGroup("Sounds")] [EventRef] public string DeathSound;

    [BoxGroup("Damaged")] public GameObject HitParticles;

    [BoxGroup("Damaged")]
    [Tooltip("The lowest percentage of health for which the visual effect will be maximally visible")]
    [MinValue(0f), MaxValue(1f)] public float HealthEffectLowestThreshold = 0.25f;


    private ScreenSpaceGreyScale cameraEffect_;
    private CharacterHealth health_;
    private Animator animator_;

    // Used to detect if the player is in combat or not
    private HashSet<EnemyAI> enemiesInCombat_;


    // Use this for initialization
    private void Start()
    {
        enemiesInCombat_ = new HashSet<EnemyAI>();
        // Camera effect for damage
        var mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        if (mainCamera)
        {
            cameraEffect_ = mainCamera.GetComponent<ScreenSpaceGreyScale>();
        }

        // Initiate all events
        health_ = GetComponent<CharacterHealth>();
        if (health_)
        {
            health_.OnHealthChanged += OnHealthChanged;
            health_.OnDamaged += TakeDamage;
            health_.OnDeath += Death;
        }
        animator_ = GetComponent<Animator>();
    }


    private void TakeDamage(ref int amount, AttackType attackType, Vector3 hitPosition)
    {
        if (Invulnerable)
        {
            amount = 0;
            return;
        }

        RuntimeManager.PlayOneShot(TakeDamageSound, transform.position);
        if (HitParticles)
        {
            foreach (var particleSys in HitParticles.GetComponentsInChildren<ParticleSystem>())
            {
                particleSys.Play();
            }
        }
    }


    private void OnHealthChanged()
    {
        if (!cameraEffect_) return;

        var minInv = 1 - HealthEffectLowestThreshold;
        var health = GetComponent<CharacterHealth>();

        // Will make sure that the visual effect will be at maximum magnitude even when health is not necessarily zero
        var percentage = 1f - (float) health.CurrentHealth / health.MaxHealth;
        var range = Mathf.Clamp(percentage, 0f, minInv) * (1 / minInv);
        cameraEffect_.Magnitude = range;
    }


    private void Death(CharacterHealth health)
    {
        if (animator_)
        {
            animator_.SetTrigger("Death");
            RuntimeManager.PlayOneShot(DeathSound, transform.position);
        }

        var respawner = GetComponent<Respawner>();
        if (respawner)
        {
            respawner.StartRespawn();
        }
    }


    // Tells the player that an enemy is attacking them
    public void AddEnemy(EnemyAI enemy)
    {
        var regen = GetComponent<HealthRegen>();
        if (regen)
        {
            regen.InCombat = true;
        }

        enemiesInCombat_.Add(enemy);
    }


    // Tells the player that an enemy is no longer attacking them
    public void RemoveEnemy(EnemyAI enemy)
    {
        enemiesInCombat_.Remove(enemy);

        var regen = GetComponent<HealthRegen>();
        if (regen && enemiesInCombat_.Count == 0)
        {
            regen.InCombat = false;
        }
    }


    public bool Invulnerable { get; set; }
}