﻿using FMODUnity;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.PostProcessing;

public class Visage : MonoBehaviour
{
    public int EnergyRequired = 100;
    public float TimeActive = 10f;
    public float TimeScale = 0.4f;
    public PostProcessingProfile PostProcessProfile;
    public float EffectFadeTime = 0.4f;
    public float FieldOfView = 90f;
    public float CameraDistance = 3f;

    [Required] public Transform VisageMask;
    [Required] public Transform ActivateParticles;
    [Required] public Transform PickupParticles;


    public GameObject PickupPrefab;

    [BoxGroup("Sounds")] [EventRef] public string PickupSound;
    
    [MinValue(0)] public int Gauge;

    private Timer timer_;
    private Timer lerpTimer_;

    private bool particlesPlaying_;

    private PostProcessingBehaviour cameraPostProcessing_;
    private Camera camera_;
    private Camera3D camera3D_;

    private PostProcessingProfile previousProfile_;
    private float prevFov_;
    private float prevCamDistance_;

    private Controller3D controller3D_;

    public Material MaskMaterial;
    public Texture2D[] MaskEmissiveMaps;

    public Material ChristmasMaterial;
    public Texture2D[] ChristmasEmissiveMaps;

    // Use this for initialization
    private void Start()
    {
        lerpTimer_ = new Timer(EffectFadeTime, true);
        timer_ = new Timer(TimeActive, true);
        var cam = GameObject.FindGameObjectWithTag("MainCamera");
        cameraPostProcessing_ = cam.GetComponent<PostProcessingBehaviour>();
        camera_ = cam.GetComponent<Camera>();
        camera3D_ = cam.GetComponent<Camera3D>();
        controller3D_ = GetComponent<Controller3D>();
        
        if (controller3D_)
        {
            var lightBox = controller3D_.LightAttackHitBox;
            if (lightBox)
            {
                lightBox.OnHit += OnAttackHit;
            }
            var heavyBox = controller3D_.HeavyAttackHitBox;
            if (heavyBox)
            {
                heavyBox.OnHit += OnAttackHit;
            }
            var shockwaveBox = controller3D_.ShockWaveHitBox;
            if (shockwaveBox)
            {
                shockwaveBox.OnHit += OnAttackHit;
            }
        }

        var saveData = IO.Instance.LoadGame();
        if (saveData != null)
        {
            Gauge = saveData.VisageMeter;
        }
        SetMaskTexture();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Pause.Instance.Paused) return;

        var deltaTime = TimeManager.UnscaledDeltaTime;

        if (!timer_.IsDone)
        {
            if (timer_.Update(deltaTime))
            {
                Deactivate();
            }

            Gauge = Mathf.FloorToInt((1f - timer_.PercentDone) * EnergyRequired);

            SetMaskTexture();
        }

        if (CanActivate)
        {
            Activate();
        }

        if (!lerpTimer_.IsDone)
        {
            lerpTimer_.Update(deltaTime);
            camera3D_.PreferredDistance = timer_.IsDone
                ? Mathf.Lerp(CameraDistance, prevCamDistance_, lerpTimer_.PercentDone)
                : Mathf.Lerp(prevCamDistance_, CameraDistance, lerpTimer_.PercentDone);
            camera_.fieldOfView = timer_.IsDone
                ? Mathf.Lerp(FieldOfView, prevFov_, lerpTimer_.PercentDone)
                : Mathf.Lerp(prevFov_, FieldOfView, lerpTimer_.PercentDone);
            TimeManager.Instance.TimeScale = timer_.IsDone
                ? Mathf.Lerp(TimeScale, 1f, lerpTimer_.PercentDone)
                : Mathf.Lerp(1f, TimeScale, lerpTimer_.PercentDone);
        }
    }


    public void Fill(int amount)
    {
        Gauge += Mathf.Min(amount, EnergyRequired - Gauge);

        SetMaskTexture();
        RuntimeManager.PlayOneShot(PickupSound, transform.position);
        foreach (var pickupParticle in PickupParticles.GetComponentsInChildren<ParticleSystem>())
        {
            pickupParticle.Stop();
            pickupParticle.Play();
        }
    }


    private void OnAttackHit(HitBox attacker, CharacterHealth attackee, int damage, Vector3 hitPosition)
    {
        if (PickupPrefab)
            SpawnPickup(hitPosition);
    }


    private void SpawnPickup(Vector3 position)
    {
        Instantiate(PickupPrefab, position, Quaternion.identity);
    }


    private void Activate()
    {
        controller3D_.OnPlayerAction(controller3D_, Controller3D.PlayerActions.Visage);

        timer_ = new Timer(TimeActive);
        lerpTimer_ = new Timer(EffectFadeTime);
        prevCamDistance_ = camera3D_.PreferredDistance;
        prevFov_ = camera_.fieldOfView;
        if (cameraPostProcessing_)
        {
            previousProfile_ = cameraPostProcessing_.profile;
            var timeProf = cameraPostProcessing_.gameObject.AddComponent<TimeUpdatableProfile>();
            timeProf.LerpOverTimeTo(cameraPostProcessing_, PostProcessProfile, EffectFadeTime);
        }
    }


    private void Deactivate()
    {
        lerpTimer_ = new Timer(EffectFadeTime);
        if (cameraPostProcessing_)
        {
            var timeProf = cameraPostProcessing_.gameObject.AddComponent<TimeUpdatableProfile>();
            timeProf.LerpOverTimeTo(cameraPostProcessing_, previousProfile_, EffectFadeTime);
        }
    }


    private void SetMaskTexture()
    {
        var index = Mathf.FloorToInt(Gauge / (float)EnergyRequired * (MaskEmissiveMaps.Length - 1));
        MaskMaterial.SetTexture("_EmissionMap", MaskEmissiveMaps[index]);
        ChristmasMaterial.SetTexture("_EmissionMap", ChristmasEmissiveMaps[index]);

        if (Gauge == EnergyRequired)
        {
            if (!particlesPlaying_)
            {
                foreach (var particles in ActivateParticles.GetComponentsInChildren<ParticleSystem>())
                {
                    particles.Play();
                }
                particlesPlaying_ = true;
            }
        }
        else
        {
            if (particlesPlaying_)
            {
                foreach (var particles in ActivateParticles.GetComponentsInChildren<ParticleSystem>())
                {
                    particles.Stop();
                }
                particlesPlaying_ = false;
            }
        }
    }


    private bool CanActivate
    {
        get
        {
            return Gauge == EnergyRequired && (Input.GetButtonDown("Visage 1") && Input.GetButton("Visage 2") ||
                                                Input.GetButtonDown("Visage 2") && Input.GetButton("Visage 1")) &&
                   timer_.IsDone;
        }
    }


    private void OnDestroy()
    {
        if (TimeManager.HasInstance)
        {
            TimeManager.Instance.TimeScale = 1f;
        }

        // Reset the mask material to avoid it being changed after game has exited
        MaskMaterial.SetTexture("_EmissionMap", MaskEmissiveMaps[0]);
        ChristmasMaterial.SetTexture("_EmissionMap", ChristmasEmissiveMaps[0]);
    }
}