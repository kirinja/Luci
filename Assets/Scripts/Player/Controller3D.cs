﻿using System;
using System.Linq;
using FMODUnity;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(CharacterController))]
public class Controller3D : MonoBehaviour
{
    public enum PlayerActions
    {
        None,
        Move,
        LightAttack,
        HeavyAttack,
        Jump,
        Dash,
        AirLightAttack,
        AirHeavyAttack,
        AttackCombo,
        Visage,
        LockOn
    }


    #region parameters

    [BoxGroup("Movement")] [MinValue(0f)] public float RunSpeed = 10f;

    [BoxGroup("Movement")] [MinValue(0f)]
    [ValidateInput("IsGreaterThanZero", "AccelerationTime must be greater than zero")]
    public float GroundAccelerationTime = 0.15f;

    [BoxGroup("Movement")] [MinValue(0f)]
    [ValidateInput("IsGreaterThanZero", "AccelerationTime must be greater than zero")]
    public float AirAccelerationTime = 0.4f;

    [BoxGroup("Movement")] [MinValue(0f)]
    [ValidateInput("IsGreaterThanZero", "AccelerationTime must be greater than zero")]
    public float AttackGlideTime = 0.4f;


    [BoxGroup("Movement")] [MinValue(0)]
    [ValidateInput("IsGreaterThanZero", "MaxRotationTime must be greater than zero")]
    public float MaxRotationTime = 0.2f;

    [BoxGroup("Movement")] [MinValue(0)]
    [ValidateInput("IsGreaterThanZero", "RotationInputSmoothingTime must be greater than zero")]
    public float RotationInputSmoothingTime = 0.1f;

    [BoxGroup("Strafe")] public bool Strafing;
    [BoxGroup("Strafe")] public Vector3 LockDirection;
    [BoxGroup("Strafe")] [MinValue(0f)] public float BackwardsStrafeMultiplier = 0.5f;


    [BoxGroup("Jump")] [MinValue(0)] public float MaxJumpHeight = 5f;
    [BoxGroup("Jump")] [MinValue(0)] public float MinJumpHeight = 2.5f;
    [BoxGroup("Jump")] [MinValue(0)] public float MaxJumpLength = 7f;
    [BoxGroup("Jump")] [MinValue(0)] public float LandingFeedbackTimeThreshold = 0.3f;

    [BoxGroup("Dash")] [MinValue(0)] public float DashSpeed = 30f;
    [BoxGroup("Dash")] [MinValue(0)] public float DashLength = 3f;
    [BoxGroup("Dash")] [MinValue(0)] public float DashCooldown = 1f;
    [BoxGroup("Dash")] public GameObject DashParticles;
    [BoxGroup("Dash")] public ParticleSystem[] ParticlesToHideWhileDashing;
    [BoxGroup("Dash")] [MinValue(0f)] public float SprintSpeed = 20f;

    [BoxGroup("Collision")] public LayerMask CollisionMask = Physics.DefaultRaycastLayers;
    [BoxGroup("Collision")] public float GroundCheckDistance = 0.1f;

    [BoxGroup("Sounds")] [Required] public Transform LeftFoot;
    [BoxGroup("Sounds")] [Required] public Transform RightFoot;
    [BoxGroup("Sounds")] [EventRef] public string FootstepSound;
    [BoxGroup("Sounds")] [EventRef] public string JumpSound;
    [BoxGroup("Sounds")] [EventRef] public string LandSound;
    [BoxGroup("Sounds")] [EventRef] public string DashSound;
    [BoxGroup("Sounds")] [EventRef] public string GroundSlamLandingSound;


    [BoxGroup("Attacks")] [MinValue(1)] public int ComboLength = 3;
    [BoxGroup("Attacks")] [MinValue(0)] public float ComboCooldown = 0.5f;
    [BoxGroup("Attacks")] [Required] public GameObject HitParticlesPrefab;
    [BoxGroup("Attacks")] [MinValue(0)] public int HitParticlesMax = 10;

    [BoxGroup("Light Attack")] public int LightAttackDamage = 10;
    [BoxGroup("Light Attack")] public int LightAttackMaxTargets = 1;
    [BoxGroup("Light Attack")] [MinValue(0)] public float LightAttackHitstopTime = 0.1f;
    [BoxGroup("Light Attack")] [Required] public HitBox LightAttackHitBox;
    [BoxGroup("Light Attack")] public TrailRenderer LightAttackTrailRight;
    [BoxGroup("Light Attack")] public TrailRenderer LightAttackTrailLeft;

    [BoxGroup("Heavy Attack")] [MinValue(0)] public int HeavyAttackDamage = 20;
    [BoxGroup("Heavy Attack")] [MinValue(0)] public int HeavyAttackMaxTargets = 3;
    [BoxGroup("Heavy Attack")] [MinValue(0)] public float HeavyAttackHitstopTime = 0.2f;
    [BoxGroup("Heavy Attack")] [Required] public HitBox HeavyAttackHitBox;
    [BoxGroup("Heavy Attack")] public TrailRenderer HeavyAttackTrail;
    [BoxGroup("Heavy Attack")] [Required] public GameObject BackLucerne;
    [BoxGroup("Heavy Attack")] [Required] public GameObject HandLucerne;

    [BoxGroup("Ground Slam")] [MinValue(0f)] public float GroundSlamSpeed = 15f;
    [BoxGroup("Ground Slam")] [MinValue(0f)] public float GroundSlamMaxTime = 0.5f;
    [BoxGroup("Ground Slam")] [MinValue(0f)] public float ShockWaveEndlag = 0.5f;
    [BoxGroup("Ground Slam")] [Required] public HitBox ShockWaveHitBox;
    [BoxGroup("Ground Slam")] [MinValue(0)] public int ShockWaveMaxTargets = 5;
    [BoxGroup("Ground Slam")] [MinValue(0)] public int ShockWaveDamage = 15;

    [BoxGroup("Dive Attack")] [MinValue(0f)] public float DiveMaxTime = 0.5f;
    [BoxGroup("Dive Attack")] [MinValue(0f)] public float DiveStartupTime = 0.25f;
    [BoxGroup("Dive Attack")] [MinValue(0f)] public float DiveEndingTime = 0.25f;
    [BoxGroup("Dive Attack")] [MinValue(0f)] public float DiveSpeed = 30f;
    [BoxGroup("Dive Attack")] [MinValue(0f), MaxValue(90f)] public float DiveAngle = 45f;
    [BoxGroup("Dive Attack")] [Required] public Transform PivotPoint;

    [BoxGroup("Sounds")] [EventRef] public string LightAttackSwingSound;
    [BoxGroup("Sounds")] [EventRef] public string HeavyAttackSwingSound;

    #endregion


    public delegate void PlayerAction(Controller3D player, PlayerActions action);

    public PlayerAction OnPlayerAction = delegate { };


    private CharacterController characterController_;
    private CharacterState state_;
    private Transform cameraTransform_;
    private Vector2 smoothedDirectionInput_;
    private float lastInputAngle_;
    private Timer dashCooldown_;
    private Timer attackCooldown_;
    private CharacterHealth health_;
    private float lightAttackTrailTime_, heavyAttackTrailTime_;
    private GameObject[] hitParticles_;
    private int hitParticlesIndex_;


    public enum Events
    {
        ActionInterruptible,
        ActionFinished1,
        ActionFinished2,
        ActionFinished3,
        ActivateHitbox,
        DeactivateHitbox
    }


    // Use this for initialization
    private void Start()
    {
        hitParticles_ = new GameObject[HitParticlesMax];
        for (var i = 0; i < hitParticles_.Length; ++i)
        {
            hitParticles_[i] = Instantiate(HitParticlesPrefab);
        }

        MaxSpeed = RunSpeed;
        smoothedDirectionInput_ = Vector2.zero;
        characterController_ = GetComponent<CharacterController>();
        cameraTransform_ = GameObject.FindGameObjectWithTag("MainCamera").transform;
        state_ = new CharacterStateGrounded(this);
        dashCooldown_ = new Timer(DashCooldown, true);
        attackCooldown_ = new Timer(ComboCooldown, true);
        health_ = GetComponent<CharacterHealth>();

        var hitStopComponent = GetComponent<Hitstop>();
        if (hitStopComponent)
        {
            LightAttackHitBox.HitstopComponent = hitStopComponent;
            HeavyAttackHitBox.HitstopComponent = hitStopComponent;
        }

        HeavyAttackHitBox.OnHit += PlayHitFeedback;
        LightAttackHitBox.OnHit += PlayHitFeedback;

        if (LightAttackTrailRight)
        {
            lightAttackTrailTime_ = LightAttackTrailRight.time;
            SetTrailActive(CharacterStateAttacking.AttackType.Light, false);
        }
        if (HeavyAttackTrail)
        {
            heavyAttackTrailTime_ = HeavyAttackTrail.time;
            SetTrailActive(CharacterStateAttacking.AttackType.Heavy, false);
        }

        var lockOn = FindObjectOfType<LockOn>();
        if (lockOn) lockOn.OnLockOn += OnLockOn;

        state_.Enter();
    }


    // Update is called once per frame
    private void Update()
    {
        if (Pause.Instance.Paused || (health_ && !health_.IsAlive)) return;

        SmoothRotationInput();
        UpdateCooldowns();

        state_.Update();

        UpdateAnimations();
    }


    private void FixedUpdate()
    {
        var deltaTime = TimeManager.UnscaledDeltaTime;
        if (Pause.Instance.Paused || (health_ && !health_.IsAlive)) return;

        Move(Velocity * deltaTime);
    }


    private void HandleEvent(Events e)
    {
        state_.HandleEvent(e);
    }


    public void Move(Vector3 movement)
    {
        if (WorldSpaceInput.magnitude > 0f)
        {
            OnPlayerAction(this, PlayerActions.Move);
        }
        var collisionFlags =
            characterController_.Move(movement);
        HandleCollisions(collisionFlags, movement);
    }


    public void SwitchState(CharacterState state)
    {
        state_.Exit();
        state_ = state;
        state_.Enter();
    }


    // Let the current state react to collisions
    public void HandleCollisions(CollisionFlags collisionFlags, Vector3 movement)
    {
        while (state_.HandleCollisions(collisionFlags, movement))
        {
        }
    }


    private void OnLockOn(LockOn lockOn)
    {
        OnPlayerAction(this, PlayerActions.LockOn);
    }


    // Input smoothing, should be run every frame regardless of state
    private void SmoothRotationInput()
    {
        var deltaTime = Time.unscaledDeltaTime;

        var input = new Vector2(WorldSpaceInput.x, WorldSpaceInput.z);

        var inputSmoothingMovement = 1f / RotationInputSmoothingTime * deltaTime;
        var inputSmoothingDirection = input - smoothedDirectionInput_;
        if (inputSmoothingMovement > inputSmoothingDirection.magnitude)
            inputSmoothingMovement =
                inputSmoothingDirection.magnitude;
        inputSmoothingDirection.Normalize();
        smoothedDirectionInput_ += inputSmoothingDirection * inputSmoothingMovement;

        if (input.magnitude > 0f)
        {
            lastInputAngle_ = Mathf.Rad2Deg * Mathf.Atan2(smoothedDirectionInput_.x, smoothedDirectionInput_.y);
        }
    }


    public void ResetDirectionInput()
    {
        lastInputAngle_ = transform.eulerAngles.y;
    }


    // ReSharper disable once UnusedMember.Local
    private bool IsGreaterThanZero(float number)
    {
        return number > 0f;
    }


    private void UpdateCooldowns()
    {
        var deltaTime = Time.unscaledDeltaTime;

        dashCooldown_.Update(deltaTime);
        attackCooldown_.Update(deltaTime);
    }


    private void UpdateAnimations()
    {
        var animator = GetComponent<Animator>();
        if (!animator) return;

        var localVel = transform.InverseTransformVector(Velocity);
        var normalizedVel = localVel / RunSpeed;
        var xVel = normalizedVel.x;
        var zVel = normalizedVel.z;

        animator.SetFloat("VelY", zVel);
        animator.SetFloat("VelX", xVel);
    }


    public void RotateToInputDirection()
    {
        if (Strafing)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x,
                MathFunctions.AngleYFromVector(LockDirection), transform.eulerAngles.z);
            ResetDirectionInput();
        }
        else if (WorldSpaceInput.magnitude > 0f)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x,
                MathFunctions.AngleYFromVector(WorldSpaceInput),
                transform.eulerAngles.z);
            ResetDirectionInput();
        }
    }


    // Adjust player rotation
    public void RotateTowardInput()
    {
        var deltaTime = Time.unscaledDeltaTime;

        var currentRotation = transform.eulerAngles.y;
        var rotationAngle = lastInputAngle_ - currentRotation;
        rotationAngle = MathFunctions.NormalizeAngle180(rotationAngle);

        var rotationSpeed = 180f / MaxRotationTime * Mathf.Sign(rotationAngle);
        var rotateAmount = rotationSpeed * deltaTime;
        if (Mathf.Abs(rotateAmount) > Mathf.Abs(rotationAngle))
        {
            rotateAmount = rotationAngle;
        }

        currentRotation += rotateAmount;
        currentRotation = MathFunctions.NormalizeAngle(currentRotation);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, currentRotation, transform.eulerAngles.z);
    }


    public void Jump()
    {
        OnPlayerAction(this, PlayerActions.Jump);

        AttacksDone = 0;
        var anim = GetComponent<Animator>();
        if (anim) anim.SetTrigger("Jump");

        SwitchState(new CharacterStateAirborne(this, true));
        var jumpVelocity = 2 * MaxJumpHeight * RunSpeed / (MaxJumpLength / 2);
        Velocity = new Vector3(Velocity.x, jumpVelocity, Velocity.z);


        PlayJumpFeedback();
    }


    public void Dash()
    {
        OnPlayerAction(this, PlayerActions.Dash);

        AttacksDone = 0;
        dashCooldown_.Reset();
        if (WorldSpaceInput.magnitude > 0f)
        {
            Velocity = WorldSpaceInput.normalized * DashSpeed;
            var angle = Mathf.Atan2(WorldSpaceInput.x, WorldSpaceInput.z) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, angle, transform.eulerAngles.z);
            lastInputAngle_ = angle;
        }
        Velocity = transform.forward * DashSpeed;

        SwitchState(new CharacterStateDashing(this));

        PlayDashFeedback();
    }


    public void StartAttackCooldown()
    {
        attackCooldown_ = new Timer(ComboCooldown);
    }


    public void HeavyAttack()
    {
        SwitchState(new CharacterStateAttacking(this, CharacterStateAttacking.AttackType.Heavy));
    }


    public void LightAttack()
    {
        SwitchState(new CharacterStateAttacking(this, CharacterStateAttacking.AttackType.Light));
    }


    public void ActivateHitbox()
    {
        HandleEvent(Events.ActivateHitbox);
    }


    public void DeactivateHitbox()
    {
        HandleEvent(Events.DeactivateHitbox);
    }


    public void AttackInterruptible()
    {
        HandleEvent(Events.ActionInterruptible);
    }


    public void AttackEnded(int attackIndex)
    {
        switch (attackIndex)
        {
            case 1:
                HandleEvent(Events.ActionFinished1);
                break;
            case 2:
                HandleEvent(Events.ActionFinished2);
                break;
            case 3:
                HandleEvent(Events.ActionFinished3);
                break;
        }
    }


    // worldSpaceInput should be a horizontal vector with magnitude [0:1]
    public void AccelerateInDirection(Vector3 worldSpaceInput, float accelerationTime)
    {
        var deltaTime = Time.unscaledDeltaTime;

        var horizontalVelocity = new Vector3(Velocity.x, 0f, Velocity.z);
        if (horizontalVelocity.magnitude <= RunSpeed) MaxSpeed = RunSpeed;

        var desiredVelocity = worldSpaceInput * MaxSpeed;

        // Apply strafe multiplier for backwards movement
        var localInput = transform.InverseTransformVector(worldSpaceInput);
        desiredVelocity *= Mathf.Lerp(BackwardsStrafeMultiplier, 1f, 1f + Mathf.Clamp(localInput.z, -1f, 0f));

        var accelerationAmount = MaxSpeed / accelerationTime * deltaTime;

        var desiredAcceleration = desiredVelocity - horizontalVelocity;
        if (accelerationAmount > desiredAcceleration.magnitude)
        {
            accelerationAmount = desiredAcceleration.magnitude;
        }
        var accelerationDirection = desiredAcceleration.normalized;
        Velocity += accelerationDirection * accelerationAmount;
    }


    public void ApplyGravity()
    {
        var deltaTime = Time.unscaledDeltaTime;

        Velocity += Gravity * deltaTime;
    }


    public void PlayFootstepFeedback(Vector3 position)
    {
        var e = RuntimeManager.CreateInstance(FootstepSound);
        var soundTag = "Default";
        var tags = new[] {"Default", "Dirt", "Stone"};
        RaycastHit hit;
        if (Physics.Raycast(position, Vector3.down, out hit, Mathf.Infinity, CollisionMask))
        {
            var currentTag = hit.collider.tag;
            if (tags.Contains(currentTag))
                soundTag = currentTag;
        }
        foreach (var s in tags)
        {
            e.setParameterValue(s, s.Equals(soundTag) ? 1f : 0f);
        }
        e.set3DAttributes(transform.position.To3DAttributes());
        e.start();
        e.release();
    }


    public void PlayJumpFeedback()
    {
        RuntimeManager.PlayOneShot(JumpSound, transform.position);
    }


    public void PlayLandingFeedback()
    {
        if (Velocity.y > Gravity.y * LandingFeedbackTimeThreshold)
            return;
        RuntimeManager.PlayOneShot(LandSound, BottomPos);
    }


    public void PlayDashFeedback()
    {
        RuntimeManager.PlayOneShot(DashSound, transform.position);
    }


    private void PlayHitFeedback(HitBox attacker, CharacterHealth attackee, int damage, Vector3 hitPosition)
    {
        //var particles = hitParticles_[hitParticlesIndex_];
        //if (++hitParticlesIndex_ >= hitParticles_.Length) hitParticlesIndex_ = 0;
        //if (particles)
        //{
        //    particles.transform.position = hitPosition;
        //    particles.transform.parent = null;
        //    foreach (var particleSys in particles.GetComponentsInChildren<ParticleSystem>())
        //    {
        //        particleSys.Play();
        //    }
        //}
    }


    public void SetTrailActive(CharacterStateAttacking.AttackType type, bool active)
    {
        switch (type)
        {
            case CharacterStateAttacking.AttackType.Heavy:
                if (HeavyAttackTrail)
                {
                    HeavyAttackTrail.time = active ? heavyAttackTrailTime_ : 0f;
                }
                break;
            case CharacterStateAttacking.AttackType.Light:
                if (LightAttackTrailRight)
                {
                    LightAttackTrailRight.time = active ? lightAttackTrailTime_ : 0f;
                }
                if (LightAttackTrailLeft)
                {
                    LightAttackTrailLeft.time = active ? lightAttackTrailTime_ : 0f;
                }
                break;
            default:
                throw new ArgumentOutOfRangeException("type", type, null);
        }
    }


    // Does not cover every condition required to jump, but the rest are covered by states
    public bool CanJump
    {
        get { return Input.GetButtonDown("Jump"); }
    }


    // Does not cover every condition required to dash, but the rest are covered by states
    public bool CanDash
    {
        get { return dashCooldown_.IsDone && Input.GetButtonDown("Dash") && !HasDashed; }
    }


    public bool CanHeavyAttack
    {
        get { return attackCooldown_.IsDone && Input.GetButtonDown("Heavy Attack"); }
    }


    public bool CanLightAttack
    {
        get { return attackCooldown_.IsDone && Input.GetButtonDown("Light Attack"); }
    }


    public Vector3 Center
    {
        get
        {
            return transform.position + new Vector3(characterController_.center.x * transform.lossyScale.x,
                       characterController_.center.y * transform.lossyScale.y,
                       characterController_.center.z * transform.lossyScale.z);
        }
    }


    public float Radius
    {
        get { return characterController_.radius * Mathf.Max(transform.lossyScale.x, transform.lossyScale.z); }
    }


    public float Height
    {
        get { return characterController_.height * transform.lossyScale.y; }
    }


    public Vector3 Velocity { get; set; }


    public Vector3 BottomPos
    {
        get { return Center + Vector3.down * Height / 2f; }
    }


    public Vector3 Gravity
    {
        get { return Vector3.down * (2 * MaxJumpHeight * Mathf.Pow(RunSpeed, 2)) / Mathf.Pow(MaxJumpLength / 2, 2); }
    }


    public float CameraRotation
    {
        get { return cameraTransform_.eulerAngles.y; }
    }


    public float MaxSlopeAngle
    {
        get { return characterController_.slopeLimit; }
    }


    public bool TouchesGround
    {
        get { return characterController_.isGrounded; }
    }


    // Returns player input in relation to camera direction
    public Vector3 WorldSpaceInput
    {
        get
        {
            var input = Input2D;
            var inputAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg;
            var worldSpaceInputAngle =
                CameraRotation +
                inputAngle;
            // Might be able to optimize by not converting back and forth between vector and angle
            return new Vector3(Mathf.Sin(Mathf.Deg2Rad * worldSpaceInputAngle) * input.magnitude, 0f,
                Mathf.Cos(Mathf.Deg2Rad * worldSpaceInputAngle) * input.magnitude);
        }
    }


    public Vector2 Input2D
    {
        get
        {
            var input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            if (input.magnitude > 1f) input.Normalize();
            return input;
        }
    }


    public class NotGroundedException : Exception
    {
        public NotGroundedException(string message) : base(message)
        {
        }
    }


    public Vector3 GroundNormal
    {
        get
        {
            Assert.IsTrue(TouchesGround);

            RaycastHit hit;
            if (Physics.SphereCast(Center,
                Radius, Vector3.down,
                out hit, Height / 2 - Radius + GroundCheckDistance, CollisionMask))
            {
                return hit.normal;
            }

            throw new NotGroundedException("Ground normal should only be accessed while touching ground.");
        }
    }


    public int AttacksDone { get; set; }


    public float MaxSpeed { get; set; }


    public bool HasDashed { get; set; }


    public bool HasAirAttacked { get; set; }
}