﻿using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Respawner : MonoBehaviour
{
    public float RespawnDelay = 3f;


    private Timer respawnTimer_;


    // Use this for initialization
    private void Start()
    {
        respawnTimer_ = new Timer(RespawnDelay, true);

        var saveData = IO.Instance.LoadGame();
        if (saveData == null) return;
        if (saveData.Checkpoint2.Equals(Vector3.zero)) return;
        transform.position = saveData.Checkpoint2;
        if (!saveData.Checkpoint) return;
        saveData.Checkpoint.Activate(this);
        
    }

    // Update is called once per frame
    private void Update()
    {
        if (Pause.Instance.Paused) return;

        var deltaTime = Time.unscaledDeltaTime;

        if (respawnTimer_.Update(deltaTime))
        {
            InstantRespawn();
        }
    }


    // Used to start the respawn process, called from outside

    public void StartRespawn()
    {
        respawnTimer_ = new Timer(RespawnDelay);
    }


    // The actual respawn method, will be called when respawn delay is done
    [Button]
    public void InstantRespawn()
    {
        SceneHandler.Instance.ReloadLevel();
    }


    public Checkpoint Checkpoint { get; set; }
}