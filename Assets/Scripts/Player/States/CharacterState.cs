﻿using UnityEngine;

public abstract class CharacterState
{
    private readonly Controller3D character_;


    protected Controller3D Character
    {
        get { return character_; }
    }


    public abstract void Enter();

    public abstract void Exit();

    // Returns true if character has entered a new state which should be updated
    public abstract bool Update();

    // Returns true if character has entered a new state which should be react to collisions
    public abstract bool HandleCollisions(CollisionFlags collisionFlags, Vector3 movement);

    public virtual void HandleEvent(Controller3D.Events e)
    {
    }


    protected CharacterState(Controller3D character)
    {
        character_ = character;
    }
}