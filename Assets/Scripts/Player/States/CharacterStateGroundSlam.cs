﻿using System;
using FMODUnity;
using UnityEngine;

public class CharacterStateGroundSlam : CharacterState
{
    private readonly Timer mainTimer_;
    private readonly Timer endlagTimer_;
    private readonly Animator animator_;
    private Vector3 realVelocity_;


    public CharacterStateGroundSlam(Controller3D character) : base(character)
    {
        mainTimer_ = new Timer(Character.GroundSlamMaxTime, true);
        endlagTimer_ = new Timer(Character.ShockWaveEndlag, true);
        animator_ = Character.GetComponent<Animator>();
    }

    public override void Enter()
    {
        Character.OnPlayerAction(Character, Controller3D.PlayerActions.AirHeavyAttack);

        Character.HasAirAttacked = true;
        SetLucerneActive(true);
        animator_.SetBool("Ground Slam", true);
        Character.Velocity = Vector3.zero;
        realVelocity_ = Character.Velocity;

        Character.RotateToInputDirection();
    }

    public override void Exit()
    {
        SetLucerneActive(false);
        animator_.SetBool("Ground Slam", false);
        Character.ShockWaveHitBox.Deactivate();

        Character.ResetDirectionInput();
    }

    public override bool Update()
    {
        var deltaTime = TimeManager.UnscaledDeltaTime;

        Character.Velocity = realVelocity_;

        if (mainTimer_.Update(deltaTime))
        {
            Character.HeavyAttackHitBox.Deactivate();
            Character.SetTrailActive(CharacterStateAttacking.AttackType.Heavy, false);
            Character.SwitchState(new CharacterStateAirborne(Character));
        }

        if (endlagTimer_.Update(deltaTime))
        {
            Character.SwitchState(new CharacterStateGrounded(Character));
        }

        return false;
    }

    public override bool HandleCollisions(CollisionFlags collisionFlags, Vector3 movement)
    {
        if ((collisionFlags & CollisionFlags.Below) != CollisionFlags.Below) return false;
        if (!(Vector3.Angle(Character.GroundNormal, Vector3.up) <= Character.MaxSlopeAngle)) return false;

        Slam();

        return false;
    }


    public override void HandleEvent(Controller3D.Events e)
    {
        switch (e)
        {
            case Controller3D.Events.ActivateHitbox:
                Character.HeavyAttackHitBox.Damage = Character.HeavyAttackDamage;
                Character.HeavyAttackHitBox.HitstopTime = Character.HeavyAttackHitstopTime;
                Character.HeavyAttackHitBox.MaxTargets = Character.HeavyAttackMaxTargets;
                Character.HeavyAttackHitBox.Activate();
                Character.SetTrailActive(CharacterStateAttacking.AttackType.Heavy, true);
                Character.Velocity = new Vector3(0f, -Character.GroundSlamSpeed, 0f);
                realVelocity_ = Character.Velocity;
                mainTimer_.Reset();
                break;
            default:
                throw new ArgumentOutOfRangeException("e", e, null);
        }
    }


    private void Slam()
    {
        mainTimer_.Finish();
        Character.HeavyAttackHitBox.Deactivate();
        Character.SetTrailActive(CharacterStateAttacking.AttackType.Heavy, false);
        endlagTimer_.Reset();
        Character.Velocity = Vector3.zero;
        realVelocity_ = Character.Velocity;
        Character.ShockWaveHitBox.Damage = Character.ShockWaveDamage;
        Character.ShockWaveHitBox.HitstopTime = Character.HeavyAttackHitstopTime;
        Character.ShockWaveHitBox.MaxTargets = Character.ShockWaveMaxTargets;
        Character.ShockWaveHitBox.Activate();
        RuntimeManager.PlayOneShot(Character.GroundSlamLandingSound, Character.transform.position);
    }


    private void SetLucerneActive(bool active)
    {
        Character.HandLucerne.SetActive(active);
        Character.BackLucerne.SetActive(!active);
    }
}