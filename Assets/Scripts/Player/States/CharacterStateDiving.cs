﻿using UnityEngine;

public class CharacterStateDiving : CharacterState
{
    private readonly Timer mainTimer_;
    private readonly Timer startupTimer_;
    private readonly Timer endTimer_;
    private float angle_;
    private Animator animator_;
    private Vector3 realVelocity_;


    public CharacterStateDiving(Controller3D character) : base(character)
    {
        startupTimer_ = new Timer(Character.DiveStartupTime);
        endTimer_ = new Timer(Character.DiveEndingTime, true);
        mainTimer_ = new Timer(Character.DiveMaxTime, true);
    }


    public override void Enter()
    {
        Character.OnPlayerAction(Character, Controller3D.PlayerActions.AirLightAttack);

        Character.HasAirAttacked = true;

        Character.Velocity = Vector3.zero;
        realVelocity_ = Character.Velocity;
        
        angle_ = (Character.Strafing
                     ? Mathf.Max(Character.DiveAngle,
                         Vector3.Angle(Character.LockDirection,
                             new Vector3(Character.LockDirection.x, 0f, Character.LockDirection.z)))
                     : Character.DiveAngle);

        animator_ = Character.GetComponent<Animator>();
        if (animator_)
        {
            animator_.SetBool("Dive Attack", true);
        }

        Character.RotateToInputDirection();
    }


    public override void Exit()
    {
        if (animator_)
        {
            animator_.SetBool("Dive Attack", false);
        }

        Character.LightAttackHitBox.Deactivate();
        Character.SetTrailActive(CharacterStateAttacking.AttackType.Light, false);

        Character.ResetDirectionInput();
    }


    public override bool Update()
    {
        var deltaTime = TimeManager.UnscaledDeltaTime;

        Character.Velocity = realVelocity_;
        
        if (!startupTimer_.IsDone)
        {
            if (startupTimer_.Update(deltaTime))
            {
                var x = Mathf.Sin(Mathf.Deg2Rad * Character.transform.eulerAngles.y);
                var z = Mathf.Cos(Mathf.Deg2Rad * Character.transform.eulerAngles.y);
                Character.Velocity = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle_) * x,
                                         -Mathf.Sin(Mathf.Deg2Rad * angle_),
                                         Mathf.Cos(Mathf.Deg2Rad * angle_) * z) * Character.DiveSpeed;
                realVelocity_ = Character.Velocity;

                
                mainTimer_.Reset();
            }
        }

        if (mainTimer_.Update(deltaTime))
        {
            Character.SwitchState(new CharacterStateAirborne(Character));
        }

        if (!endTimer_.IsDone)
        {
            endTimer_.Update(deltaTime);
            
            if (endTimer_.IsDone)
            {
                Character.SwitchState(new CharacterStateGrounded(Character));
            }
        }

        return false;
    }


    public override bool HandleCollisions(CollisionFlags collisionFlags, Vector3 movement)
    {
        if ((collisionFlags & CollisionFlags.Below) != CollisionFlags.Below) return false;
        if (!(Vector3.Angle(Character.GroundNormal, Vector3.up) <= Character.MaxSlopeAngle)) return false;

        animator_.SetBool("Grounded", true);
        EndAttack();

        return false;
    }


    public override void HandleEvent(Controller3D.Events e)
    {
        switch (e)
        {
            case Controller3D.Events.DeactivateHitbox:
                Character.LightAttackHitBox.Deactivate();
                Character.SetTrailActive(CharacterStateAttacking.AttackType.Light, false);
                break;
        }
    }


    private void EndAttack()
    {
        Character.LightAttackHitBox.Damage = Character.LightAttackDamage;
        Character.LightAttackHitBox.HitstopTime = Character.LightAttackHitstopTime;
        Character.LightAttackHitBox.MaxTargets = Character.LightAttackMaxTargets;
        Character.LightAttackHitBox.Activate();

        Character.SetTrailActive(CharacterStateAttacking.AttackType.Light, true);
        Character.Velocity = Vector3.zero;
        realVelocity_ = Character.Velocity;
        endTimer_.Reset();
        mainTimer_.Finish();
    }
}