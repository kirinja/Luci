﻿using UnityEngine;

public class CharacterStateAirborne : CharacterState
{
    private bool jumping_;
    private bool gliding_;


    public CharacterStateAirborne(Controller3D character, bool jumping = false) : base(character)
    {
        jumping_ = jumping;
    }


    public override void Enter()
    {
        //Character.MaxSpeed = Character.RunSpeed;
        var anim = Character.GetComponent<Animator>();
        if (anim)
        {
            anim.SetBool("Grounded", false);
        }
    }


    public override void Exit()
    {
    }


    public override bool Update()
    {
        var deltaTime = Time.unscaledDeltaTime;

        // Dash code
        if (Character.CanDash)
        {
            Character.Dash();
            return true;
        }

        // Attacks
        if (Character.CanHeavyAttack && !Character.HasAirAttacked)
        {
            Character.SwitchState(new CharacterStateGroundSlam(Character));
            return true;
        }
        if (Character.CanLightAttack && !Character.HasAirAttacked)
        {
            Character.SwitchState(new CharacterStateDiving(Character));
            return true;
        }

        // Gravity
        if (Character.TouchesGround && Character.Velocity.y <= 0)
        {
            var groundNormal = Character.GroundNormal;
            var normalVectorWithInvertedY = new Vector3(groundNormal.x, -groundNormal.y, groundNormal.z);
            if (!gliding_)
            {
                gliding_ = true;
            }

            Character.Velocity += normalVectorWithInvertedY * Character.Gravity.magnitude * deltaTime;
        }
        else
        {
            gliding_ = false;
            Character.ApplyGravity();
        }


        var minJumpSpeed = 2 * Character.MinJumpHeight * Character.RunSpeed / (Character.MaxJumpLength / 2);
        if (jumping_ && !Input.GetButton("Jump") && Character.Velocity.y > minJumpSpeed)
        {
            Character.Velocity = new Vector3(Character.Velocity.x, minJumpSpeed, Character.Velocity.z);
            jumping_ = false;
        }

        // Accelerate in the direction of player input
        if (!gliding_) Character.AccelerateInDirection(Character.WorldSpaceInput, Character.AirAccelerationTime);

        if (!Character.Strafing)
            Character.RotateTowardInput();
        else
        {
            Character.transform.eulerAngles = new Vector3(Character.transform.eulerAngles.x,
                MathFunctions.AngleYFromVector(Character.LockDirection), Character.transform.eulerAngles.z);
            Character.ResetDirectionInput();
        }

        return false;
    }


    public override bool HandleCollisions(CollisionFlags collisionFlags, Vector3 movement)
    {
        // Change to grounded state if it hit the ground
        if ((CollisionFlags.Below & collisionFlags) == CollisionFlags.Below && Character.Velocity.y < 0)
        {
            if (Vector3.Angle(Character.GroundNormal, Vector3.up) <= Character.MaxSlopeAngle)
            {
                Character.PlayLandingFeedback();
                Character.SwitchState(
                    new CharacterStateGrounded(Character));

                return true;
            }
        }
        if ((CollisionFlags.Above & collisionFlags) == CollisionFlags.Above)
            Character.Velocity = new Vector3(Character.Velocity.x, 0f, Character.Velocity.z);

        return false;
    }
}