﻿using System;
using FMODUnity;
using UnityEngine;

public class CharacterStateAttacking : CharacterState
{
    private readonly AttackType attackType_;
    private bool interruptible_;
    private Animator animator_;
    private Vector3 inputDirection_;


    public enum AttackType
    {
        Heavy,
        Light
    }


    public CharacterStateAttacking(Controller3D character, AttackType attackType) : base(character)
    {
        attackType_ = attackType;
    }


    public override void Enter()
    {
        Character.OnPlayerAction(Character,
            attackType_ == AttackType.Light
                ? Controller3D.PlayerActions.LightAttack
                : Controller3D.PlayerActions.HeavyAttack);

        //Character.Velocity = Vector3.zero;
        if (attackType_ == AttackType.Heavy)
        {
            SetLucerneActive(true);
        }
        ++Character.AttacksDone;

        if (Character.AttacksDone > 1)
        {
            Character.OnPlayerAction(Character, Controller3D.PlayerActions.AttackCombo);
        }

        animator_ = Character.GetComponent<Animator>();
        if (animator_)
        {
            animator_.SetTrigger((attackType_ == AttackType.Heavy ? "Heavy Attack " : "Light Attack ") +
                                 Character.AttacksDone);
        }

        PlayAttackFeedback();

        Character.RotateToInputDirection();
        inputDirection_ = Character.WorldSpaceInput;
    }


    public override void Exit()
    {
        if (attackType_ == AttackType.Heavy)
        {
            SetLucerneActive(false);
        }
        if (Character.AttacksDone == Character.ComboLength)
        {
            Character.StartAttackCooldown();
            Character.AttacksDone = 0;
        }
        Character.SetTrailActive(attackType_, false);

        Character.ResetDirectionInput();
    }


    public override bool Update()
    {
        if (interruptible_)
        {
            if (Character.CanHeavyAttack)
            {
                Character.HeavyAttack();
                return false;
            }
            if (Character.CanLightAttack)
            {
                Character.LightAttack();
                return false;
            }
            if (Character.CanDash)
            {
                Character.Dash();
                return false;
            }
            if (Character.CanJump)
            {
                Character.Jump();
                return false;
            }
        }
        else if (attackType_ == AttackType.Light && Character.CanDash)
        {
            Character.LightAttackHitBox.Deactivate();
            Character.Dash();
            return true;
        }

        //Character.AccelerateInDirection(Vector3.zero, Character.AttackGlideTime);

        if (animator_)
        {
            Character.Velocity = inputDirection_ * animator_.GetFloat("Movement") * Character.RunSpeed;
            //Character.Velocity =
            //    Character.transform.TransformVector(new Vector3(0f, 0f,
            //        animator_.GetFloat("Movement") * Character.RunSpeed));
            animator_.SetLayerWeight(animator_.GetLayerIndex("Attack Lower"), 1f - Mathf.Clamp01(animator_.GetFloat("Movement")));
        }

        return false;
    }


    public override bool HandleCollisions(CollisionFlags collisionFlags, Vector3 movement)
    {
        return false;
    }


    public override void HandleEvent(Controller3D.Events e)
    {
        switch (e)
        {
            case Controller3D.Events.ActionInterruptible:
                if (Character.AttacksDone < Character.ComboLength)
                    interruptible_ = true;
                break;
            case Controller3D.Events.ActivateHitbox:
                Character.SetTrailActive(attackType_, true);
                switch (attackType_)
                {
                    case AttackType.Heavy:
                        Character.HeavyAttackHitBox.Damage = Character.HeavyAttackDamage;
                        Character.HeavyAttackHitBox.HitstopTime = Character.HeavyAttackHitstopTime;
                        Character.HeavyAttackHitBox.MaxTargets = Character.HeavyAttackMaxTargets;
                        Character.HeavyAttackHitBox.Activate();
                        break;
                    case AttackType.Light:
                        Character.LightAttackHitBox.Damage = Character.LightAttackDamage;
                        Character.LightAttackHitBox.HitstopTime = Character.LightAttackHitstopTime;
                        Character.LightAttackHitBox.MaxTargets = Character.LightAttackMaxTargets;
                        Character.LightAttackHitBox.Activate();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                break;
            case Controller3D.Events.DeactivateHitbox:
                Character.SetTrailActive(attackType_, false);
                switch (attackType_)
                {
                    case AttackType.Heavy:
                        Character.HeavyAttackHitBox.Deactivate();
                        break;
                    case AttackType.Light:
                        var trail = Character.LightAttackHitBox.GetComponentInChildren<TrailRenderer>();
                        if (trail)
                            trail.time = 0f;
                        Character.LightAttackHitBox.Deactivate();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                break;
            // HACK: Terrible switch case
            case Controller3D.Events.ActionFinished1:
                if (Character.AttacksDone == 1)
                {
                    End();
                }
                break;
            case Controller3D.Events.ActionFinished2:
                if (Character.AttacksDone == 2)
                    End();
                break;
            case Controller3D.Events.ActionFinished3:
                if (Character.AttacksDone == 3)
                    End();
                break;
            default:
                throw new ArgumentOutOfRangeException("e", e, null);
        }
    }


    private void End()
    {
        Character.SwitchState(new CharacterStateGrounded(Character));
        Character.AttacksDone = 0;
    }


    private void SetLucerneActive(bool active)
    {
        Character.HandLucerne.SetActive(active);
        Character.BackLucerne.SetActive(!active);
    }


    private void PlayAttackFeedback()
    {
        RuntimeManager.PlayOneShot(attackType_ == AttackType.Heavy
            ? Character.HeavyAttackSwingSound
            : Character.LightAttackSwingSound);
    }
}