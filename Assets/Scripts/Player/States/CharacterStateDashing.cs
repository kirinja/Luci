﻿using UnityEngine;

public class CharacterStateDashing : CharacterState
{
    private readonly Timer dashTimer_;
    private PlayerCombat combat_;

    public CharacterStateDashing(Controller3D character) : base(character)
    {
        dashTimer_ = new Timer(character.DashLength / character.DashSpeed);
    }


    public override void Enter()
    {
        SetFeedbackState(true);
        Character.MaxSpeed = Character.SprintSpeed;
        combat_ = Character.GetComponent<PlayerCombat>();
        if (combat_)
        {
            combat_.Invulnerable = true;
        }
    }


    public override void Exit()
    {
        if (combat_)
        {
            combat_.Invulnerable = false;
        }
        SetFeedbackState(false);
        // Set player speed according to input
        Character.Velocity = Character.Velocity.normalized * Character.MaxSpeed;
    }


    private void SetFeedbackState(bool active)
    {
        var anim = Character.GetComponent<Animator>();
        if (anim)
        {
            anim.SetBool("Dash", active);
        }
        if (Character.DashParticles)
        {
            foreach (var particleSys in Character.DashParticles.GetComponentsInChildren<ParticleSystem>())
            {
                if (active)
                    particleSys.Play();
                else
                    particleSys.Stop();
            }
        }

        foreach (var renderer in Character.GetComponentsInChildren<MeshRenderer>())
        {
            renderer.enabled = !active;
        }
        foreach (var renderer in Character.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            renderer.enabled = !active;
        }
        foreach (var particleSystem in Character.ParticlesToHideWhileDashing)
        {
            if (active)
            {
                particleSystem.Clear();
                particleSystem.Stop();
            }
            else
            {
                particleSystem.Play();
            }
        }
    }


    public override bool Update()
    {
        var deltaTime = Time.unscaledDeltaTime;

        // Check if grounded
        RaycastHit rayHit;
        if (Physics.SphereCast(Character.Center, Character.Radius, Vector3.down, out rayHit,
            Character.Height / 2 - Character.Radius + Character.GroundCheckDistance, Character.CollisionMask))
        {
            if (Vector3.Angle(rayHit.normal, Vector3.up) <= Character.MaxSlopeAngle)
            {
                if (Character.CanHeavyAttack)
                {
                    Character.HeavyAttack();
                    return true;
                }
                if (Character.CanLightAttack)
                {
                    Character.LightAttack();
                    return true;
                }
                if (Character.CanJump)
                {
                    Character.Jump();
                    return true;
                }
            }
            
        }
        if (Character.CanHeavyAttack && !Character.HasAirAttacked)
        {
            Character.SwitchState(new CharacterStateGroundSlam(Character));
            return true;
        }
        if (Character.CanLightAttack && !Character.HasAirAttacked)
        {
            Character.SwitchState(new CharacterStateDiving(Character));
            return true;
        }
        if (Character.CanJump)
        {
            Character.HasDashed = true;
            Character.SwitchState(new CharacterStateAirborne(Character));
            return true;
        }

        // Will exit dashing state if timer is done
        if (dashTimer_.Update(deltaTime))
        {
            // Check if grounded
            RaycastHit hit;
            if (Physics.SphereCast(Character.Center, Character.Radius, Vector3.down, out hit,
                Character.Height / 2 - Character.Radius + Character.GroundCheckDistance, Character.CollisionMask))
            {
                if (Vector3.Angle(hit.normal, Vector3.up) <= Character.MaxSlopeAngle)
                {
                    if (!Character.TouchesGround)
                        Character.Move(Vector3.down * (hit.distance - (Character.Height / 2 - Character.Radius) +
                                                       Character.GroundCheckDistance));

                    Character.SwitchState(new CharacterStateGrounded(Character));
                    return true;
                }
            }

            // If it didn't already enter a grounded state, go into the airborne state
            Character.SwitchState(new CharacterStateAirborne(Character));
            Character.HasDashed = true;
            return true;
        }

        return false;
    }


    public override bool HandleCollisions(CollisionFlags collisionFlags, Vector3 movement)
    {
        return false;
    }
}