﻿using UnityEngine;

public class CharacterStateGrounded : CharacterState
{
    private float lastFrameFootstepLeft_;
    private float lastFrameFootstepRight_;


    public CharacterStateGrounded(Controller3D character) : base(character)
    {
    }


    public override void Enter()
    {
        Character.HasDashed = false;
        Character.HasAirAttacked = false;
        Character.Velocity = new Vector3(Character.Velocity.x, 0f, Character.Velocity.z);

        var anim = Character.GetComponent<Animator>();
        if (anim)
        {
            anim.SetBool("Grounded", true);
        }
    }


    public override void Exit()
    {
    }


    public override bool Update()
    {
        // Jump code
        if (Character.CanJump)
        {
            Character.Jump();
            return true;
        }

        // Dash code
        if (Character.CanDash)
        {
            Character.Dash();
            return true;
        }

        // Attack code
        if (Character.CanHeavyAttack)
        {
            Character.HeavyAttack();
            return true;
        }
        if (Character.CanLightAttack)
        {
            Character.LightAttack();
            return true;
        }

        // Accelerate in the direction of player input
        Character.AccelerateInDirection(Character.WorldSpaceInput, Character.GroundAccelerationTime);

        if (!Character.Strafing)
            Character.RotateTowardInput();
        else
        {
            Character.transform.eulerAngles = new Vector3(Character.transform.eulerAngles.x,
                MathFunctions.AngleYFromVector(Character.LockDirection), Character.transform.eulerAngles.z);
            Character.ResetDirectionInput();
        }

        CheckFootsteps();

        return false;
    }


    private void CheckFootsteps()
    {
        //Check THIS FRAME to see if we need to play a sound for the left foot, RIGHT NOW...
        var anim = Character.GetComponent<Animator>();
        if (anim)
        {
            var currentFrameFootstepLeft =
                anim.GetFloat(
                    "Footstep Left"); //get left foot's CURVE FLOAT from the Animator Controller, from the LAST FRAME.
            if (currentFrameFootstepLeft >= 0 && lastFrameFootstepLeft_ < 0)
            {
                //is this frame's curve BIGGER than the last frames?
                Character.PlayFootstepFeedback(Character.LeftFoot.position);
            }
            lastFrameFootstepLeft_ =
                anim.GetFloat(
                    "Footstep Left"); //get left foot's CURVE FLOAT from the Animator Controller, from the CURRENT FRAME.


            //Check THIS FRAME to see if we need to play a sound for the right foot, RIGHT NOW...
            var currentFrameFootstepRight =
                anim.GetFloat(
                    "Footstep Right"); //get right foot's CURVE FLOAT from the Animator Controller, from the LAST FRAME.
            if (currentFrameFootstepRight < 0 && lastFrameFootstepRight_ > 0)
            {
                //is this frame's curve SMALLER than last frames?
                Character.PlayFootstepFeedback(Character.RightFoot.position);
            }
            lastFrameFootstepRight_ =
                anim.GetFloat(
                    "Footstep Right"); //get right foot's CURVE FLOAT from the Animator Controller, from the CURRENT FRAME.
        }
    }


    public override bool HandleCollisions(CollisionFlags collisionFlags, Vector3 movement)
    {
        // Change state if not grounded
        if ((CollisionFlags.Below & collisionFlags) != CollisionFlags.Below)
        {
            var slopeDistance = Mathf.Tan(Mathf.Deg2Rad * Character.MaxSlopeAngle) *
                                new Vector3(movement.x, 0f, movement.z).magnitude;

            RaycastHit hit;

            if (Physics.SphereCast(Character.Center, Character.Radius, Vector3.down, out hit,
                Character.Height / 2 - Character.Radius + Character.GroundCheckDistance + slopeDistance,
                Character.CollisionMask))
            {
                if (Vector3.Angle(hit.normal, Vector3.up) <= Character.MaxSlopeAngle)
                {
                    Character.Move(Vector3.down * (hit.distance - (Character.Height / 2 - Character.Radius) +
                                                   Character.GroundCheckDistance));

                    return false;
                }
            }


            Character.SwitchState(new CharacterStateAirborne(Character));
            return true;
        }

        return false;
    }
}