﻿using UnityEngine;


public class StationaryVisagePickup : MonoBehaviour
{
    public int FillAmount = 100;

    
    private void OnTriggerStay(Collider other)
    {
        var visage = other.GetComponent<Visage>();
        if (visage)
        {
            visage.Fill(FillAmount);
            Destroy();
        }
    }


    private void Destroy()
    {
        Destroy(gameObject);
    }
}
