﻿using UnityEngine;

public class PlayerPauseHandler : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        Pause.Instance.OnPauseChanged += OnPausedChanged;
    }


    private void OnPausedChanged(bool paused)
    {
        GetComponent<Animator>().enabled = !paused;
    }


    private void OnDestroy()
    {
        if (Pause.HasInstance)
            Pause.Instance.OnPauseChanged -= OnPausedChanged;
    }
}