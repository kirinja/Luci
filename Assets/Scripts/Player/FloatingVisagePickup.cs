﻿using UnityEngine;

public class FloatingVisagePickup : MonoBehaviour
{
    public float DelayTime = 0.3f;
    public float Acceleration = 1f;
    public float StartVelocity = 3;
    public int FillAmount = 1;


    private Transform target_;
    private Vector3 velocity_;
    private Timer delayTimer_;


    // Use this for initialization
    private void Start()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        target_ = player.GetComponent<Visage>().VisageMask;
        var angle = Mathf.Deg2Rad * MathFunctions.NormalizeAngle(Random.Range(float.Epsilon, 360f));
        var x = Mathf.Cos(angle);
        var z = Mathf.Sin(angle);
        var direction = new Vector3(x, 0f, z).normalized;
        velocity_ = direction * StartVelocity;
        delayTimer_ = new Timer(DelayTime);
    }


    // Update is called once per frame
    private void Update()
    {
        var deltaTime = TimeManager.DeltaTime;
        if (delayTimer_.Update(deltaTime))
        {
            velocity_ = Vector3.zero;
        }

        if (delayTimer_.IsDone)
        {
            var direction = (target_.position - transform.position).normalized;
            velocity_ = direction * velocity_.magnitude;
            velocity_ += Acceleration * direction * deltaTime;
        }
        else
        {
            velocity_ = velocity_.normalized * Mathf.Lerp(StartVelocity, 0f, delayTimer_.PercentDone);
        }
    }


    private void FixedUpdate()
    {
        var deltaTime = TimeManager.DeltaTime;

        transform.position += velocity_ * deltaTime;
    }


    private void OnTriggerStay(Collider other)
    {
        if (!delayTimer_.IsDone) return;

        var visage = other.GetComponent<Visage>();
        if (visage)
        {
            visage.Fill(FillAmount);
            Destroy();
        }
    }


    private void Destroy()
    {
        Destroy(gameObject);
    }
}