﻿using UnityEngine;


[RequireComponent(typeof(Renderer))]
public class CorruptedMaterialReplacer : CorruptedObject
{
    public Material[] CorruptedMaterials;
    public Material[] CleansedMaterials;


    public override void UpdateCorruption()
    {
        var ren = GetComponent<Renderer>();
        ren.materials = Corrupted ? CorruptedMaterials : CleansedMaterials;
    }
}