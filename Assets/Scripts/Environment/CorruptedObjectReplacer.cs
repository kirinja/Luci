﻿using NaughtyAttributes;
using UnityEngine;

public class CorruptedObjectReplacer : CorruptedObject
{
    [Required] public Transform CorruptedObject;
    [Required] public Transform CleansedObject;


    public override void UpdateCorruption()
    {
        CorruptedObject.gameObject.SetActive(Corrupted);
        CleansedObject.gameObject.SetActive(!Corrupted);
    }
}