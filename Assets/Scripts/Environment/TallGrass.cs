﻿using FMODUnity;
using NaughtyAttributes;
using UnityEngine;

[RequireComponent(typeof(CharacterHealth))]
public class TallGrass : MonoBehaviour
{
    public float FadeTime = 2f;

    [BoxGroup("Sounds")] [EventRef] public string CutSound;


    private Timer fadeTimer_;
    private Timer animationTimer_;
    private Vector3[] originalScales_;


    // Use this for initialization
    private void Start()
    {
        var animators = GetComponentsInChildren<Animator>();
        animationTimer_ = animators.Length > 0
            ? new Timer(animators[0].GetCurrentAnimatorClipInfo(0)[0].clip.length, true)
            : new Timer(1f, true);
        foreach (var animator in animators)
        {
            animator.enabled = false;
        }

        var childTransforms = GetComponentsInChildren<Transform>();
        originalScales_ = new Vector3[childTransforms.Length];
        for (var i = 0; i < childTransforms.Length; ++i)
        {
            originalScales_[i] = childTransforms[i].localScale;
        }

        GetComponent<CharacterHealth>().OnDeath += OnDeath;
        fadeTimer_ = new Timer(FadeTime, true);
    }


    private void Update()
    {
        var deltaTime = TimeManager.DeltaTime;

        if (animationTimer_.Update(deltaTime))
        {
            fadeTimer_ = new Timer(FadeTime);
        }

        if (fadeTimer_.IsDone) return;

        if (fadeTimer_.Update(deltaTime))
        {
            Destroy(gameObject);
        }
        else
        {
            var childTransforms = GetComponentsInChildren<Transform>();
            for (var i = 0; i < childTransforms.Length; ++i)
            {
                if (childTransforms[i].gameObject != gameObject)
                    childTransforms[i].transform.localScale =
                        Vector3.Lerp(originalScales_[i], Vector3.zero, fadeTimer_.PercentDone);
            }
        }
    }


    private void OnDeath(CharacterHealth health)
    {
        foreach (var animator in GetComponentsInChildren<Animator>())
        {
            animator.enabled = true;
        }
        foreach (var grassCollider in GetComponentsInChildren<Collider>())
        {
            grassCollider.enabled = false;
        }
        animationTimer_.Reset();

        RuntimeManager.PlayOneShot(CutSound, transform.position);
    }
}