﻿using System.Collections;
using System.Collections.Generic;
using Panda;
using UnityEngine;

public class CorruptedSkyChanger : MonoBehaviour
{
    private Timer skyboxChangeTimer_;
    public float ChangeTime = 0.5f;

    private bool inCorruption;

	// Use this for initialization
	void Start ()
    {
		skyboxChangeTimer_ = new Timer(ChangeTime);
    }
	
	// Update is called once per frame
	void Update ()
	{
	    var deltaTime = TimeManager.DeltaTime;
        if (inCorruption && !skyboxChangeTimer_.IsDone)
        {
            // change to corruption skybox
            skyboxChangeTimer_.Update(deltaTime);
            var value = Mathf.SmoothStep(1.0f, 0.0f, skyboxChangeTimer_.PercentDone);
            Material skybox = RenderSettings.skybox;
            skybox.SetFloat("_Blend", value);
            RenderSettings.skybox = skybox;
        }
        else if (!inCorruption && !skyboxChangeTimer_.IsDone)
        {
            // change to normal skybox
            skyboxChangeTimer_.Update(deltaTime);
            var value = Mathf.SmoothStep(0.0f, 1.0f, skyboxChangeTimer_.PercentDone);
            Material skybox = RenderSettings.skybox;
            skybox.SetFloat("_Blend", value);
            RenderSettings.skybox = skybox;
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        //if (!Core) return;
        inCorruption = true;
        skyboxChangeTimer_.Reset();// = new Timer(ChangeTime);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        //if (!Core) return;
        inCorruption = false;
        skyboxChangeTimer_.Reset();// = new Timer(ChangeTime);
    }

    public void RevertToNormalSky()
    {
        inCorruption = false;
        skyboxChangeTimer_ = new Timer(ChangeTime);
    }

    private void OnDisable()
    {
        Material skybox = RenderSettings.skybox;
        skybox.SetFloat("_Blend", 1.0f);
        RenderSettings.skybox = skybox;
    }


}
