﻿using NaughtyAttributes;
using UnityEngine;


public abstract class CorruptedObject : MonoBehaviour
{
    [OnValueChanged("UpdateCorruption")] public bool Corrupted = true;
    public bool Destroying;


    // Use this for initialization
    private void Start()
    {
        UpdateCorruption();
    }


    private void OnDestroy()
    {
        Destroying = true;
    }


    public void Cleanse()
    {
        Corrupted = false;
        UpdateCorruption();
    }


    public void Corrupt()
    {
        Corrupted = true;
        UpdateCorruption();
    }


    public abstract void UpdateCorruption();
}