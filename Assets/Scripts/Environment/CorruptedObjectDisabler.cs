﻿using UnityEngine;

public class CorruptedObjectDisabler : CorruptedObject
{
    [Tooltip("True if object should be active when it is corrupted. If false, the object will be active when cleansed instead.")]
    public bool ActiveWhenCorrupted = true;


    public override void UpdateCorruption()
    {
        gameObject.SetActive(ActiveWhenCorrupted ? Corrupted : !Corrupted);
    }
}
