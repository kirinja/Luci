﻿using System.Collections;
using FMODUnity;
using NaughtyAttributes;
using Panda;
using UnityEngine;

public class GuardianAI : EnemyAI
{
    [BoxGroup("Rotation")] public float MaxRotationTime = 1.5f;
    [BoxGroup("Rotation")] [Required] public Transform Rotator;
    [BoxGroup("Rotation")] [Required] public Transform BeamHandle;

    //[BoxGroup("Attack")] [Required] public SphereCollider AggroCollider;
    [BoxGroup("Attack")] [Required] public HitBox BeamHitbox;
    [BoxGroup("Attack")] [MinValue(0f)] public float BeamDuration = 5f;
    [BoxGroup("Attack")] [MinValue(0f)] public float RechargeDuration = 5f;
    [BoxGroup("Attack")] public LayerMask LaserMask;
    [BoxGroup("Attack")] [Required] public ParticleSystem LaserHitParticles;

    [BoxGroup("Sounds")] [EventRef] public string LaserSound;
    [BoxGroup("Sounds")] [Required] public StudioEventEmitter LaserSoundEmitter;
    [BoxGroup("Sounds")] [EventRef] public string DamagedSound;
    [BoxGroup("Sounds")] [EventRef] public string DeathSound;

    [Required] public Transform DamagedParticles;

    private Status status_;

    private float attackRange_;

    private Transform player_;

    private Timer attackingTimer_;
    private Timer rechargeTimer_;


    private void Start()
    {
        //attackRange_ = AggroCollider.radius *
        //            Mathf.Max(Mathf.Max(AggroCollider.transform.lossyScale.x, AggroCollider.transform.lossyScale.y),
        //                AggroCollider.transform.lossyScale.z);

        attackRange_ = AggroRange;

        SetBeamLength(attackRange_);
        LaserSoundEmitter.Event = LaserSound;
        player_ = GameObject.FindGameObjectWithTag("Player").transform;

        attackingTimer_ = new Timer(BeamDuration);
        rechargeTimer_ = new Timer(RechargeDuration, true);

        var characterHealth = GetComponent<CharacterHealth>();
        if (characterHealth)
        {
            //characterHealth.OnDeath += OnDeath;
            characterHealth.OnDamaged += OnDamaged;
        }

        Deactivate();
    }

    private void Update()
    {
        IsPlayerClose = PlayerInsideAggroRange();
        IsActive = PlayerInsideActiveRange();

        UpdateStatus(IsActive);
    }

    private bool PlayerInsideAggroRange()
    {
        var layerMask = LayerMask.GetMask("Player");
        var hits = Physics.OverlapSphere(transform.position, AggroRange, layerMask);
        return hits.Length > 0;
    }

    private bool PlayerInsideActiveRange()
    {
        var layerMask = LayerMask.GetMask("Player");
        var hits = Physics.OverlapSphere(transform.position, ActiveRange, layerMask);
        return hits.Length > 0;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.3f);
        Gizmos.DrawSphere(transform.position, AggroRange);

        Gizmos.color = new Color(0, 1, 0, 0.3f);
        Gizmos.DrawSphere(transform.position + ActiveRangeOffset, ActiveRange);
    }


    [Task]
    private void RotateHorizontally()
    {
        var task = Task.current;

        var deltaTime = TimeManager.DeltaTime;
        var currentRotation = Rotator.eulerAngles.y;
        var direction = player_.GetComponent<Controller3D>().Center - Rotator.position;
        var rotationAngle = MathFunctions.AngleYFromVector(direction) - currentRotation;
        rotationAngle = MathFunctions.NormalizeAngle180(rotationAngle);

        var rotationSpeed = 180f / MaxRotationTime * Mathf.Sign(rotationAngle);
        var rotateAmount = rotationSpeed * deltaTime;
        if (Mathf.Abs(rotateAmount) > Mathf.Abs(rotationAngle))
        {
            rotateAmount = rotationAngle;
        }

        currentRotation += rotateAmount;
        currentRotation = MathFunctions.NormalizeAngle(currentRotation);
        Rotator.eulerAngles = new Vector3(0, currentRotation, 0);

        task.Succeed();
    }


    [Task]
    private void RotateVertically()
    {
        var task = Task.current;

        var deltaTime = TimeManager.DeltaTime;
        var currentRotation = BeamHandle.localEulerAngles.x;
        var direction = player_.GetComponent<Controller3D>().Center - BeamHandle.position;
        var rotationAngle = MathFunctions.AngleXFromVector(Rotator.InverseTransformVector(direction)) -
                            currentRotation;
        rotationAngle = MathFunctions.NormalizeAngle180(rotationAngle);

        var rotationSpeed = 180f / MaxRotationTime * Mathf.Sign(rotationAngle);
        var rotateAmount = rotationSpeed * deltaTime;
        if (Mathf.Abs(rotateAmount) > Mathf.Abs(rotationAngle))
        {
            rotateAmount = rotationAngle;
        }

        currentRotation += rotateAmount;
        currentRotation = MathFunctions.NormalizeAngle(currentRotation);
        BeamHandle.localEulerAngles = new Vector3(currentRotation, 0, 0);

        task.Succeed();
    }


    private void OnDamaged(ref int amount, AttackType attackType, Vector3 hitPosition)
    {
        RuntimeManager.PlayOneShot(DamagedSound, transform.position);
        DamagedParticles.transform.position = hitPosition;
        foreach (var particles in DamagedParticles.GetComponentsInChildren<ParticleSystem>())
        {
            particles.Stop();
            particles.Play();
        }
    }


    private void OnDeath(CharacterHealth healthComp)
    {
        StopAttacking();
        RuntimeManager.PlayOneShot(DeathSound, transform.position);
        StartCoroutine(Despawn());
    }


    private IEnumerator Despawn()
    {
        var rb = GetComponent<Rigidbody>();
        if (rb)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }
        var col = GetComponent<Collider>();
        if (col)
        {
            col.enabled = false;
        }

        // hardcoded value
        yield return new WaitForSeconds(10.0f);

        Destroy(gameObject);
    }


    private void StartAttacking()
    {
        BeamHitbox.Damage = AttackStrength;
        BeamHitbox.Activate();
        BeamHitbox.GetComponent<Renderer>().enabled = true; // TEMP
        LaserSoundEmitter.Play();
    }

    [Task]
    private void StopAttacking()
    {
        var task = Task.current;

        BeamHitbox.Deactivate();
        BeamHitbox.GetComponent<Renderer>().enabled = false; // TEMP
        LaserSoundEmitter.Stop();
        LaserHitParticles.Stop();

        if (task != null) task.Succeed();
    }


    [Task]
    private void Attack()
    {
        var task = Task.current;

        var deltaTime = TimeManager.DeltaTime;

        if (rechargeTimer_.Update(deltaTime))
        {
            StartAttacking();
            attackingTimer_ = new Timer(BeamDuration);
        }
        else if (!rechargeTimer_.IsDone)
        {
            task.Fail();
            return;
        }

        if (attackingTimer_.Update(deltaTime))
        {
            StopAttacking();
            rechargeTimer_ = new Timer(RechargeDuration);
            task.Fail();
            return;
        }

        AdjustBeamLength();

        if (!BeamHitbox.GetComponent<Renderer>().enabled) StartAttacking();

        task.Succeed();
    }


    private void AdjustBeamLength()
    {
        RaycastHit hit;
        if (Physics.Raycast(BeamHandle.position, BeamHandle.forward, out hit, attackRange_, LaserMask))
        {
            SetBeamLength(hit.distance);
            LaserHitParticles.transform.position = hit.point;
            if (!LaserHitParticles.isEmitting)
            {
                LaserHitParticles.Play();
            }
        }
        else
        {
            SetBeamLength(attackRange_);
            if (LaserHitParticles.isEmitting)
            {
                LaserHitParticles.Stop();
            }
        }
    }


    private void SetBeamLength(float length)
    {
        BeamHitbox.transform.localScale = new Vector3(BeamHitbox.transform.localScale.x, length / 2f,
            BeamHitbox.transform.localScale.z);
        BeamHitbox.transform.localPosition = new Vector3(BeamHitbox.transform.localPosition.x,
            BeamHitbox.transform.localPosition.y, length / 2f);
    }


    [Task]
    private void SinkIntoGround()
    {
        transform.position += Vector3.down * Time.deltaTime * 1.0f;

        Task.current.Succeed();
    }

    [Task]
    private void Death()
    {
        BeamHitbox.Deactivate();
        BeamHitbox.GetComponent<Renderer>().enabled = false; // TEMP
        LaserSoundEmitter.Stop();
        LaserHitParticles.Stop();

        RuntimeManager.PlayOneShot(DeathSound, transform.position);
        StartCoroutine(Despawn());

        Task.current.Succeed();
    }

    public override void Activate()
    {
        base.Activate();
        
        GetComponent<Rigidbody>().isKinematic = false;

        GetComponent<CapsuleCollider>().enabled = true;

        GetComponent<PandaBehaviour>().enabled = true;

        //BeamHitbox.GetComponent<Renderer>().enabled = true; // TEMP

        status_ = Status.Active;
    }

    public override void Deactivate()
    {
        base.Deactivate();

        // dont disable the navmesh agent if it has been activated once seems to fix some issues
        //GetComponent<NavMeshAgent>().enabled = false;
        
        GetComponent<Rigidbody>().isKinematic = true;

        GetComponent<CapsuleCollider>().enabled = false;

        GetComponent<PandaBehaviour>().enabled = false;

        BeamHitbox.GetComponent<Renderer>().enabled = false; // TEMP

        status_ = Status.Deactive;
    }

    private void UpdateStatus(bool active)
    {
        if (active && status_ == Status.Deactive)
            Activate();
        if (!active && status_ == Status.Active)
            Deactivate();
    }
}