﻿using System.Collections;
using System.Collections.Generic;
using Panda;
using UnityEngine;

public class EnableEnemesInArea : MonoBehaviour
{
    public List<PandaBehaviour> enemies_;

	// Use this for initialization
	void Awake ()
    {
        enemies_ = new List<PandaBehaviour>();
        var layerMask = 1 << LayerMask.NameToLayer("Enemy");// | 1 << LayerMask.NameToLayer("Ignore Raycast") | 1 << LayerMask.NameToLayer("Enemy Attack");
        //layerMask = ~layerMask;
        var ens = Physics.OverlapBox(transform.position, GetComponent<BoxCollider>().size,
            Quaternion.identity, layerMask);

        foreach (var e in ens)
        {
            var core = e.GetComponent<CoreAI>();
            if (!core)
            {
                var bt = e.GetComponent<PandaBehaviour>();
                if (bt)
                {
                    bt.enabled = false;
                    enemies_.Add(bt);
                }
                //e.gameObject.SetActive(false);
                //enemies_.Add(e.gameObject);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // enable all enemies within area
            foreach (var e in enemies_)
            {
                //e.SetActive(true);
                e.enabled = true;
            }
        }
    }


}

