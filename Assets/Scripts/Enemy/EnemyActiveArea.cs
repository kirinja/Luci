﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActiveArea : MonoBehaviour
{

    private EnemyAI ai_;

    // Use this for initialization
    private void Awake()
    {
        ai_ = GetComponentInParent<EnemyAI>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ai_.IsActive = true;
            ai_.Activate();
        }

    }


    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ai_.IsActive = false;
            ai_.Deactivate();
        }
    }
}
