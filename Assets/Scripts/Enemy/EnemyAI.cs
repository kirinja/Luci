﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using Panda;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [BoxGroup("Movement")][MinValue(0f)] public float MovementSpeed;
    [HideInInspector] // unused
    [Tooltip("How long the attack is active")]
    [BoxGroup("Attack")] public float AttackDuration;
    [BoxGroup("Attack")][MinValue(0)] public int AttackStrength;
    [HideInInspector] // unused
    [BoxGroup("Attack")] public float InvincibleTime;
    [Task] [HideInInspector]
    public bool IsPlayerClose;

    [BoxGroup("Area")] public float AggroRange;
    [BoxGroup("Area")] public float ActiveRange;
    [BoxGroup("Area")] public Vector3 ActiveRangeOffset;

    [BoxGroup("LockOn")] public float LockOnIndicatorOffset;


    [Task] public bool IsActive = false;

    public virtual void Deactivate() { }
    public virtual void Activate() { }

    protected enum Status
    {
        Active,
        Deactive
    }
}
