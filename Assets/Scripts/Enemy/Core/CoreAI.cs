﻿using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using NaughtyAttributes;
using Panda;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;
//using System = FMOD.System;

/// <summary>
/// BUG LIST:
/// - Attack is buggy with how the animations play
///   Triggers are called incorrectly check BT flow
/// </summary>
public class CoreAI : EnemyAI
{
    [BoxGroup("Name")] public string CoreAreaName;

    [MinValue(1)]
    [BoxGroup("Summon")] public int SummonAmount;
    [MinValue(1.0f)]
    [BoxGroup("Summon")] public float SummonMaxRange = 10.0f;
    [Tooltip("The enemy prefab to use for spawning")]
    [BoxGroup("Summon")] public GameObject EnemyToSpawn;

    //[BoxGroup("Defend")] public float DefendTime = 2.5f;
    [BoxGroup("Defend")] public float ShieldRespawnTimer = 10.0f;
    [BoxGroup("Defend")] public float ShieldDurationTimer = 2.5f;

    [BoxGroup("Death")] [Required] public GameObject DeathParticles;
    [BoxGroup("Death")] [Required] public GameObject FogParticles;
    [BoxGroup("Death")] [MinValue(0f)] public float FogTiming = 2f;
    [BoxGroup("Death")] [MinValue(0f)] public float OverlayTiming = 2f;

    [BoxGroup("Aggro")] public FightGate[] Gates;

    [Required] public Transform Beam;

    private Status status_;

    public Transform MusicTriggerArea;


    #region Sound and Music Variables
    /**
    * What sounds do we need for core enemy
    * - Attacking (how does it sound?) (swiping/slashing sound? smaller roar?)
    * - Summoning (how does it sound?) (? roaring?)
    * - Defending (how does it sound?) (constricting noise?)
    * - Taking Damage (how does it sound?) (2 different sounds, 1 for taking damage normally and one for when defending (player have responsibility for latter sound))
    *   (Squishy, like cutting through watermelon or cucumber?) (clinking hard noise? metal on metal or metal on stone?)
    * - Idle      (how does it sound?) (Ambient omnious? slow heartbeat? liquid pumping?)  
    * - Death     (how does it sound?) (roaring???)
    ***/
    [BoxGroup("Sound Events")] [EventRef] public string AttackSound;
    [BoxGroup("Sound Events")] [EventRef] public string SummonSound;
    [Tooltip("This is the sound for when spawning the shield")]
    [BoxGroup("Sound Events")] [EventRef] public string DefendSound;
    [BoxGroup("Sound Events")] [EventRef] public string IdleSound;
    [BoxGroup("Sound Events")] [EventRef] public string DeathSound;
    [BoxGroup("Sound Events")] [EventRef] public string DamagedSound;
    [Tooltip("This is the sound for when defending and taking no damage")]
    [BoxGroup("Sound Events")] [EventRef] public string DefendingSound;

    // HACK: Should probably not be here
    [BoxGroup("Sound Events")] [EventRef] public string DefeatMusic;
#endregion

    #region Transform Variables
    // below we're gonna define all the child objects that we need for feedback
    private Transform inAttackEffect_;
    private Transform inHurtParticles_;
    private Transform inShield_;
    private Transform inCoreFragments_;
    private Transform inCoreRoots_;
    private Transform refPlayer_;
    private Transform inSummonParticles_;
    private Transform inCoreCollision_;
    private Transform[] inShieldChildren_; // TEMP TODO REMOVE
    #endregion

    private ParticleSystem inShieldParticleSystem_; // TEMP TODO REMOVE

    #region Animators
    private Animator inAttackAnimator_;
    private Animator inCoreAnimator_;
    private Animator inRootsAnimator_;
    private Animator inShieldAnimator_;
#endregion

    private List<GameObject> enemyPool_;
    private List<EnemyAI> ownedEnemies_; 
    private List<CorruptedObject> ownedEnvironment_;
    
    private int summonCounter_; // Keeping track of how many units the core has summoned
    private Vector3 summonPosition_; // used as spawning position
    private Vector3 attackPosition_;

    private bool hasDied;

    [Task]
    private bool SummonDone { get { return summonCounter_ >= SummonAmount; } }
    [Task] private bool CanSummon { get; set; }
    [Task] private bool TookDamage { get; set; }
    private CharacterHealth health_;
    private int halfHealth_;
    [Task]
    private bool ShieldSummoned { get; set; }
    private bool CanSummonShield { get; set; }
    private bool AttackActive { get; set; }

    #region Timer variables
    private Timer shieldRespawnTimer_;
    private Timer shieldDurationTimer_;
    private Timer attackDurationTimer_;
#endregion

    #region Initialization
    private void InitInternalHiearchy()
    {
        inAttackEffect_ = transform.Find("core_attack");
        inAttackEffect_.GetComponent<HitBox>().Damage = AttackStrength;
        inAttackAnimator_ = inAttackEffect_.GetComponent<Animator>();
        
        inHurtParticles_ = transform.Find("core_hurt_particles");

        inCoreFragments_ = transform.Find("core_core");
        inCoreRoots_ = transform.Find("core_roots");
        inRootsAnimator_ = inCoreRoots_.GetComponent<Animator>();
        inRootsAnimator_.SetBool("IsAlive", true);

        refPlayer_ = GameObject.FindGameObjectWithTag("Player").transform;

        inSummonParticles_ = transform.Find("core_summon_particles");

        inCoreCollision_ = transform.Find("core_collision");


        inShield_ = transform.Find("core_shield");
        inShieldChildren_ = new Transform[inShield_.childCount-1];
        for (var i = 0; i < inShield_.childCount-1; i++)
        {
            inShieldChildren_[i] = inShield_.GetChild(i);
        }
        inShieldAnimator_ = inShield_.GetComponent<Animator>();


        inShieldParticleSystem_ = inShield_.Find("CoreShield_particles").GetComponentInChildren<ParticleSystem>();

        inCoreAnimator_ = GetComponent<Animator>();
        inCoreAnimator_.SetBool("IsAlive", true);
    }

    private void FindOwnedObjects()
    {
        ownedEnemies_ = new List<EnemyAI>();
        ownedEnvironment_ = new List<CorruptedObject>();

        //var area = transform.Find("core_area").GetComponent<SphereCollider>();
        var aggroRange = ActiveRange; //area.radius * Mathf.Max(Mathf.Max(area.transform.lossyScale.x, area.transform.lossyScale.y), area.transform.lossyScale.z);
        var hits = Physics.OverlapSphere(transform.position + ActiveRangeOffset, aggroRange);
        foreach (var hit in hits)
        {
            var ai = hit.transform.GetComponent<EnemyAI>();
            var env = hit.transform.GetComponent<CorruptedObject>();
            if (ai && ai != this)
            {
                ownedEnemies_.Add(ai);
            }
            else if (env)
            {
                ownedEnvironment_.Add(env);
                env.Corrupt();
            }
        }
    }

    private void PoolEnemies()
    {
        enemyPool_ = new List<GameObject>(SummonAmount);

        for (var i = 0; i < SummonAmount; i++)
        {
            enemyPool_.Add(GameObject.Instantiate(EnemyToSpawn));
            enemyPool_[i].transform.SetParent(transform);
            enemyPool_[i].SetActive(false);
        }
    }

    private void SetupVariables()
    {
        health_ = GetComponent<CharacterHealth>();
        health_.OnDamaged += Damage;

        halfHealth_ = health_.MaxHealth / 2;

        shieldRespawnTimer_ = new Timer(ShieldRespawnTimer);
        shieldDurationTimer_ = new Timer(ShieldDurationTimer);
        attackDurationTimer_ = new Timer(5.0f); // max interval between attacks

        summonPosition_ = transform.position;
        attackPosition_ = transform.position;

        CanSummonShield = true;
    }

    private bool LoadData()
    {
        // if name is empty or RESPAWN then dont load data no matter what
        if (CoreAreaName.Equals("") || CoreAreaName.Equals("RESPAWN")) return false;

        if (!EnemyCoreHandler.Instance.IsCoreDead(this)) return false;
        SelfDestruct();
        return true;
    }
    #endregion

    #region Unity Methods
    private void Start ()
	{
        InitInternalHiearchy();
        FindOwnedObjects();
	    SetupVariables();
        if (!LoadData())
            PoolEnemies();

        Deactivate();
    }

    private void Update()
    {
        // should shield have a timer or health?
        // if it has health then we need to set that up as well

        UpdateShieldTimers();

        IsActive = PlayerInsideActiveRange();
        if (!IsPlayerClose)
        {
            IsPlayerClose = PlayerInsideAggroRange();
            if (IsPlayerClose)
            {
                StartAggroSequence();
            }
        }

        if (IsPlayerClose)
        {
            Beam.gameObject.SetActive(false);
        }

        UpdateStatus(IsActive);
    }


    private void OnDestroy()
    {
        if (ownedEnvironment_ == null) return;

        foreach (var corruptedObject in ownedEnvironment_)
        {
            if (corruptedObject && !corruptedObject.Destroying)
            {
                corruptedObject.gameObject.SetActive(true);
            }
        }
    }


    private void StartAggroSequence()
    {
        foreach (var fightGate in Gates)
        {
            fightGate.OpenGate();
        }
    }


    private bool PlayerInsideAggroRange()
    {
        var layerMask = LayerMask.GetMask("Player");
        var hits = Physics.OverlapSphere(transform.position, AggroRange, layerMask);
        return hits.Length > 0;
    }

    private bool PlayerInsideActiveRange()
    {
        var layerMask = LayerMask.GetMask("Player");
        var hits = Physics.OverlapSphere(transform.position, ActiveRange, layerMask);
        return hits.Length > 0;
    }
    #endregion

    private void SelfDestruct()
    {
        MusicTriggerArea.GetComponent<CorruptedSkyChanger>().RevertToNormalSky();
        // defeat and remove the core
        // defeat means that we add it to progression system, but not enemy core handler
        // we also need to cleanse the area and remove all owned enemies in the area
        ProgressTracker.Instance.UpdateProgress(this); // update progress difficulty

        //foreach (var corruptedObject in ownedEnvironment_)
        //{
        //    corruptedObject.Cleanse();
        //}

        //foreach (var obj in ownedEnemies_)
        //{
        //    Destroy(obj.gameObject);
        //}
        //Destroy(gameObject);

        // this ought to work
        DespawnOwnedEnemies();
        CleanseOwnedObjects();
    }

    #region DEBUGGING PURPOSES
    private void OnDrawGizmos()
    {
        // visualize attack position
        Gizmos.color = new Color(Color.black.r, Color.black.g, Color.black.b, 0.50f);
        Gizmos.DrawSphere(attackPosition_, 0.75f);

        Debug.DrawLine(transform.position, attackPosition_, Color.black);

        // visualize summoning position
        Gizmos.color = new Color(Color.green.r, Color.green.g, Color.green.b, 0.50f);
        Gizmos.DrawSphere(summonPosition_, 0.75f);

        Debug.DrawLine(transform.position, summonPosition_, Color.green);

        // visualize player position
        if (refPlayer_)
        {
            Gizmos.color = new Color(Color.cyan.r, Color.cyan.g, Color.cyan.b, 0.50f);
            Gizmos.DrawSphere(refPlayer_.position, 0.75f);
        }

        Gizmos.color = new Color(1, 0, 0, 0.3f);
        Gizmos.DrawSphere(transform.position, AggroRange);

        Gizmos.color = new Color(0, 1, 0, 0.3f);
        Gizmos.DrawSphere(transform.position + ActiveRangeOffset, ActiveRange);
    }
    #endregion

    #region Delegate Implementations
    private void Damage(ref int amount, AttackType attackType, Vector3 hitPosition)
    {
        if (ShieldSummoned)
        {
            // also play correct sound here
            amount = 0;
            PlaySoundEffect(SoundType.Defending);
            return;
        }
        
        inHurtParticles_.GetComponent<ParticleSystem>().Play(true);
        TookDamage = true;
        //inCoreAnimator_.SetBool("TookDamage", TookDamage);
        inCoreAnimator_.SetTrigger("TookDamage");
        PlaySoundEffect(SoundType.Damaged);
        //StartCoroutine(CoreDamageShatter());
        // determine when we can summon
        if (health_.CurrentHealth <= halfHealth_)
            CanSummon = true;
        if (health_.CurrentHealth - amount <= 0)
        {
            inCoreAnimator_.SetBool("IsAlive", false);
            inRootsAnimator_.SetBool("IsAlive", false);
        }
    }

    private IEnumerator CoreDamageShatter()
    {
        var rbs = inCoreFragments_.GetComponentsInChildren<Rigidbody>();
        foreach (var rb in rbs)
        {
            rb.isKinematic = false;
        }
        yield return new WaitForSeconds(0.05f); // this will break the core apart
        // maybe a cool effect would be to break it apart and then pull it back together again real fast?
        // the pull apart would be dependant on how much damage it recieved?

        // BUG this part conflicts with core being defeated
        foreach (var rb in rbs)
        {
            rb.isKinematic = true;
        }
    }

    #endregion

    #region Tasks

    #region Die and Destroy
    [Task]
    private void DefeatCore()
    {
        var task = Task.current;

        if (hasDied)
        {
            task.Fail();
            return;
        }

        hasDied = true;

        EnemyCoreHandler.Instance.KillCore(this); // add the core to defeated cores
        ProgressTracker.Instance.UpdateProgress(this); // update progress difficulty
        var rbs = inCoreFragments_.GetComponentsInChildren<Rigidbody>();
        foreach (var rb in rbs)
        {
            rb.isKinematic = false;
            rb.AddExplosionForce(0.5f, rb.position, 0.1f, 0.5f, ForceMode.Impulse);
        }
        // disable being able to hit core
        GetComponent<CapsuleCollider>().enabled = false;

        // disable collision
        inCoreCollision_.gameObject.SetActive(false);

        PlaySoundEffect(SoundType.Death);
        MusicManager.Instance.PlayMusic(DefeatMusic);

        // need to trigger the cleanse stuff here too
        StartCoroutine(DeathEffects());

        task.Succeed();
    }


    private IEnumerator DeathEffects()
    {
        Instantiate(DeathParticles, transform.position, transform.rotation);
        //foreach (var particles in DeathParticles.GetComponentsInChildren<ParticleSystem>())
        //{
        //    particles.Play();
        //}

        yield return new WaitForSeconds(FogTiming);
        

        Instantiate(FogParticles, transform.position, transform.rotation);
        //foreach (var particles in FogParticles.GetComponentsInChildren<ParticleSystem>())
        //{
        //    particles.Play();
        //}

        yield return new WaitForSeconds(OverlayTiming);
        MusicTriggerArea.GetComponent<CorruptedSkyChanger>().RevertToNormalSky();
        var screenFade = FindObjectOfType<OverlayFade>();
        var fadeTime = 0f;
        if (screenFade)
        {
            screenFade.StartFade();
            fadeTime = screenFade.FadeInTime;
        }

        yield return new WaitForSeconds(fadeTime);
        
        // dont do this unless screen is completely white
        if (EnemyCoreHandler.Instance.IsLastCore())
        {
            yield return new WaitForSeconds(screenFade.WaitTime);
            SceneManager.LoadScene("Credits");
        }

        DespawnOwnedEnemies();
        CleanseOwnedObjects();
    }


    [Task]
    public void DespawnEnemies()
    {
        var task = Task.current;
        DespawnOwnedEnemies();
        task.Succeed();
    }


    private void DespawnOwnedEnemies()
    {
        if (ownedEnemies_ != null)
        {
            foreach (var obj in ownedEnemies_)
            {
                if (obj)
                    Destroy(obj.gameObject);
            }
        }
        if (enemyPool_ != null)
        {
            foreach (var obj in enemyPool_)
            {
                if (obj)
                    Destroy(obj);
            }
        }
        if (MusicTriggerArea.gameObject)
            Destroy(MusicTriggerArea.gameObject);
        gameObject.SetActive(false);
    }


    [Task]
    public void CleanseArea()
    {
        var task = Task.current;
        CleanseOwnedObjects();
        task.Succeed();
    }


    private void CleanseOwnedObjects()
    {
        foreach (var corruptedObject in ownedEnvironment_)
        {
            corruptedObject.Cleanse();
        }
    }

    #endregion
    
    #region Defend
    [Task]
    private void CheckShieldStatus()
    {
        // if we can summon shield then we succeed, otherwise we fail and we cant do any of the defend states
        var task = Task.current;
        // reset the Took damage variable (this will always happen after we have taken damage)
        TookDamage = false;
        //inCoreAnimator_.SetBool("TookDamage", TookDamage);

        // if we can summon the shield and we dont have an active shield, then we can proceed
        task.Complete(CanSummonShield && !ShieldSummoned);
    }

    [Task]
    private void SpawnShield()
    {
        var task = Task.current;
        if (task.isStarting)
        {
            // here we can do the fancy stuff when spawning the shield
            // we're gonna have the shield active all the time and just play animations instead
            //inShield_.gameObject.SetActive(true);
            //SetShield(true);

            ShieldSummoned = true;
            CanSummonShield = false;

            var distToPlayer = (transform.position - refPlayer_.position).magnitude;
            Debug.Log(distToPlayer);
            if (distToPlayer < 4.0f) // I dunno
                refPlayer_.GetComponent<Knockback>().Knock(10, transform.position);
        }

        task.Succeed();
    }

    [Task]
    private void PlayDefendAnimation()
    {
        var task = Task.current;
        inShieldParticleSystem_.Play(true);
        inShieldAnimator_.SetBool("Shielding", true);
        task.Succeed();
    }

    [Task]
    private void PlayDefendSound()
    {
        var task = Task.current;
        PlaySoundEffect(SoundType.Defend);
        task.Succeed();
    }

    private void UpdateShieldTimers()
    {
        // if we cant summon a shield then update the respawn of the timer
        if (!CanSummonShield)
            shieldRespawnTimer_.Update(Time.deltaTime);
        // if we have summoned a shield then update how long it's active
        if (ShieldSummoned)
            shieldDurationTimer_.Update(Time.deltaTime);

        if (shieldDurationTimer_.IsDone)
        {
            // if the shield duration is done then set shield summoned to false and disable the shield, also reset the timer
            ShieldSummoned = false;
            // do fancy despawn here possibly
            //inShield_.gameObject.SetActive(false);
            //SetShield(false);
            inShieldParticleSystem_.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            shieldDurationTimer_.Reset();
            inShieldAnimator_.SetBool("Shielding", false);
        }

        // if respawn timer for the shield is done then we set that we can summon a new shield and reset the timer
        if (shieldRespawnTimer_.IsDone)
        {
            CanSummonShield = true;
            shieldRespawnTimer_.Reset();
        }
    }

    private void SetShield(bool active)
    {
        foreach (var t in inShieldChildren_)
        {
            t.gameObject.SetActive(active);
        }
    }
    #endregion
    
    #region Summon
    [Task]
    private void GetSummonPosition()
    {
        var task = Task.current;
        summonPosition_ = GetRandomGroundPosition(SummonMaxRange, transform);
        if (summonPosition_.Equals(Vector3.zero))
            task.Fail();
        else
        {
            task.Succeed();
        }
    }

    [Task]
    private void DoSummonAnimation()
    {
        var task = Task.current;
        inSummonParticles_.position = summonPosition_ + new Vector3(0, 1, 0); // NICE HACK
        inSummonParticles_.GetComponent<ParticleSystem>().Play(true);
        PlaySoundEffect(SoundType.Summon);

        task.Succeed();
    }

    [Task]
    private void SummonEnemy()
    {
        var task = Task.current;

        enemyPool_[summonCounter_].transform.position = summonPosition_;
        enemyPool_[summonCounter_].gameObject.SetActive(true);
        summonCounter_++;

        task.Succeed();
    }
    #endregion

    #region Attack
    [Task]
    private void GetAttackPosition()
    {
        var task = Task.current;
        if (task.isStarting)
        {
            
        }

        attackPosition_ = GetPlayerGroundPosition();
        if (attackPosition_.Equals(Vector3.zero))
            task.Fail();
        else
        {
            inAttackEffect_.transform.position = attackPosition_;
            task.Succeed();
        }
    }

    [Task]
    private void CanAttack()
    {
        var task = Task.current;

        if (!AttackActive)
        {
            task.Succeed();
        }
        else
        {
            task.Fail();
        }
    }

    [Task]
    private void SpawnAttack()
    {
        // we dont want to do this unless the attack is free to use
        var task = Task.current;

        // enable particles
        //inAttackEffect_.GetComponent<ParticleSystem>().Play(true);
        inAttackEffect_.GetComponentInChildren<ParticleSystem>().Play(true); // possibly?
        inAttackAnimator_.SetTrigger("SpawnAttack");
        
        task.Succeed();
    }

    [Task]
    private void EnableAttackHitbox()
    {
        var task = Task.current;
        inAttackEffect_.GetComponent<HitBox>().Activate();
        AttackActive = true;

        // might have to lower chance of this playing
        PlaySoundEffect(SoundType.Attack);

        task.Succeed();
    }

    [Task]
    private void DespawnAttack()
    {
        var task = Task.current;

        // turn off particles
        //inAttackEffect_.GetComponent<ParticleSystem>().Stop(true, ParticleSystemStopBehavior.StopEmitting);
        inAttackEffect_.GetComponentInChildren<ParticleSystem>().Stop(true, ParticleSystemStopBehavior.StopEmitting);
        inAttackAnimator_.SetTrigger("DespawnAttack"); // BUG there's a bug with setting trigger here
        AttackActive = false;

        task.Succeed();
    }

    [Task]
    private void DisableAttackHitbox()
    {
        var task = Task.current;

        // turn off hitbox
        inAttackEffect_.GetComponent<HitBox>().Deactivate();
        inAttackAnimator_.SetTrigger("Reset");

        task.Succeed();
    }

    #endregion

    #region Idle
    [Task]
    private void Idle()
    {
        var task = Task.current;
        if (task.isStarting)
        {
            // entry to task
            // setup idle sounds and animations here
            // we want to randomize when we should play animations or sounds
            var value = UnityEngine.Random.value;
            if (value < 0.1f) // 10% chance every frame (possibly stupid)
                PlaySoundEffect(SoundType.Idle);
        }

        // always succeed idle task
        // making it loop basically
        task.Succeed();
    }
    #endregion

    #endregion

    private Vector3 GetRandomGroundPosition(float range, Transform target)
    {
        // get random position within a sphere
        var circle = UnityEngine.Random.insideUnitSphere;
        circle *= range;
        circle += target.position;

        // move the position to way up high
        circle.y += 1000.0f;

        // try to clamp to ground via raycasts

        // the layers we DONT want to hit
        // this is gonna collide with the camera trigger, which is a problem
        var layerMask = 1 << LayerMask.NameToLayer("Enemy") | 1 << LayerMask.NameToLayer("Ignore Raycast") | 1 << LayerMask.NameToLayer("Enemy Attack");
        // invert the layermask
        layerMask = ~layerMask;
        RaycastHit hit;
        if (Physics.Raycast(circle, Vector3.down, out hit, Mathf.Infinity, layerMask))
        {
            var y = hit.point.y;
            circle.y = y;
            return circle;
        }

        return Vector3.zero;
    }

    private Vector3 GetPlayerGroundPosition()
    {
        var position = refPlayer_.position;
        // need to clamp this to ground
        RaycastHit hit;
        if (Physics.Raycast(position, Vector3.down, out hit, Mathf.Infinity))
        {
            position.y = hit.point.y - 0.15f; // magic value to make sure the model is in the ground completely
            return position;
        }
        return Vector3.zero;
    }

    public override void Activate()
    {
        base.Activate();
        GetComponent<PandaBehaviour>().enabled = true;

        GetComponent<Rigidbody>().isKinematic = false;

        GetComponent<CapsuleCollider>().enabled = true;

        //foreach (var s in enemyPool_)
        //{
        //    s.GetComponent<EnemyAI>().Activate();
        //}

        status_ = Status.Active;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        GetComponent<PandaBehaviour>().enabled = false;

        GetComponent<Rigidbody>().isKinematic = true;

        GetComponent<CapsuleCollider>().enabled = false;

        if (enemyPool_ != null)
        { 
            foreach (var s in enemyPool_)
            {
                s.GetComponent<EnemyAI>().Deactivate();
            }
        }

        status_ = Status.Deactive;
    }

    private void UpdateStatus(bool active)
    {
        if (active && status_ == Status.Deactive)
            Activate();
        if (!active && status_ == Status.Active)
            Deactivate();
    }

    #region Sound Implementation
    private enum SoundType
    {
        Attack,
        Defend,
        Summon,
        Idle,
        Death,
        Damaged,
        Defending
    };

    private void PlaySoundEffect(SoundType type)
    {
        switch (type)
        {
            case SoundType.Attack:
                RuntimeManager.PlayOneShot(AttackSound, transform.position);
                break;

            case SoundType.Defend:
                RuntimeManager.PlayOneShot(DefendSound, transform.position);
                break;

            case SoundType.Summon:
                RuntimeManager.PlayOneShot(SummonSound, transform.position);
                break;

            case SoundType.Idle:
                RuntimeManager.PlayOneShot(IdleSound, transform.position);
                break;

            case SoundType.Death:
                RuntimeManager.PlayOneShot(DeathSound, transform.position);
                break;

            case SoundType.Damaged:
                RuntimeManager.PlayOneShot(DamagedSound, transform.position);
                break;

            case SoundType.Defending:
                RuntimeManager.PlayOneShot(DefendingSound, transform.position);
                break;
        }
    }
#endregion
}
