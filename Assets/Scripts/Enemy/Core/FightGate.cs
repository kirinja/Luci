﻿using UnityEngine;


[RequireComponent(typeof(Animator))]
public class FightGate : MonoBehaviour
{
    public void OpenGate()
    {
        foreach (var coll in GetComponents<Collider>())
        {
            coll.enabled = true;
        }
        GetComponent<Animator>().enabled = true;
    }
}
