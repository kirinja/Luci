﻿using NaughtyAttributes;
using UnityEngine;

public class CoreBeam : MonoBehaviour
{
    [Required] public Transform Beam;


    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        Beam.gameObject.SetActive(false);
    }
}