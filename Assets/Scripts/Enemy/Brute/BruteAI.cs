﻿using System;
using Panda;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.AI;

public class BruteAI : EnemyAI
{
    // doesnt move, have to look over
    [BoxGroup("Attack")] public HitBox ScytheHitbox;
    private Vector3 targetPosition_;
    private Vector3 patrolPosition_;
    private Animator animator_;

    private Transform player_;
    private CharacterHealth health_;

    [Task]
    public bool Patrolling { get; set; }

    private bool attacking_;
    private bool canDespawn_;

    private NavMeshAgent agent_;
    private NavMeshPath path_;
    
    private Status status_;

    #region Feedback systems
    private Transform particleHurt_;
#endregion

    #region Sounds
    [BoxGroup("Sound Events")] [EventRef] public string AttackSound;
    [BoxGroup("Sound Events")] [EventRef] public string PatrolSound;
    [BoxGroup("Sound Events")] [EventRef] public string IdleSound;
    [BoxGroup("Sound Events")] [EventRef] public string DeathSound;
    [BoxGroup("Sound Events")] [EventRef] public string TakeDamageSound;
    #endregion

    // Use this for initialization
    void Start ()
    {
        player_ = GameObject.FindGameObjectWithTag("Player").transform;
        animator_ = GetComponent<Animator>();

        particleHurt_ = transform.Find("part_EnemyTakeDamage");
        Patrolling = true;

        health_ = GetComponent<CharacterHealth>();
        health_.OnDamaged += OnDamaged;

        ScytheHitbox.Damage = AttackStrength;

        agent_ = GetComponent<NavMeshAgent>();
        //agent_.speed = MovementSpeed;
        //path_ = new NavMeshPath();

        //StartCoroutine(InitAgent());
        Deactivate();
    }

    IEnumerator InitAgent()
    {
        agent_ = GetComponent<NavMeshAgent>();
        agent_.speed = MovementSpeed;
        agent_.enabled = false;
        path_ = new NavMeshPath();

        yield return new WaitForSeconds(0.1f);

        agent_.enabled = true;

        NavMeshHit closestHit;

        if (NavMesh.SamplePosition(gameObject.transform.position, out closestHit, 500f, NavMesh.AllAreas))
            gameObject.transform.position = closestHit.position;
        else
            Debug.LogError("Could not find position on NavMesh!");

        yield return null;
    }

    // Update is called once per frame
    void Update ()
    {
        IsActive = PlayerInsideActiveRange();
        IsPlayerClose = PlayerInsideAggroRange();

        UpdateStatus(IsActive);
    }
    private bool PlayerInsideAggroRange()
    {
        var layerMask = LayerMask.GetMask("Player");
        var hits = Physics.OverlapSphere(transform.position, AggroRange, layerMask);
        return hits.Length > 0;
    }

    private bool PlayerInsideActiveRange()
    {
        var layerMask = LayerMask.GetMask("Player");
        var hits = Physics.OverlapSphere(transform.position, ActiveRange, layerMask);
        return hits.Length > 0;
    }


    #region DEBUGGING PURPOSES
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(Color.yellow.r, Color.yellow.g, Color.yellow.b, 0.50f);
        Gizmos.DrawSphere(targetPosition_, 0.75f);

        Debug.DrawLine(transform.position, targetPosition_, Color.yellow);

        Gizmos.color = new Color(1, 0, 0, 0.3f);
        Gizmos.DrawSphere(transform.position, AggroRange);

        Gizmos.color = new Color(0, 1, 0, 0.3f);
        Gizmos.DrawSphere(transform.position + ActiveRangeOffset, ActiveRange);
    }
    #endregion

    // test
    [Task]
    private void AttackMove()
    {
        var task = Task.current;
        if (task.isStarting)
        {
            //agent_.SetDestination()
        }

        RotateTowards(player_.position);
        float t = (transform.position - targetPosition_).magnitude;
        
        if (Task.isInspected)
            task.debugInfo = string.Format("d={0:0.000}", t);

        animator_.SetFloat("VelY", agent_.velocity.normalized.magnitude);
        //if (Vector3.Distance(transform.position, targetPosition_) <= agent_.stoppingDistance)
        if (t <= 1.1f)
        {
            task.Succeed();
            animator_.SetFloat("VelY", 0);
            task.debugInfo = "d=0.000";
        }
    }

    private void OnDamaged(ref int amount, AttackType attackType, Vector3 hitPosition)
    {
        // play feedback and sound
        var ps = particleHurt_.GetComponentsInChildren<ParticleSystem>();
        foreach (var p in ps)
        {
            p.Play(true);
        }
        PlaySoundEffect(SoundType.Damaged);
    }

    private IEnumerator Despawn()
    {
        //var rb = GetComponent<Rigidbody>();
        //if (rb)
        //{
        //    rb.isKinematic = true;
        //    rb.useGravity = false;
        //}
        agent_.enabled = false;
        var col = GetComponent<Collider>();
        if (col)
        {
            col.enabled = false;
        }

        // hardcoded value
        yield return new WaitForSeconds(10.0f);

        Destroy(gameObject);
    }

    #region Tasks
    #region Death

    [Task]
    private void DisableBrute()
    {
        var task = Task.current;
        // disable all the hitboxes etc
        ScytheHitbox.Deactivate();
        attacking_ = false;
        task.Succeed();
    }

    [Task]
    private void PlayDeathAnimation()
    {
        var task = Task.current;
        animator_.SetTrigger("Death");
        PlaySoundEffect(SoundType.Death);
        StartCoroutine(Despawn());

        task.Succeed();
    }

    [Task]
    private void RemoveBrute()
    {
        var task = Task.current;

        task.Succeed();
    }

    [Task]
    void SinkIntoGround()
    {
        // have to wait for animation
        if (canDespawn_)
            transform.position += Vector3.down * Time.deltaTime * 1.0f;

        //Task.current.Complete();
    }
    #endregion

    #region Attack

    [Task]
    private void CanAttack()
    {
        var task = Task.current;

        task.Succeed();
    }

    [Task]
    private void FacePlayer()
    {
        var task = Task.current;

        RotateTowards(player_.position);
        
        var dir = player_.position - transform.position;
        if (Vector3.Dot(dir, transform.forward) > 0.0f)
        {
            task.Succeed();
        }
    }

    [Task]
    private void SetAttackingTarget()
    {
        var task = Task.current;
        targetPosition_ = GetPlayerTarget();

        NavMeshHit hit2;
        //NavMesh.Raycast(point, point, out navHit)
        // this doesnt work properly
        if (NavMesh.Raycast(transform.position, targetPosition_ + new Vector3(0, 1f, 0), out hit2, new NavMeshQueryFilter()))
            task.Fail();

        else
        {

            agent_.CalculatePath(targetPosition_, path_);
            // only succeed if the route is ok

            // it can set the destination to an area that is not navmeshed
            // we need to make sure it can only set targetPosition inside navmesh areas
            if (path_.status == NavMeshPathStatus.PathComplete)
            {
                //agent_.SetDestination(targetPosition_);
                agent_.SetPath(path_);
                task.Succeed();
            }
            else
            {
                task.Fail();
            }
        }
        
        //task.Succeed();
    }

    [Task]
    private void CheckValidPath()
    {
        var task = Task.current;

        NavMeshHit hit;
        //NavMesh.Raycast(point, point, out navHit)
        // this doesnt work properly
        //if (NavMesh.Raycast(transform.position, targetPosition_, out hit2, new NavMeshQueryFilter()))
        //    task.Fail();

        // this seems to work for staying on the navmesh
        if (!NavMesh.SamplePosition(targetPosition_, out hit, 0.5f, NavMesh.AllAreas))
        {
            task.Fail();
        }

        else
        {
            if (agent_)
            {
                agent_.CalculatePath(targetPosition_, path_);
                // only succeed if the route is ok

                // it can set the destination to an area that is not navmeshed
                // we need to make sure it can only set targetPosition inside navmesh areas
                //Debug.Log(path_.status);
                if (path_.status == NavMeshPathStatus.PathComplete)
                {
                    //agent_.SetDestination(targetPosition_);
                    agent_.SetPath(path_);
                    task.Succeed();
                }
                else
                {
                    task.Fail();
                }
            }
            else
            {
                task.Fail();
            }

        }
    }

    [Task]
    private void StartAttackAnimation()
    {
        var task = Task.current;
        attacking_ = true;
        animator_.SetTrigger("Attack");
        task.Succeed();
    }

    [Task]
    private void WaitForAttackAnimation()
    {
        var task = Task.current;

        if (!attacking_ || !health_.IsAlive)
        {
            task.Succeed();
        }
        // only succeed when the animation is over
    }
    #endregion

    #region Movement
    [Task]
    private void MoveToTarget()
    {
        var task = Task.current;
        if (task.isStarting)
        {
            if (Patrolling)
                PlaySoundEffect(SoundType.Patrol);
        }

        Vector3 destination = targetPosition_;
        Vector3 delta = (destination - transform.position);
        Vector3 velocity = (MovementSpeed) * delta.normalized;

        RotateTowards(destination);

        transform.position = transform.position + velocity * Time.deltaTime;

        Vector3 newDelta = (destination - transform.position);
        float d = newDelta.magnitude;

        animator_.SetFloat("VelY", d);

        if (Task.isInspected)
            task.debugInfo = string.Format("d={0:0.000}", d);

        if (Vector3.Dot(delta, newDelta) <= 0.0f || d < 1e-3)
        {
            transform.position = destination;
            task.Succeed();
            d = 0.0f;
            task.debugInfo = "d=0.000";
            animator_.SetFloat("VelY", 0);
        }
    }

    [Task]
    private void Move()
    {
        var task = Task.current;
        if (task.isStarting)
        {
            if (Patrolling)
                PlaySoundEffect(SoundType.Patrol);
        }

        //RotateTowards(player_.position);
        float t = (transform.position - targetPosition_).magnitude;
        // determine if moving away or towards player
        //var dot = Vector3.Dot(targetPosition_, transform.rotation.eulerAngles);
        //Debug.Log(dot);
        // need to check current position, target position and our rotation

        animator_.SetFloat("VelY", agent_.velocity.normalized.magnitude);
        //if (Vector3.Distance(transform.position, targetPosition_) <= agent_.stoppingDistance)
        if (t <= 1.1f)
        {
            task.Succeed();
            animator_.SetFloat("VelY", 0);
        }
    }
    #endregion

    #region Patrol

    [Task]
    private void GetPatrolTarget()
    {
        var task = Task.current;

        // get random position inside sphere
        targetPosition_ = RandomPosition(10); // hardcoded value
        //Debug.Log(targetPosition_);
        if (targetPosition_.Equals(Vector3.zero))
            task.Fail();

        NavMeshHit hit2;
        //NavMesh.Raycast(point, point, out navHit)
        // this doesnt work properly
        if (NavMesh.Raycast(transform.position, targetPosition_ + new Vector3(0, 1f, 0), out hit2, new NavMeshQueryFilter()))
            task.Fail();

        else
        {

            agent_.CalculatePath(targetPosition_, path_);
            // only succeed if the route is ok

            // it can set the destination to an area that is not navmeshed
            // we need to make sure it can only set targetPosition inside navmesh areas
            if (path_.status == NavMeshPathStatus.PathComplete)
            {
                //agent_.SetDestination(targetPosition_);
                agent_.SetPath(path_);
                task.Succeed();
            }
            else
            {
                task.Fail();
            }
        }
    }
#endregion
    #endregion

    void RotateTowards(Vector3 target)
    {
        var lookPos = target - transform.position;
        lookPos.y = 0;
        if (Mathf.Abs(lookPos.magnitude) < 0.01) return;

        var rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5f);
    }

    private Vector3 RandomPosition(float range)
    {
        // get random position within a sphere
        var circle = UnityEngine.Random.insideUnitSphere;
        circle *= range;
        circle += transform.position;

        // move the position to way up high
        circle.y += 5.0f;

        // try to clamp to ground via raycasts

        // the layers we DONT want to hit
        var layerMask = 1 << LayerMask.NameToLayer("Enemy") | 1 << LayerMask.NameToLayer("Ignore Raycast") | 1 << LayerMask.NameToLayer("Enemy Attack");
        // invert the layermask
        layerMask = ~layerMask;
        RaycastHit hit;
        if (Physics.Raycast(circle + new Vector3(0, 1f, 0), Vector3.down, out hit, Mathf.Infinity, layerMask))
        {
            var y = hit.point.y;
            circle.y = y;
            return circle;
        }

        return Vector3.zero;
    }

    private Vector3 GetPlayerTarget()
    {
        var target =
            player_.position; // target position have to take into account player direction and try to stand a little bit away from player
        var direction = player_.position - transform.position;
        direction.Normalize();
        target -= (direction * (4)); // might work
        var layerMask = 1 << LayerMask.NameToLayer("Enemy") | 1 << LayerMask.NameToLayer("Ignore Raycast") |
                        1 << LayerMask.NameToLayer("Enemy Attack");
        // invert the layermask
        layerMask = ~layerMask;
        RaycastHit hit;
        if (Physics.Raycast(target + new Vector3(0, 1f, 0), Vector3.down, out hit, Mathf.Infinity, layerMask))
        {
            var y = hit.point.y;
            target.y = y;
        }
        return target;
    }

    // these can only run once, not every frame
    public override void Activate()
    {
        base.Activate();
        StartCoroutine(InitAgent());
        GetComponent<NavMeshAgent>().enabled = true;

        GetComponent<CharacterController>().enabled = true;

        GetComponent<PandaBehaviour>().enabled = true;

        status_ = Status.Active;
    }

    public override void Deactivate()
    {
        base.Deactivate();

        // dont disable the navmesh agent if it has been activated once seems to fix some issues
        //GetComponent<NavMeshAgent>().enabled = false;

        GetComponent<CharacterController>().enabled = false;

        GetComponent<PandaBehaviour>().enabled = false;

        status_ = Status.Deactive;
    }

    private void UpdateStatus(bool active)
    {
        if (active && status_ == Status.Deactive)
            Activate();
        if (!active && status_ == Status.Active)
            Deactivate();
    }

    #region Animation Events

    public void EnableAttackHitbox()
    {
        ScytheHitbox.Activate();
        PlaySoundEffect(SoundType.Attack);
    }

    public void DisableAttackHitbox()
    {
        ScytheHitbox.Deactivate();
    }

    public void AttackIsDone()
    {
        attacking_ = false;
    }

    void CanDespawn()
    {
        canDespawn_ = true;
    }
    #endregion

    #region Sound Methods
    enum SoundType { Attack, Patrol, Idle, Death, Damaged }

    private void PlaySoundEffect(SoundType type)
    {
        switch (type)
        {
            case SoundType.Attack:
                RuntimeManager.PlayOneShot(AttackSound, transform.position);
                break;

            case SoundType.Patrol:
                RuntimeManager.PlayOneShot(PatrolSound, transform.position);
                break;

            case SoundType.Idle:
                RuntimeManager.PlayOneShot(IdleSound, transform.position);
                break;

            case SoundType.Death:
                RuntimeManager.PlayOneShot(DeathSound, transform.position);
                break;

            case SoundType.Damaged:
                RuntimeManager.PlayOneShot(TakeDamageSound, transform.position);
                break;
        }
    }
    #endregion
}
