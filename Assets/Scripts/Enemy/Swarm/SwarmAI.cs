﻿using System;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using NaughtyAttributes;
using Panda;
using UnityEngine;
using UnityEngine.AI;

public class SwarmAI : EnemyAI {

    // swarm doesnt move, probably doesnt set a targe properly to move to

	// Use this for initialization
    [Task] [HideInInspector]
    /*[BoxGroup("Patrol")]*/ public bool Patrolling;
    [BoxGroup("Movement")] public float PatrolSpeed;
    [BoxGroup("Movement")] public float PatrolRange = 5.0f;
    private Vector3 target_;
    
    private CharacterHealth health_;
    private HitBox attackHitbox_;
    private bool canDespawn_;

    private NavMeshPath path_;
    private NavMeshAgent agent_;
    private Transform player_;
    private SwarmController swarmController_;
    
    private Status status_;

    #region Sounds
    [BoxGroup("Sound Events")] [EventRef] public string AttackSound;
    [BoxGroup("Sound Events")] [EventRef] public string PatrolSound;
    [BoxGroup("Sound Events")] [EventRef] public string IdleSound;
    [BoxGroup("Sound Events")] [EventRef] public string DeathSound;
    [BoxGroup("Sound Events")] [EventRef] public string TakeDamageSound;
#endregion

    private Animator animator_;
    #region Transforms
    private Transform particleHurt_;
    #endregion

    #region Initialization Methods

    private void InitVariables()
    {
        health_ = GetComponent<CharacterHealth>();
        attackHitbox_ = transform.Find("Attack").GetComponent<HitBox>();
        attackHitbox_.Damage = AttackStrength;
        

        attackHitbox_.Continuous = false;
        //attackHitbox_.Activate();

        // get animator
        animator_ = GetComponent<Animator>();

        // delegates
        health_.OnDamaged += Damaged;

        target_ = transform.position;
        particleHurt_ = transform.Find("part_EnemyTakeDamage");

        player_ = GameObject.FindGameObjectWithTag("Player").transform;
        swarmController_ = gameObject.GetComponentInParent<SwarmController>();
        agent_ = GetComponent<NavMeshAgent>();

        Deactivate();
    }
    #endregion

    #region Unity Methods
    private void Start ()
    {
        InitVariables();

        //StartCoroutine(InitAgent());
    }

    IEnumerator InitAgent()
    {
        agent_.enabled = false;
        path_ = new NavMeshPath();

        yield return new WaitForSeconds(0.1f);

        agent_.enabled = true;

        //NavMeshHit closestHit;

        //if (NavMesh.SamplePosition(gameObject.transform.position, out closestHit, 500f, NavMesh.AllAreas))
        //    gameObject.transform.position = closestHit.position;
        //else
        //    Debug.LogError("Could not find position on NavMesh!");
        

        yield return null;
    }

    #region DEBUGGING PURPOSES
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(Color.magenta.r, Color.magenta.g, Color.magenta.b, 0.50f);
        Gizmos.DrawSphere(target_, 0.75f);

        Debug.DrawLine(transform.position, target_, Color.magenta);


        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawWireSphere(transform.position, AggroRange);

        Gizmos.color = new Color(0, 1, 0, 0.5f);
        Gizmos.DrawWireSphere(transform.position, ActiveRange);
    }
    #endregion
#endregion

    #region Delegate Implementations

    private void Damaged(ref int amount, AttackType attackType, Vector3 hitPosition)
    {
        var ps = particleHurt_.GetComponentsInChildren<ParticleSystem>();
        foreach (var p in ps)
        {
            p.Play(true);
        }
        PlaySoundEffect(SoundType.Damaged);
    }
#endregion
    
    private IEnumerator Despawn()
    {
        //var rb = GetComponent<Rigidbody>();
        //if (rb)
        //{
        //    rb.isKinematic = true;
        //    rb.useGravity = false;
        //}

        var col = GetComponent<Collider>();
        if (col)
        {
            col.enabled = false;
        }

        // hardcoded value
        yield return new WaitForSeconds(10.0f);
        
        Destroy(gameObject);
    }

    #region Tasks
    #region Death

    [Task]
    private void Kill()
    {
        var task = Task.current;
        if (task.isStarting)
        {
            PlaySoundEffect(SoundType.Death);
            //animator_.SetBool("PlayerInSight", false);
            animator_.SetTrigger("Death");
            attackHitbox_.Deactivate();
            agent_.enabled = false;
            SignalDeath();
            StartCoroutine(Despawn());
        }

        task.Succeed();
    }

    [Task]
    void SinkIntoGround()
    {
        // have to wait for animation
        if (canDespawn_)
            transform.position += Vector3.down * Time.deltaTime * 1.0f;

        //Task.current.Complete();
    }
    #endregion

    /// <summary>
    /// Animation Event. Called when the death animation is over
    /// </summary>
    void CanDespawn()
    {
        canDespawn_ = true;
    }

    void DisableAttack()
    {
        attackHitbox_.Deactivate();
    }

    #region Attack

    [Task]
    private void PerformAttack()
    {
        var task = Task.current;
        // TODO need slight random delay on these 
        animator_.SetTrigger("Attack");
        attackHitbox_.Activate();

        task.Succeed();
    }

    [Task]
    private void PlayAttackSound()
    {
        var task = Task.current;
        // TODO implement
        PlaySoundEffect(SoundType.Attack);
        task.Succeed();
    }
    #endregion

    #region Movement
    [Task]
    private void CheckValidPath()
    {
        var task = Task.current;

        NavMeshHit hit;
        //NavMesh.Raycast(point, point, out navHit)
        // this doesnt work properly
        //if (NavMesh.Raycast(transform.position, targetPosition_, out hit2, new NavMeshQueryFilter()))
        //    task.Fail();

        // this seems to work for staying on the navmesh
        if (!NavMesh.SamplePosition(target_, out hit, 0.5f, NavMesh.AllAreas))
        {
            task.Fail();
        }

        else
        {
            agent_.CalculatePath(target_, path_);
            // only succeed if the route is ok

            // it can set the destination to an area that is not navmeshed
            // we need to make sure it can only set targetPosition inside navmesh areas
            //Debug.Log(path_.status);
            if (path_.status == NavMeshPathStatus.PathComplete)
            {
                //agent_.SetDestination(targetPosition_);
                agent_.SetPath(path_);
                task.Succeed();
            }
            else
            {
                task.Fail();
            }
        }
    }

    [Task]
    private void Move()
    {
        var task = Task.current;
        if (task.isStarting)
        {
            if (Patrolling)
            {
                PlaySoundEffect(SoundType.Patrol);
                agent_.speed = PatrolSpeed;
            }
            else
            {
                agent_.speed = MovementSpeed;
            }


        }
        if (!Patrolling)
            RotateTowards(player_.position);
        float t = (transform.position - target_).magnitude;
        agent_.SetDestination(target_);
        animator_.SetFloat("Blend", agent_.velocity.normalized.magnitude);

        var d = agent_.remainingDistance;

        if (Task.isInspected)
            task.debugInfo = string.Format("d={0:0.000}", d);

        //if (Vector3.Distance(transform.position, targetPosition_) <= agent_.stoppingDistance)
        if (t <= 1.1f)
        {
            task.Succeed();
            animator_.SetFloat("Blend", 0);
            task.debugInfo = "d=0.000";
        }
    }

    [Task]
    private void GetRandomPosition()
    {
        var task = Task.current;
        target_ = RandomPosition(PatrolRange);

        //Debug.Log(target_);

        if (target_.Equals(Vector3.zero))
            task.Fail();
        else
        {
            if (agent_)
            {
                agent_.CalculatePath(target_, path_);
                // only succeed if the route is ok

                // it can set the destination to an area that is not navmeshed
                // we need to make sure it can only set targetPosition inside navmesh areas
                if (path_.status == NavMeshPathStatus.PathComplete)
                {
                    agent_.SetPath(path_);
                    task.Succeed();
                }
                else
                {
                    task.Fail();
                }
            }
            else
            {
                task.Fail();
            }
        }
        //task.Succeed();
    }

    [Task]
    private void ValidateTargetPosition()
    {
        var task = Task.current;

        // have to return depending on wether the position is ok or not
        var valid = ValidatePosition(target_);

        task.Complete(valid);
        //task.Succeed();
    }

    [Task]
    private void MoveToPosition()
    {
        var task = Task.current;
        if (task.isStarting)
        {
            // init
            // TODO Call patrol sounds if we're patrolling, otherwise call attack sounds
            if (Patrolling)
                PlaySoundEffect(SoundType.Patrol);

            //PlaySoundEffect(Patrolling ? SoundType.Patrol : SoundType.Attack);

            // set animation variables?
            // moving so we're not punching
            //animator_.SetBool("PlayerInSight", false); // gonna change this to a trigger possible
        }

        // only succeed when we are at the position or if player enters aggro range
        //task.Succeed();

        // need to rotate towards direction

        Vector3 destination = target_;
        Vector3 delta = (destination - transform.position);
        Vector3 velocity = (Patrolling ? PatrolSpeed : MovementSpeed) * delta.normalized;

        RotateTowards(destination);

        transform.position = transform.position + velocity * Time.deltaTime;

        Vector3 newDelta = (destination - transform.position);
        float d = newDelta.magnitude;
        // only apply this when we arent close enough
        animator_.SetFloat("Blend", d);
        //Debug.LogWarning(velocity.magnitude);
        if (Task.isInspected)
            Task.current.debugInfo = string.Format("d={0:0.000}", d);
        
        if (Vector3.Dot(delta, newDelta) <= 0.0f || d < 1e-3)
        {
            transform.position = destination;
            Task.current.Succeed();
            d = 0.0f;
            Task.current.debugInfo = "d=0.000";
            animator_.SetFloat("Blend", 0);
            //animator_.SetBool("PlayerInSight", IsPlayerClose);

            Patrolling = false;
        }

        //task.Succeed();
    }

    public void SetTarget(Vector3 newTarget)
    {
        target_ = newTarget;
    }
    #endregion

    #region Patrol

    [Task]
    private void PlayPatrolAnimation()
    {
        var task = Task.current;
        // TODO implement
        task.Succeed();
    }

    [Task]
    private void PlayPatrolSound()
    {
        var task = Task.current;
        // TODO implement
        task.Succeed();
    }
#endregion

    #region Idle

    [Task]
    private void PlayIdleAnimation()
    {
        var task = Task.current;
        // TODO implement
        task.Succeed();
    }

    [Task]
    private void PlayIdleSound()
    {
        var task = Task.current;
        // TODO implement
        // randomize how often we can play these
        var val = UnityEngine.Random.value;
        if (val <= 0.1f)
            PlaySoundEffect(SoundType.Idle);
        task.Succeed();
    }
#endregion
    #endregion

    private Vector3 RandomPosition(float range)
    {
        // get random position within a sphere
        var circle = UnityEngine.Random.insideUnitSphere;
        circle *= range;
        circle += transform.parent.position; // HACK dangerous (assuming every swarm has a parent)

        // move the position to way up high
        circle.y += 5.0f;

        // try to clamp to ground via raycasts

        // the layers we DONT want to hit
        // this is gonna collide with the camera trigger, which is a problem
        var layerMask = 1 << LayerMask.NameToLayer("Enemy") | 1 << LayerMask.NameToLayer("Ignore Raycast") | 1 << LayerMask.NameToLayer("Enemy Attack");
        // invert the layermask
        layerMask = ~layerMask;
        RaycastHit hit;
        if (Physics.Raycast(circle, Vector3.down, out hit, Mathf.Infinity, layerMask))
        {
            var y = hit.point.y;
            circle.y = y;
            return circle;
        }

        return Vector3.zero;
    }

    private bool ValidatePosition(Vector3 position)
    {
        // we're gonna spherecast from the input position to see if there is actually something we can move to (might have to change this to pathfinding later so it can move better?)


        var layerMask = 1 << LayerMask.NameToLayer("Enemy") | 1 << LayerMask.NameToLayer("Ignore Raycast") | 1 << LayerMask.NameToLayer("Enemy Attack");
        // invert the layermask
        layerMask = ~layerMask;
        //RaycastHit hit;

        var hits = Physics.OverlapSphere(position, 1, layerMask, QueryTriggerInteraction.Ignore);

        // if we hit anything then we're gonna say it's good
        return hits.Length > 0;
    }

    void RotateTowards(Vector3 target)
    {
        var lookPos = target - transform.position;
        lookPos.y = 0;
        if (Math.Abs(lookPos.magnitude) < 0.01) return;

        var rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5f);
    }

    void SignalDeath()
    {
        swarmController_.RemoveDeadSwarm(this);
    }

    public override void Activate()
    {
        base.Activate();
        //GetComponent<NavMeshAgent>().enabled = true;
        agent_.enabled = true;
        StartCoroutine(InitAgent());

        GetComponent<CharacterController>().enabled = true;

        GetComponent<PandaBehaviour>().enabled = true;

        status_ = Status.Active;
    }

    public override void Deactivate()
    {
        base.Deactivate();

        //GetComponent<NavMeshAgent>().enabled = false;
        //if (agent_)
        //    agent_.enabled = false;

        GetComponent<CharacterController>().enabled = false;

        GetComponent<PandaBehaviour>().enabled = false;

        status_ = Status.Deactive;
    }

    private void UpdateStatus(bool active)
    {
        if (active && status_ == Status.Deactive)
            Activate();
        if (!active && status_ == Status.Active)
            Deactivate();
    }

    #region Sound Methods
    enum SoundType { Attack, Patrol, Idle, Death, Damaged}

    private void PlaySoundEffect(SoundType type)
    {
        switch (type)
        {
            case SoundType.Attack:
                RuntimeManager.PlayOneShot(AttackSound, transform.position);
                break;

            case SoundType.Patrol:
                RuntimeManager.PlayOneShot(PatrolSound, transform.position);
                break;

            case SoundType.Idle:
                RuntimeManager.PlayOneShot(IdleSound, transform.position);
                break;

            case SoundType.Death:
                RuntimeManager.PlayOneShot(DeathSound, transform.position);
                break;

            case SoundType.Damaged:
                RuntimeManager.PlayOneShot(TakeDamageSound, transform.position);
                break;
        }
    }
#endregion
}
