﻿using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using Panda;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// class that handles a bunch of swarm enemies
/// keeps track on when player is near the area and where each swarm should set its movement target
/// </summary>
public class SwarmController : EnemyAI
{
    private List<SwarmAI> swarms_; // we probably should object pool instead, that way if we change the prefab then we dont have to reassign every child
    [BoxGroup("Swarm Variables")] public int AmountInHorde;
    [BoxGroup("Swarm Variables")] public GameObject SwarmEnemy;

    [Task]
    private bool ShouldPatrol { get; set; }

    [BoxGroup("Swarm Variables")] public float SwarmPatrolChance = 50.0f;
    [BoxGroup("Swarm Variables")] public float SwarmIdleChance = 75.0f;
    [BoxGroup("Swarm Variables")] public float StateSwitchChange = 25.0f;

    private Transform player_;

    private NavMeshPath path_;
    private NavMeshAgent agent_;

    [Task] private bool IsDead = false;
    // we can relay the information in the tasks

    private Status status_;

    private void InitializeSwarms()
    {
        InitObjectPool();

        Deactivate();
    }

    private void InitObjectPool()
    {
        var swarmsObject = transform.Find("swarm_controller_swarms");
        swarms_ = new List<SwarmAI>(AmountInHorde);
        for (int i = 0; i < AmountInHorde; i++)
        {
            var randPosition = UnityEngine.Random.insideUnitCircle;
            var go = Instantiate(SwarmEnemy);
            go.transform.SetParent(swarmsObject);
            go.transform.position = transform.position + (new Vector3(randPosition.x, 0, randPosition.y) * 5);
            var ai = go.GetComponent<SwarmAI>();
            swarms_.Add(ai);
        }
    }

    [Task]
    private void UpdateActiveStatus()
    {
        var task = Task.current;
        foreach (var s in swarms_)
            s.IsActive = IsActive;

        task.Complete(!IsActive);
    }

    [Task]
    private void CheckValidPath()
    {
        var task = Task.current;

        NavMeshHit hit;
        //NavMesh.Raycast(point, point, out navHit)
        // this doesnt work properly
        //if (NavMesh.Raycast(transform.position, targetPosition_, out hit2, new NavMeshQueryFilter()))
        //    task.Fail();

        // this seems to work for staying on the navmesh
        if (!NavMesh.SamplePosition(PlayerGroundPosition(), out hit, 0.5f, NavMesh.AllAreas))
        {
            task.Fail();
        }

        else
        {
            if (agent_)
            {
                agent_.CalculatePath(PlayerGroundPosition(), path_);
                // only succeed if the route is ok

                // it can set the destination to an area that is not navmeshed
                // we need to make sure it can only set targetPosition inside navmesh areas
                //Debug.Log(path_.status);
                if (path_.status == NavMeshPathStatus.PathComplete)
                {
                    //agent_.SetDestination(targetPosition_);
                    //agent_.SetPath(path_);
                    task.Succeed();
                }
                else
                {
                    task.Fail();
                }
            }
            else
            {
                task.Fail();
            }
        }
    }

    #region Unity Methods
    // Use this for initialization
    void Awake ()
    {
        InitializeSwarms();
    }

    void Start()
    {
        player_ = GameObject.FindGameObjectWithTag("Player").transform;
        // only need one reference to an agent
        agent_ = swarms_[0].GetComponent<NavMeshAgent>(); // this is dangerous, we need to get first alive target and updated accordingly
        // when a target dies it needs to signal the controller so it can remove it from the list
        path_ = new NavMeshPath();
    }

    void Update()
    {
        IsActive = PlayerInsideActiveRange();
        IsPlayerClose = PlayerInsideAggroRange();
        
        UpdateStatus(IsActive);
    }

    #region DEBUGGING PURPOSES
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.3f);
        Gizmos.DrawSphere(transform.position, AggroRange);

        Gizmos.color = new Color(0, 1, 0, 0.3f);
        Gizmos.DrawSphere(transform.position + ActiveRangeOffset, ActiveRange);
    }
    #endregion

    private bool PlayerInsideAggroRange()
    {
        var layerMask = LayerMask.GetMask("Player");
        var hits = Physics.OverlapSphere(transform.position, AggroRange, layerMask);
        return hits.Length > 0;
    }

    private bool PlayerInsideActiveRange()
    {
        var layerMask = LayerMask.GetMask("Player");
        var hits = Physics.OverlapSphere(transform.position, ActiveRange, layerMask);
        return hits.Length > 0;
    }
    #endregion
    #region Tasks

    #region Death

    [Task]
    private void RemoveSelf()
    {
        Destroy(gameObject);
    }
    #endregion

    #region Attack
    [Task]
    private void SetAttacking()
    {
        var task = Task.current;
        UpdatePlayerProximity();
        task.Succeed();
    }

    [Task]
    private void DeclareTargets()
    {
        var task = Task.current;

        var attackPositions = GetAttackPositions(AmountInHorde);

        // assign targets to each of the swarm enemies in the pack
        // have to calculate the positions around the player and assign them randomly to swarms
        for (var i = 0; i < swarms_.Count; i++)
        {
            var pos = attackPositions[i];
            var s = swarms_[i];
            s.SetTarget(pos);
            // should rotate the thing towards the player?
        }

        task.Succeed();
    }
    #endregion

    #region Patrol

    [Task]
    private void SetPatrolling()
    {
        // this could be randomized per swarm, so they dont all act exactly the same
        var task = Task.current;
        UpdatePlayerProximity();
        UpdateSwarmPatrol();
        task.Succeed();
    }
    #endregion

    #region Idle

    [Task]
    private void SetIdle()
    {
        // this could be randomized per swarm, so they dont all act exactly the same
        var task = Task.current;
        UpdatePlayerProximity();
        UpdateSwarmIdle();
        task.Succeed();
    }
    #endregion
#endregion

    private void UpdatePlayerProximity()
    {
        foreach (var s in swarms_)
        {
            s.IsPlayerClose = IsPlayerClose;
        }
    }

    private void UpdateSwarmIdle()
    {
        // randomly determine, per swarm, if they should idle or not
        // only have to do this for patrol since it determine if they are patrolling or not
        foreach (var s in swarms_)
        {
            var value = UnityEngine.Random.value;
            if (value >= SwarmIdleChance / 100.0f)
            {
                s.Patrolling = false;
            }
        }
    }

    private void UpdateSwarmPatrol()
    {
        // randomly determine, per swarm, if they should patrol or not
        foreach (var s in swarms_)
        {
            var value = UnityEngine.Random.value;
            if (value >= SwarmPatrolChance / 100.0f)
            {
                s.Patrolling = true;
            }
        }
    }

    #region Helper Tasks
    [Task]
    private void StopPatrolling()
    {
        var task = Task.current;
        var value = UnityEngine.Random.value;
        if (value < StateSwitchChange / 100.0f)
            ShouldPatrol = false;
        task.Succeed();
    }
    [Task]
    private void StartPatrolling()
    {
        var task = Task.current;
        var value = UnityEngine.Random.value;
        if (value < StateSwitchChange / 100.0f)
            ShouldPatrol = true;
        task.Succeed();
    }
    #endregion

    private Vector3[] GetAttackPositions(int amount)
    {
        var vectors = new Vector3[amount];

        var playerPos = PlayerGroundPosition();
        //if (Math.Abs(playerPos.magnitude) < 0.01f)
        if ( playerPos.Equals(Vector3.zero))
        {
            playerPos = player_.position; // use air position in worst case
        }

        // calculate x amount of positions for swarm, surrounding player
        for (int i = 0; i < amount; i++)
        {
            // calculate with i and set angle
            var angle = 70.0f;
            var stargingAngle = player_.eulerAngles.y - ((amount * angle) / 2);
            var finalAngle = (angle * i) + stargingAngle;
            

            var rotation = Quaternion.Euler(0, finalAngle, 0);
            var pos = rotation * transform.forward;
            pos *= 0.5f; // Magic value which is the range
            vectors[i] = pos + playerPos;
        }

        return vectors;
    }

    private Vector3 PlayerGroundPosition()
    {
        var position = player_.position;
        // need to clamp this to ground
        RaycastHit hit;
        if (Physics.Raycast(position, Vector3.down, out hit, Mathf.Infinity))
        {
            position.y = hit.point.y;
            return position;
        }
        return Vector3.zero;
    }

    public void RemoveDeadSwarm(SwarmAI swarmAi)
    {
        swarms_.Remove(swarmAi);
        if (swarms_.Count > 0)
            agent_ = swarms_[0].GetComponent<NavMeshAgent>();
        else
        {
            agent_ = null;
            IsDead = true;
        }
    }

    public override void Deactivate()
    {
        base.Deactivate();

        foreach (var s in swarms_)
        {
            s.Deactivate();
        }
        GetComponent<PandaBehaviour>().enabled = false;
        status_ = Status.Deactive;
    }

    public override void Activate()
    {
        base.Activate();

        foreach (var s in swarms_)
        {
            s.Activate();
        }
        GetComponent<PandaBehaviour>().enabled = true;
        status_ = Status.Active;
    }

    private void UpdateStatus(bool active)
    {
        if (active && status_ == Status.Deactive)
            Activate();
        if (!active && status_ == Status.Active)
            Deactivate();
    }
}
