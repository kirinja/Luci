﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerArea : MonoBehaviour
{
    private EnemyAI ai_;

    // Use this for initialization
    private void Awake ()
    {
        ai_ = GetComponentInParent<EnemyAI>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ai_.IsPlayerClose = true;
            other.GetComponent<PlayerCombat>().AddEnemy(ai_);
        }

    }


    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ai_.IsPlayerClose = false;
            other.GetComponent<PlayerCombat>().RemoveEnemy(ai_);
        }
    }

}
