﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveData
{
    [SerializeField]
    public Checkpoint Checkpoint; // can be null

    [SerializeField] public Vector3 Checkpoint2; // if 0 then discard

    [SerializeField] public List<string> DefeatedCores; // save names of the cores
    
    public string[] Collectibles;

    public int NumberOfCollectibles;

    public int VisageMeter;

}