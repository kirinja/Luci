﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

/// <summary>
/// Should have inparameters to Save/Load, sending in a Checkpoint and a Player?
/// </summary>
public class IO : MonoBehaviour
{
    // the two values we're interested in are current HP and which abilities the player have
    private string saveDirectory_;

    private string savePath_;

    private static SaveData data_;

    private static IO instance_;

    // need to keep track of the player, since we're changing values on it;
    //private CharacterHealth health_;
    //private Controller3D player_;
    // need to keep track of active checkpoint?
    // need to keep track of player visage meter

    public static IO Instance
    {
        get
        {
            // find any IO in the scene
            if (instance_ == null)
            {
                instance_ = FindObjectOfType<IO>();
            }

            // if we couldnt find one then create a new object with it
            if (instance_ == null)
            {
                GameObject go = new GameObject("IO Singleton");
                go.AddComponent<IO>();
                go.hideFlags = HideFlags.HideInHierarchy;
                instance_ = go.GetComponent<IO>();
            }
            return instance_;
        }
    }


    private bool HasMemoryData
    {
        get { return data_ != null; }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);

        saveDirectory_ = Path.Combine(Application.dataPath, SaveDirectory);

        if (!Directory.Exists(saveDirectory_))
            Directory.CreateDirectory(saveDirectory_);

        savePath_ = Path.Combine(saveDirectory_, SaveFile);
    }

    public void NewGame()
    {
        DeleteSaveFile();
    }

    public void SaveGame(Respawner playerRespawner)
    {
        var data = CreateSaveData(playerRespawner);
        SaveToMemory(data);
        SaveToFile(data);
    }

    private SaveData CreateSaveData(Respawner playerRespawner)
    {
        var saveData = new SaveData
        {
            Checkpoint = playerRespawner != null ? playerRespawner.Checkpoint : null,
            Checkpoint2 = playerRespawner != null ? playerRespawner.Checkpoint != null ?  playerRespawner.Checkpoint.SpawnPoint : Vector3.zero : Vector3.zero,
            VisageMeter = playerRespawner != null ? playerRespawner.GetComponent<Visage>().Gauge : 0,
            DefeatedCores = EnemyCoreHandler.Instance.DefeatedCores(),
            Collectibles =  playerRespawner != null ? playerRespawner.GetComponent<CollectibleHolder>().FoundCollectibles : null,
            NumberOfCollectibles = playerRespawner!= null ? playerRespawner.GetComponent<CollectibleHolder>().NumberOfCollectibles : 0
        };

        return saveData;
    }

    private void SaveToMemory(SaveData data)
    {
        data_ = data;
    }

    private void SaveToFile(SaveData data)
    {
        var stringifiedPlayer = JsonUtility.ToJson(data);
        File.WriteAllText(savePath_, stringifiedPlayer);

        //var encrypted = EncryptString(stringifiedPlayer, KeyPhrase);
        //File.WriteAllText(savePath_, encrypted);

        GC.Collect();
    }


    public SaveData LoadGame()
    {
        if (!HasMemoryData && HasFileData())
        {
            data_ = LoadFromFile();
        }

        return data_;
    }

    private SaveData LoadFromFile()
    {
        var stringifiedData = File.ReadAllLines(savePath_);
        var saveData = JsonUtility.FromJson<SaveData>(stringifiedData[0]);

        // Check if anything in save file is corrupt
        if (IsDataCorrupt(saveData)) saveData = null;

        GC.Collect();

        return saveData;
    }

    private static bool IsDataCorrupt(SaveData data)
    {
        if (/*data.Checkpoint == null ||*/ data.DefeatedCores == null) return true;
        foreach (var core in data.DefeatedCores)
        {
            if (core == null) return true;
        }

        return false;
    }

    private bool HasFileData()
    {
        return File.Exists(savePath_);
    }


    public void DeleteSaveFile()
    {
        if (File.Exists(savePath_))
            File.Delete(savePath_);

        if (HasMemoryData)
            data_ = null;
    }


    //private void OnEnable()
    //{
    //    SceneManager.sceneLoaded += OnLevelFinishedLoading;
    //}


    //private void OnDisable()
    //{
    //    SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    //}


    //private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    //{

    //    //QualitySettings.vSyncCount = Vsync ? 1 : 0;
    //    //Application.targetFrameRate = FPS;

    //    //var go = GameObject.FindGameObjectWithTag("Player");
    //    //if (!go) return;

    //    // this might not work correctly with multiscenes? might be that every time a new scenes is added then it loads from memory again
    //    if (!LoadFromMemory())
    //        LoadFromFiles();

    //}

    #region Constants

    private const string SaveFile = "Save.sav";
    private const string SaveDirectory = "Save";
    private const float RegularTimeScale = 1f;

    // This size of the IV (in bytes) must = (KeySize / 8).  Default KeySize is 256, so the IV must be
    // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
    private const string InitVector = "pemgail9uzpgzl88";

    // This constant is used to determine the KeySize of the encryption algorithm
    private const int KeySize = 256;

    // storing the encrypt/descrypt key inside the actual program is dangerous since you can read the memory and find the key
    // it doesnt matter in this case however since all we want to do is change the output file from english to gibberish (so the player cant easily edit it)
    private const string KeyPhrase = "Broken Dreams";

    #endregion

    #region Encryption

    // the following encrypt and decrypt code is taken from http://tekeye.biz/2015/encrypt-decrypt-c-sharp-string
    // Encrypt
    public static string EncryptString(string plainText, string passPhrase)
    {
        byte[] initVectorBytes = Encoding.UTF8.GetBytes(InitVector);
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
        byte[] keyBytes = password.GetBytes(KeySize / 8);
        RijndaelManaged symmetricKey = new RijndaelManaged();
        symmetricKey.Mode = CipherMode.CBC;
        ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
        MemoryStream memoryStream = new MemoryStream();
        CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
        cryptoStream.FlushFinalBlock();
        byte[] cipherTextBytes = memoryStream.ToArray();
        memoryStream.Close();
        cryptoStream.Close();
        return Convert.ToBase64String(cipherTextBytes);
    }

    // Decrypt
    public static string DecryptString(string cipherText, string passPhrase)
    {
        byte[] initVectorBytes = Encoding.UTF8.GetBytes(InitVector);
        byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
        PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
        byte[] keyBytes = password.GetBytes(KeySize / 8);
        RijndaelManaged symmetricKey = new RijndaelManaged();
        symmetricKey.Mode = CipherMode.CBC;
        ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
        MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
        CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
        byte[] plainTextBytes = new byte[cipherTextBytes.Length];
        int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
        memoryStream.Close();
        cryptoStream.Close();
        return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
    }

    #endregion
}