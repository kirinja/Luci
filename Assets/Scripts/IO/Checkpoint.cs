﻿using System;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using NaughtyAttributes;
using UnityEngine;
using UnityEngineInternal.Input;

[Serializable]
public class Checkpoint : MonoBehaviour
{
    private bool Active { get; set; }
    [BoxGroup("Sound Events")] [EventRef] public string ActivateSound;
    [BoxGroup("Sound Events")] [EventRef] public string SaveSound;

    private Vector3 spawnPoint_;
    // we need to set our player position to this value at the respawn method
    public Vector3 SpawnPoint
    {
        get { return spawnPoint_ + GetOffset; }
    }

    public float SpawnOffset;

    private Vector3 GetOffset
    {
        get
        {
            var offset = UnityEngine.Random.insideUnitCircle;
            offset *= SpawnOffset;

            return new Vector3(offset.x, 0, offset.y);
        }
    }
    
	// Use this for initialization
	void Awake ()
	{
	    spawnPoint_ = transform.position;
	}

    public void Activate(Respawner checkpoint)
    {
        // disable previously held checkpoint (if there is one)
        if (checkpoint.Checkpoint)
            checkpoint.Checkpoint.Disable();
        Active = true;

        // fire all the partile systems and enable eyes
        // set this checkpoint as active checkpoint on player

        var particleParent = gameObject.transform.Find("ParticleSystem");
        var systems = particleParent.GetComponentsInChildren<ParticleSystem>();
        //var start_smoke = particleParent.Find("Start_Smoke");
        //var start_symbols = particleParent.Find("Start_Symbols");
        RuntimeManager.PlayOneShot(ActivateSound, transform.position);
        RuntimeManager.PlayOneShot(SaveSound, transform.position);
        foreach (var p in systems)
        {
            var emission = p.emission;
            emission.enabled = true;
            p.Play(true);
        }
        //start_smoke.gameObject.SetActive(true);
        //start_symbols.gameObject.SetActive(true);

        checkpoint.Checkpoint = this;
    }

    private void Disable()
    {
        Active = false;
        // disable all particle effect and disable eyes?
        // also make sure this checkpoint is not linked to the player (although player will probably keep track of that)

        var particleParent = gameObject.transform.Find("ParticleSystem");
        var systems = particleParent.GetComponentsInChildren<ParticleSystem>();
        foreach (var p in systems)
        {
            var emission = p.emission;// = true;
            emission.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") )
        {
            // run animations
            if (!Active)
            {
                var otherCheckpoint = other.GetComponent<Respawner>();
                if (!otherCheckpoint)
                    Debug.LogError("No attached active checkpoint component on Player");
                else
                    Activate(otherCheckpoint);
            }
            // save
            Save(other.GetComponent<Respawner>());
        }
    }

    private void Save(Respawner playerRespawner)
    {
        IO.Instance.SaveGame(playerRespawner);
    }
}
