﻿using UnityEngine;

public class Hitstop : MonoBehaviour
{
    public MonoBehaviour[] ComponentsToDisable;
    public Rigidbody[] RigidbodiesToDisable;
    public Animator[] AnimatorsToDisable;


    private Timer stopTimer_;


    // Use this for initialization
    private void Start()
    {
        stopTimer_ = new Timer(1f, true);
    }


    // Update is called once per frame
    private void Update()
    {
        if (Pause.Instance.Paused) return;

        var deltaTime = Time.unscaledDeltaTime;

        if (stopTimer_.Update(deltaTime))
        {
            SetStopped(false);
        }
    }


    // Use this to apply hitstop
    public void Stop(float time)
    {
        if (time <= 0f) return;
        SetStopped(true);

        stopTimer_ = new Timer(time);
    }


    private void SetStopped(bool stopped)
    {
        foreach (var component in ComponentsToDisable)
        {
            component.enabled = !stopped;
        }
        foreach (var component in RigidbodiesToDisable)
        {
            component.isKinematic = stopped;
        }
        foreach (var component in AnimatorsToDisable)
        {
            component.enabled = !stopped;
        }
    }


    public bool IsStopped
    {
        get { return !stopTimer_.IsDone; }
    }
}