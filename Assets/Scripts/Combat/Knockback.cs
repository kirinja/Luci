﻿using UnityEngine;

[RequireComponent(typeof(Controller3D))]
[RequireComponent(typeof(CharacterHealth))]
public class Knockback : MonoBehaviour
{
    public float VelocityMultiplier = 1f;
    public float MaxVelocity = 40f;


    private Controller3D controller3D_;

    
	// Use this for initialization
    private void Start ()
	{
	    GetComponent<CharacterHealth>().OnDamaged += OnDamaged;
	    controller3D_ = GetComponent<Controller3D>();
	}


    private void OnDamaged(ref int amount, AttackType attackType, Vector3 hitPosition)
    {
        if (attackType == AttackType.Laser) return;

        Knock(amount, hitPosition);
    }


    public void Knock(int amount, Vector3 hitPosition)
    {
        var direction = (transform.position - hitPosition).normalized;
        direction = new Vector3(direction.x, 0.6f, direction.z).normalized;
        var speed = amount * VelocityMultiplier;
        Debug.Log(speed);
        controller3D_.Velocity = direction * Mathf.Min(amount * VelocityMultiplier, MaxVelocity);
    }
}
