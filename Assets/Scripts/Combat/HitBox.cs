﻿using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class HitBox : MonoBehaviour
{
    public bool Active;

    [MinValue(0)] public int Damage = 1;

    public AttackType Type = AttackType.Light;
    public Hitstop HitstopComponent;
    [MinValue(0f)] public float HitstopTime;


    public bool Continuous;

    [ShowIf("Continuous")] public float RefreshTime = 0.2f;

    [Tooltip("The max amount of targets that can be hit by the attack. 0 or less means infinite.")]
    [ShowIf("NotContinuous")] public int MaxTargets;


    public delegate void Hit(HitBox attacker, CharacterHealth attackee, int damage, Vector3 hitPosition);

    public Hit OnHit = delegate { };


    private bool active_;
    private List<Collider> ignoredColliders_;
    private int targetsHit_;
    private Timer refreshTimer_;


    // Only used by NaughtyAttributes
    private bool NotContinuous()
    {
        return !Continuous;
    }


    private void Awake()
    {
        GetComponent<Collider>().isTrigger = true;
        active_ = false;
        ignoredColliders_ = new List<Collider>();
    }


    private void Start()
    {
        refreshTimer_ = new Timer(RefreshTime);
    }


    private void Update()
    {
        var deltaTime = TimeManager.UnscaledDeltaTime;

        if (active_ && !Active)
        {
            Deactivate();
        }
        else if (!active_ && Active)
        {
            Activate();
        }

        if (active_ && Continuous && refreshTimer_.Update(deltaTime))
        {
            Activate();
            // Equality comparison is fine because there is no rounding
            // ReSharper disable once CompareOfFloatsByEqualityOperator 
            if (refreshTimer_.MaxTime == RefreshTime)
                refreshTimer_.ResetToSurplus();
            else
            {
                refreshTimer_ = new Timer(RefreshTime);
            }
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (!active_ || Pause.Instance.Paused) return;

        Physics.IgnoreCollision(GetComponent<Collider>(), other);
        ignoredColliders_.Add(other);

        var enemyHealth = other.GetComponent<CharacterHealth>();
        if (!enemyHealth) return;

        var hitstop = HitstopTime;
        var damage = Damage;
        var hitPosition = other.ClosestPoint(transform.position);
        if (!enemyHealth.Attack(ref damage, Type,
            hitPosition, ref hitstop)) return;

        if (HitstopComponent)
        {
            HitstopComponent.Stop(hitstop);
        }

        if (damage > 0)
        {
            OnHit(this, enemyHealth, damage, hitPosition);
        }

        if (!Continuous && MaxTargets > 0)
        {
            if (++targetsHit_ >= MaxTargets) Deactivate();
        }
    }


    public void Activate()
    {
        active_ = true;
        Active = active_;
        
        foreach (var ignoredCollider in ignoredColliders_)
        {
            if (ignoredCollider)
                Physics.IgnoreCollision(GetComponent<Collider>(), ignoredCollider, false);
        }
        ignoredColliders_.Clear();
        targetsHit_ = 0;
    }


    public void Deactivate()
    {
        active_ = false;
        Active = active_;
    }
}