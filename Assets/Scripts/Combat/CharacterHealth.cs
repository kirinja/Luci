﻿using NaughtyAttributes;
using Panda;
using UnityEngine;

public class CharacterHealth : MonoBehaviour
{
    public int MaxHealth = 1;


    public int CurrentHealth
    {
        get { return health_; }
    }

    [Task]
    public bool IsAlive { get; private set; }


    // delegate for dealing damage, other classes can subscribe to this and call their own method on top of it
    public delegate void DealDamage(ref int amount, AttackType attackType, Vector3 hitPosition);

    public delegate void ChangedHealth();

    public delegate void Die(CharacterHealth health);


    public ChangedHealth OnHealthChanged;
    public DealDamage OnDamaged;
    public Die OnDeath;


    [ShowNonSerializedField] private int health_;


    private void Awake()
    {
        if (OnDamaged == null)
            OnDamaged = delegate { };
        if (OnHealthChanged == null)
        {
            OnHealthChanged = delegate { };
        }
        if (OnDeath == null)
        {
            OnDeath = delegate { };
        }
    }


    // Use this for initialization
    private void Start()
    {
        health_ = MaxHealth;
        IsAlive = true;
    }


    public bool Attack(ref int damage, AttackType attackType, Vector3 hitPosition, ref float hitstopTime)
    {
        if (!IsAlive)
        {
            hitstopTime = 0f;
            return false;
        }

        OnDamaged(ref damage, attackType, hitPosition);

        if (damage == 0)
        {
            hitstopTime = 0f;
            return true;
        }

        var hitstopComponent = GetComponent<Hitstop>();
        if (hitstopComponent)
        {
            hitstopComponent.Stop(hitstopTime);
        }

        health_ -= Mathf.Min(damage, health_);
        if (health_ <= 0f)
        {
            IsAlive = false;
            OnDeath(this);
        }

        OnHealthChanged();

        return true;
    }


    public void Heal(int healAmount)
    {
        if (!IsAlive) return;

        if (health_ < MaxHealth)
        {
            health_ += Mathf.Min(healAmount, MaxHealth - health_);
            OnHealthChanged();
        }
    }
}