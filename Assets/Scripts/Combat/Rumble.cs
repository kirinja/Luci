﻿using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using XInputDotNetPure;

public class Rumble : MonoBehaviour
{
    public HitBox[] Hitboxes;

    public float HeavyTime = 0.75f;
    public float HeavyForce = 1.0f;
    public float LightTime = 0.4f;
    public float LightForce = 1.0f;
    

	// Use this for initialization
	void Start ()
    {
		//Hitbox.OnHit += OnHit;
        foreach (var h in Hitboxes)
            h.OnHit += OnHit;
	}
    
    private void OnHit(HitBox attacker, CharacterHealth attackee, int damage, Vector3 hitPosition)
    {
        float time, force;
        switch (attacker.Type)
        {
            case AttackType.Heavy:
                time = HeavyTime;
                force = HeavyForce;
                break;
            case AttackType.Light:
                time = LightTime;
                force = LightForce;
                break;
            case AttackType.Laser:
                time = 1.0f;
                force = 2.0f;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        StartCoroutine(RumbleForSeconds(time, force));
    }

    IEnumerator RumbleForSeconds(float seconds, float force)
    {
        GamePad.SetVibration(0, force, force);
        
        yield return new WaitForSecondsRealtime(seconds);

        GamePad.SetVibration(0, 0, 0);
    }

    private void OnDisable()
    {
        GamePad.SetVibration(0, 0, 0);
    }
}
