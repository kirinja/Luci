﻿using UnityEngine;

[RequireComponent(typeof(CharacterHealth))]
public class HealthRegen : MonoBehaviour
{
    public float RegenPerSecondInCombat = 1f;
    public float RegenPerSecondOutOfCombat = 5f;

    private Timer regenTimer_;
    private bool inCombat_;


    // Use this for initialization
    private void Start()
    {
        inCombat_ = false;
        ChangeTimer();
    }


    // Update is called once per frame
    private void Update()
    {
        if (Pause.Instance.Paused) return;

        var deltaTime = Time.unscaledDeltaTime;

        if (regenTimer_.Update(deltaTime))
        {
            var health = GetComponent<CharacterHealth>();
            health.Heal(1);
            regenTimer_.ResetToSurplus();
        }
    }


    private void ChangeTimer()
    {
        regenTimer_ = new Timer(1 / (inCombat_ ? RegenPerSecondInCombat : RegenPerSecondOutOfCombat));
    }


    public bool InCombat
    {
        get { return inCombat_; }
        set
        {
            if (inCombat_ == value) return;
            inCombat_ = value;
            ChangeTimer();
        }
    }
}