﻿using TMPro;
using UnityEngine;


[RequireComponent(typeof(TextMeshProUGUI))]
public class UIPrompt : MonoBehaviour
{
    private Timer timer_;
    private TextMeshProUGUI text_;


    // Use this for initialization
    private void Start()
    {
        timer_ = new Timer(1f, true);
        text_ = GetComponent<TextMeshProUGUI>();
        text_.text = "";
    }


    // Update is called once per frame
    private void Update()
    {
        if (timer_.Update(Time.unscaledDeltaTime))
        {
            text_.text = "";
        }
    }


    public void DisplayText(string text, float timeShown)
    {
        text_.text = text;
        timer_ = new Timer(timeShown);
    }
}