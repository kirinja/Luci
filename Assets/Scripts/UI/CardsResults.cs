﻿using TMPro;
using UnityEngine;


[RequireComponent(typeof(TextMeshProUGUI))]
public class CardsResults : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        var saveData = IO.Instance.LoadGame();
        if (saveData == null) return;

        GetComponent<TextMeshProUGUI>().text = saveData.Collectibles.Length == saveData.NumberOfCollectibles
            ? "Congratulations, you found all " + saveData.NumberOfCollectibles + " bonus cards!"
            : "Bonus cards found: " + saveData.Collectibles.Length + "/" + saveData.NumberOfCollectibles;
    }
}