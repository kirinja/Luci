﻿using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Image))]
public class ImagePrompt : MonoBehaviour
{
    private Timer timer_;
    private Image image_;


    // Use this for initialization
    private void Start()
    {
        timer_ = new Timer(1f, true);
        image_ = GetComponent<Image>();
        image_.enabled = false;
    }


    // Update is called once per frame
    private void Update()
    {
        if (timer_.Update(Time.unscaledDeltaTime))
        {
            image_.enabled = false;
        }
    }


    public void DisplayImage(Sprite sprite, float timeShown)
    {
        image_.sprite = sprite;
        image_.enabled = true;
        timer_ = new Timer(timeShown);
    }
}
