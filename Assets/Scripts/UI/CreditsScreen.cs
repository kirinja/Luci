﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsScreen : MonoBehaviour
{
    public RectTransform NamesObject;
    [Tooltip("Measured in pixels per second")]
    public float ScrollingSpeed = 25.0f;

    public bool StartScrolling { get; set; }

    [Tooltip("Y Value of NamesObject when we move back to Meny")]
    public float StoppingYValue = 450.0f;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    StartScrolling = FindObjectOfType<OverlayFadeOut>().IsDone();
	    var deltaTime = TimeManager.DeltaTime;
	    if (StartScrolling)
	    {
	        NamesObject.anchoredPosition = NamesObject.anchoredPosition + new Vector2(0, ScrollingSpeed * deltaTime);

	        if (Input.anyKeyDown)
	            ReturnToMeny();

            // has to change depending on how much text we add
	        if (NamesObject.anchoredPosition.y >= StoppingYValue)
	            ReturnToMeny();
	    }
	}

    private void ReturnToMeny()
    {
        SceneManager.LoadScene("Meny");
    }
}
