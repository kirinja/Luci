﻿using NaughtyAttributes;
using UnityEngine;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
    [Required]
    public GameObject FirstSelectedObject;


	// Use this for initialization
    private void Start ()
	{
	    Pause.Instance.OnPauseChanged += OnPaused;
        gameObject.SetActive(Pause.Instance.Paused);
	}


    public void OnPaused(bool paused)
    {
        gameObject.SetActive(paused);
        if(paused) RequestFocus();
    }


    public void RequestFocus()
    {
        var eventSys = FindObjectOfType<EventSystem>();
        EventSystem.current = eventSys;
        if (!EventSystem.current)
        {
            Debug.Log("No event system");
        }
        eventSys.SetSelectedGameObject(null);
        eventSys.SetSelectedGameObject(FirstSelectedObject);
    }


    public void Resume()
    {
        Pause.Instance.Paused = false;
    }

    public void Restart()
    {
        Pause.Instance.Paused = false;
        SceneHandler.Instance.ReloadLevel();
    }

    private void OnDestroy()
    {
        if (Pause.HasInstance)
        {
            Pause.Instance.OnPauseChanged -= OnPaused;
        }
    }
}
