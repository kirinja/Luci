﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SceneButton : MonoBehaviour
{
    public string Scene;
    public bool DeleteSave = true;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(LoadScene);
    }


    public void LoadScene()
    {
        if (DeleteSave)
            IO.Instance.NewGame();
        SceneManager.LoadScene(Scene);
    }
}
