﻿using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;


[RequireComponent(typeof(Image))]
public class OverlayFade : MonoBehaviour
{
    public float FadeInTime = 1f;
    public float WaitTime = 1f;
    public float FadeOutTime = 1f;


    private Timer fadeInTimer_;
    private Timer waitTimer_;
    private Timer fadeOutTimer_;
    private Image image_;


    // Use this for initialization
    private void Start()
    {
        fadeInTimer_ = new Timer(FadeInTime, true);
        waitTimer_ = new Timer(WaitTime, true);
        fadeOutTimer_ = new Timer(FadeOutTime, true);
        image_ = GetComponent<Image>();
        image_.color = new Color(image_.color.r, image_.color.g, image_.color.b, 0f);
    }


    // Update is called once per frame
    private void Update()
    {
        var deltaTime = TimeManager.DeltaTime;

        if (!fadeOutTimer_.IsDone)
        {
            fadeOutTimer_.Update(deltaTime);
            image_.color = new Color(image_.color.r, image_.color.g, image_.color.b, Mathf.SmoothStep(1f, 0f, fadeOutTimer_.PercentDone));
        }
        if (waitTimer_.Update(deltaTime))
        {
            fadeOutTimer_ = new Timer(FadeOutTime);
        }
        if (!fadeInTimer_.IsDone)
        {
            if (fadeInTimer_.Update(deltaTime))
            {
                waitTimer_ = new Timer(WaitTime);
            }
            image_.color = new Color(image_.color.r, image_.color.g, image_.color.b, Mathf.SmoothStep(0f, 1f, fadeInTimer_.PercentDone));
        }
    }


    [Button]
    public void StartFade()
    {
        fadeInTimer_ = new Timer(FadeInTime);
    }
}