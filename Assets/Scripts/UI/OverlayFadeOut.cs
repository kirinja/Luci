﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class OverlayFadeOut : MonoBehaviour
{
    public float FadeOutTime = 1f;

    private Timer fadeOutTimer_;
    private Image image_;


    // Use this for initialization
    private void Start()
    {
        fadeOutTimer_ = new Timer(FadeOutTime, true);
        image_ = GetComponent<Image>();
        image_.color = new Color(image_.color.r, image_.color.g, image_.color.b, 1f);
        StartFade();
    }


    // Update is called once per frame
    private void Update()
    {
        var deltaTime = TimeManager.DeltaTime;

        if (!fadeOutTimer_.IsDone)
        {
            fadeOutTimer_.Update(deltaTime);
            image_.color = new Color(image_.color.r, image_.color.g, image_.color.b,
                Mathf.SmoothStep(1f, 0f, fadeOutTimer_.PercentDone));
        }
    }

    [Button]
    public void StartFade()
    {
        fadeOutTimer_ = new Timer(FadeOutTime);
    }

    public bool IsDone()
    {
        return fadeOutTimer_.IsDone;
    }
}
