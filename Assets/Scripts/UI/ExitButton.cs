﻿using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Button))]
public class ExitButton : MonoBehaviour {

	// Use this for initialization
    private void Start () {
		GetComponent<Button>().onClick.AddListener(ExitGame);
	}


    public void ExitGame()
    {
        Application.Quit();
    }
}
